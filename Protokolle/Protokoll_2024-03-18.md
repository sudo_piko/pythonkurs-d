# Pikos Python Kurs am 18.03.2024

Protokoll 18.03.2024

Recap:

Klasse - wie mensch seine eigene Art Objekte baut

def __init__ → Konstruktor

in unserem Fall: Melodie Objekte mit 3 Methoden (Ursprungscode)

dann beim Abrufen: Objekt.methode()

wichtig: self nicht vergessen -- wenn wir mit dem Objekt etwas tun
wollen

Aufgabe 1

1.1.Methoden schreiben

→ waren Fingerübung-Aufgaben ;)

Das self in der Klammer der Methode nicht vergessen

self. .. können wir verwenden wie eine Variable

Methoden = wie ziemlich normale Funktionen

2. hier verwenden wir „//": gleich mit abgerundet statt 3,5→ 3

hier benutzen wir sclicing

.....

1.2 Methoden verstehen

Erinnerung: intervall = 1 default Argument

print Statements um zu verstehen, was da passiert

Frage der Aufgabe: was muss reingeschrieben werden, sodass wir das
verstehen

siehe Code-Schnipsel:

- Start mit Anfang & Ende der Methode

- es gibt elegantere Weisen als print Statements, aber große
Empfehlung: einfach & verständlich

**1.3 Oktavsprügen**→ Video am üblichen Ort :)

falls es noch Unklarheiten gibt nach dem Video → Mail an piko

Aufgabe 2 -- heute besonders wichtig

Wie kann ich Sachen selber verstehen, wie kann ich mich orientieren?

Gitlab ist ähnlich wie github (github ist größer & bekannter), für
beides gilt: da können Leute Code zusammen entwickeln & zur Verfügung
stellen

- gensound ist in python geschrieben und auf github findet sich der
Code

- auf github kann man sich durch ein Projekt durchklicken

- für uns interessant das README von gensound

- wenn in einem github/lab Projekt eine Datei als **README** existiert,
dann wird sie immer angezeigt (so aka „ich erklär die dir Welt")

> in dem Fall von gensound: was ist gensound, wie kann mensch es
installieren, show me the code > Beispiele von code

Vorgehen mit Code-Erklärungen/Beispielen z.B. auf github

> nicht alles durchlesen & versuchen zu merken
> 
> spielerischer Zugang: Beispiele ausprobieren

ein paar Wörter aus GENSOUND README
* channel: links und rechts bei den Lautsprechern → wave[0] linsk,
wave[1] rechts 
* dezibel: Messeinheit für Lautstärke
* crop: abschneiden
* Hz: Herz: wie hoch ein Ton ist 440 ist .....(pikos Tonbeispiel), 220
tiefer als 440  (Oktaven verdoppeln Hz-Zahlen quasi)

**Cheatsheets** können wir überfliegen & schauen, was wir schon kennen
* was ist leicht verständlich für mich?
* Wenn ich etwas nicht kenne, ignorieren

Erklärung zum WIKI:
- viel ausführlichere Themen
- für uns: Melodic Shorthand Notation besonders interessant

Aufgaben in der Stunde

**1. Auftrag:**
Wie kann ich einzelne Töne machen?

Problem: bei unserer Melodie wenn ich 5,5 schreibe dann wird 5 einfach
länger

> für den Auftrag müssen wir herausfinden, wie mensch Pausen machen
kann

Aufgabenlösung: (Code)

- r macht Pause

- um die Pause nicht sooo lange zu machen: r = z.B. 0.2

- wenn ich einmal =0.2 schreibe, dann wir jeder danach folgender
Ton/Pause auch so kurz, deshalb müsste mensch zu jedem Ton/Pause eine
Länge aufschreiben (Pikos spoiler: Fall für eine for-Schleife)

→ wenn wir das bei Melodie-Klasse integrieren:
(Code-Schnipsel)

wir fügen noch ein „=1" für den Ton, „Leerzeichen" und ein „ r =0.2"

und so muss das nicht alles niedergetippt werden

2. Auftrag:

mit dem Code-Beispiel 4-part chorale rumprobieren, abspielen ggf. etwas
verändern

- ganz viel unserer jetzigen Musik basiert auf Choralen: gut
verständlich, weil alle zusammen singen, 4 Stimmen: Sopran, Alt, Tenor,
Bass

weitere Erklärungen:

- Schreibweise 0.5e3 = 500.0
- fermata = Haltestelle, wenn im Choral ein Halbsatz z.B. zu Ende ist
- pan = Richtung, aus der das gefühlt kommt, z.B. ...große Plus-Zahl
links, große -- Zahl rechts → durch Stereo-Ausgang hat mensch das Gefühl
die Stimmen sind verteilt im Raum
- da sehr komplex, lieber einfachere Programme nehmen und sich dann
Ideen einholen aus anderen Codes

How-To: Rauskopieren, Ausprobieren <3

beim Ausprobieren, nicht zu doll ärgern über Fehlermeldung, lieber
weiterziehen und nächstes Ausprobieren

- damit der String bei fermata mitausrechnet, muss f" ergänzt werden

Fragen aus der Stunde:

wenn wir jetzt schreiben: from gensound import Sine - von wo holt sich
dann thonny den code?

→ nicht von der WEBSEITE sondern im Computer gespeichert, beim
runterladen lokal gespeichert, wenn sich also etwas im code (auf github)
ändert, müssten wir gensound im thonny updaten

**nächste Woche: wie kann mensch weitermachen**

## Pikos Datei

```python
from gensound import Sine

class Melodie:
    def __init__(self, toene):
        self.toene = toene
    
    def erhoehen(self, intervall=1):
        neue_toene = []
        print("Anfang der Methode. self.toene ist:", self.toene)
        for ton in self.toene:
            neue_toene.append(ton+intervall)
        
        self.toene = neue_toene
        print("Ende der Methode. self.toene ist:", self.toene)
        
    def print_toene(self):
        print("Hier die Töne der Melodie:", self.toene)
        
    def melodie_laenge_printen(self):
        melodie_laenge = len(self.toene)
        if melodie_laenge > 10:
            print("Lange Melodie")
        else:
            print("Kurze Melodie")
        
    def print_halbe_melodie(self):
        haelfte = len(self.toene)//2
        print(self.toene[:haelfte])
        
        
    def ausgeben_fuer_sine(self):
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
        
        ergebnis = ""
        for ton in self.toene:
            ergebnis += tonnamen[ton] + " "
        return ergebnis


kleine_melodie = Melodie([5, 3, 1, 4, 5, 3, 1])
kleine_melodie.print_toene()
kleine_melodie.print_halbe_melodie()


melodiestring = kleine_melodie.ausgeben_fuer_sine()
print("So sieht die Ausgabe von 'ausgeben_fuer_sine' aus:", melodiestring)

s = Sine(melodiestring, duration=500.0)*0.2
s.play()

kleine_melodie.erhoehen(5)
kleine_melodie.print_toene()


melodiestring = kleine_melodie.ausgeben_fuer_sine()
print("So sieht die Ausgabe von 'ausgeben_fuer_sine' aus:", melodiestring)

s = Sine(melodiestring, duration=500.0)*0.2
s.play()
```
```python
from gensound import WAV, test_wav

wav = WAV(test_wav) # load sample WAV, included with gensound
wav.play(sample_rate=320000) # original sample rate 44.1 kHz
```
```python
from gensound import Sine

class Melodie:
    def __init__(self, toene):
        self.toene = toene
    
    def erhoehen(self, intervall=1):
        neue_toene = []
        print("Anfang der Methode. self.toene ist:", self.toene)
        for ton in self.toene:
            neue_toene.append(ton+intervall)
        
        self.toene = neue_toene
        print("Ende der Methode. self.toene ist:", self.toene)
        
    def print_toene(self):
        print("Hier die Töne der Melodie:", self.toene)
        
    def melodie_laenge_printen(self):
        melodie_laenge = len(self.toene)
        if melodie_laenge > 10:
            print("Lange Melodie")
        else:
            print("Kurze Melodie")
        
    def print_halbe_melodie(self):
        haelfte = len(self.toene)//2
        print(self.toene[:haelfte])
        
        
    def ausgeben_fuer_sine(self):
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
        
        ergebnis = ""
        for ton in self.toene:
            ergebnis += tonnamen[ton] + "=1" + " r=0.2 "
        return ergebnis


kleine_melodie = Melodie([5, 3, 3, 1, 4, 5, 5, 3, 1])
kleine_melodie.print_toene()
kleine_melodie.print_halbe_melodie()


melodiestring = kleine_melodie.ausgeben_fuer_sine()
print("So sieht die Ausgabe von 'ausgeben_fuer_sine' aus:", melodiestring)


s = Sine(melodiestring, duration=500.0)*0.2
s.play()
```
```python
from gensound import Sine

kurze_melodie_string = "C C E E G G E"
fermata = 0.1

s = Sine(f"C r=0.2 C=1 r=0.2 E=1 r=0.2 E={2+fermata} r=0.2 G=1 r=0.2 G=1 r=0.2 E=1", duration=500.0)*0.6
s.play()


# Aufgabe: Recherchiert im Wiki von gensound, wie Ihr zwischen die Töne
# kurze Pausen machen könnt, sodass "C C" nicht mehr klingt wie ein
# langer Ton, sondern wie zwei kurze.
# Zeit bis 20:30
```
```python
from gensound import Sine, Triangle, Square, Pan

sig = Sine # Sine? Square?

beat = 0.5e3 #  120 bpm
fermata = 0.4 # make fermatas in the melody slightly longer
pause = 0.6 # and breathe for a moment before starting the next phrase

S = sig(f"r D5 D=2 C#=1 B-13=2 A=1 D E=2 F#-13={2+fermata} r={pause} F#=1 F#=2 F#=1 E=2 F#-13=1 G F#-13=2 E={2+fermata} r={pause} "
        f"D+16=1 E=2 F#-13=1 E=2 D+16=1 B-13 C#=2 D+9={2+fermata} r={pause} A'=1 F#-13=2 D+16=1 E=2 G=1 F#-13 E=2 D=3", beat)
A = sig(f"r A4 B=2 A+16=1 G=2 F#-13=1 F# B-13 A A={2+fermata} r={pause} C#=1 B=2 B=1 B A A A D A A={2+fermata} r={pause} "
        f"B=1 A=2 A=1 B-13 A=0.5 G F#=1 B-13 B A#-13 B={2+fermata} r={pause} A=1 A=2 B=1 A=2 A=1 A B-13 A F#-13=3", beat)
T = sig(f"r F#4-13 F#=2 F#=1 D=2 D=1 D D C#-13 D={2+fermata} r={pause} C#=1 D+16=2 D+16=1 D C#-13 D E A, D C#-13={2+fermata} r={pause} "
        f"F#=1 E=2 D=1 D C#-13 D+16 D G+5 F# F#={2+fermata} r={pause} E=1 F#-13=2 F#=1 E=2 C#-13=1 A B C#-13 D=3", beat)
B = sig(f"r D3 B-16 D F# G B-13 D B-16 G A D,={2+fermata} r={pause} A#'-13=1 B=2 A=1 G#-13 A F#-13 C#-13 D F#-13 A={2+fermata} r={pause} "
        f"B=1 C#-13=2 D=1 G, A B G E F# B,={2+fermata} r={pause} C#'-13=1 D C# B C#-13 B A D G, A D,=3", beat)

chorale = S*Pan(25) + B*Pan(-25) + T*Pan(80) + A*Pan(-80) # position the voices in the stereo field
chorale.play() # can you spot the parallel octaves?
```
Siehe: https://github.com/Quefumas/gensound/wiki/Melodic-Shorthand-Notation