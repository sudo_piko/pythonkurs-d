# Pikos Python Kurs am 08.01.2024

## Pads
Alle Python-Wörter:  
https://md.ha.si/03OdwDaaQdC3kC7qZ7P7eg#



## Protokoll

### Allgemeines
Es gibt Interesse für weitere Wiederholerstunde in Form eines Quiz. Es
kommen auch wieder mehr Basics in den Stunden vor.
Nächstes Mal gibt es ein Quiz zu den beiden Funktionsvideos aus der
Weihnachtspause. Tipp: Unbedingt Videos zu Funktionen anschauen.
Piko hat ein Pad für uns erstellt mit allen Funktionen, die wir mal
verwendet haben: https://md.ha.si/03OdwDaaQdC3kC7qZ7P7eg#

### Codeschnipsel aus der Stunde mit Hinweisen
`from random import random` # Funktion heißt wie das Modul ‚random'.
Sie wird am häufigsten verwendet und gibt zuverlässig zufällige Zahl
zwischen 0 und 1 aus.

`zufallsfarbe = (random(), random(), random())` # Empfehlung: Als Tupel
schreiben für besseren Überblick. Die Farben  bleiben so, wenn sie ausgewürfelt werden.
‚random()' wird nicht erneut ausgeführt.

`zufallsfarbe = bgcolor` # bgcolor ohne Klammern: Funktion ist hier ein
Objekt ohne Funktion. Es wird nur der Speicherort der Funktion auf dem
Computer ausgegeben. Hinweis: Speicherort wird als Hexadezimalzahl
angegeben - falls sich eins mehr damit auseinandersetzen mag. Das ist
nochmal anders als das Binärsystem.

```python
zufallsfarbe = (random(), random(), random())
...
zufallsfarbe = (random(), random(), random())
```

Hier wird die erste
Zeile mit neuen zufälligen Zahlen überschrieben, da erneut ausgewürfelt
und das Ergebnis in der gleichen Variable abgespeichert wird.

`input()` Wird in einer Variable gespeichert weiter nutzbar: 
`farbe_geraten = input("Was glaubst du, ist das für eine Farbe?")`
Es wird immer ein string daraus = eine Quelle von Fehlern durch Eingaben, die nicht eingegeben werden
sollen (Leaks, IT-Security)

`for ziffernschnipsel in text.split(", "):` # da `text.split(", ")`
nur einmal verwendet wird, muss es nicht unbedingt zuerst in einer
Variable abgespeichert werden.
...

### Farben
Es gibt zwei unterschiedliche Arten Farben zu beschreiben: Additive und
subtraktive Farbmischung.

Bei der additiven Farbmischung wird Licht hinzugefügt. Die Primärfarben
sind rot, grün, blau (RGB-Farben).
Die Darstellung der einzelnen Werte erfolgt zwischen 0 und 1. Für das
Farbquiz haben wir RGB-Farben verwendet.---> Umso näher Richtung 0
(schwarz), umso dunkler wird die Farbe. Umso näher Richtung 1 (weiß),
umso heller.

Bei der subtraktiven Farbmischung wird Licht weggenommen. Es wird nur
ein Teil des Lichts zurückgeworfen. Bei Maximum kommt schwarz raus.
Beispiele sind ein Grundschulfarbmalkasten oder Druckerfarben. Die
Farben sind: cyan (türkis), magenta und gelb.

Auf Wikipedia gibt es gute Artikel mit Bildern dazu:
<https://de.wikipedia.org/wiki/Subtraktive_Farbmischung>
<https://de.wikipedia.org/wiki/Additive_Farbmischung>
<https://de.wikipedia.org/wiki/RGB-Farbraum>


## Pikos Dateien
```python
from turtle import *
from random import random

setup(300, 800, 2580, 10)

zufallsfarbe = (random(), random(), random())  # 0.54, 0.11, 0.93

bgcolor(zufallsfarbe)
write("Rate mal, welche Farbe das ist!")
# color("black")
# pensize(100)
# dot()
print(zufallsfarbe)


# Tipps:
# Das hier sind nicht die selben Werte, die in bgcolor
# reingekommen sind... Wie kann ich die behalten? Ich
# muss die irgendwie speichern, damit ich sie später
# wiederverwenden kann...
# In einer Variable speichern! Das muss dann außerhalb von bgcolor
# passieren, also *vorher*. Dann kann ich die Variable für alles
# verwenden.

# Wenn Ihr schon durch seid: Überlegt, wie Ihr die Farben etwas
# spannender machen könnt, zB darf die Summe der Farben immer nur xxx sein
# und so.
```

```python
from turtle import *
from random import random


setup(300, 800, 2580, 10)

zufallsfarbe = 0.54, 0.11, 0.93

bgcolor(zufallsfarbe)


zufallsfarbe = 0.11, 0.93, 0.01

print(zufallsfarbe)




# Tipps:
# Das hier sind nicht die selben Werte, die in bgcolor
# reingekommen sind... Wie kann ich die behalten? Ich
# muss die irgendwie speichern, damit ich sie später
# wiederverwenden kann...
# In einer Variable speichern! Das muss dann außerhalb von bgcolor
# passieren, also *vorher*. Dann kann ich die Variable für alles
# verwenden.

# Wenn Ihr schon durch seid: Überlegt, wie Ihr die Farben etwas
# spannender machen könnt, zB darf die Summe der Farben immer nur xxx sein
# und so.
```
```python
from turtle import *
from random import random

setup(300, 800, 2580, 10)

zufallsfarbe = (random(), random(), random())  # 0.54, 0.11, 0.93

bgcolor(zufallsfarbe)


farbe_geraten = input("Was glaubst du, ist das für eine Farbe?")

print("Dein Rateversuch: " + farbe_geraten)
zahlen = []
for ziffernschnipsel in farbe_geraten.split(", "):
    kommazahl = float(ziffernschnipsel)
    zahlen.append(kommazahl)
    
print(type(farbe_geraten))
color(zahlen)
pensize(100)
dot()

print("Richtiges Ergebnis", zufallsfarbe)


# Verändert das Programm so, dass, bevor die richtige Lösung unten
# ausgegeben wird, nach einem Rateversuch gefragt wird.
# Danach soll der Rateversuch angezeigt werden, und darunter
# die richtige Lösung, zB so:
# Dein Rateversuch: 0.3, 0, 28
# Richtiges Ergebnis: (0.8960829969309921, 0.6076938968774044, 0.6592784720292257)
```
```python
text = "0.2, 0.8, 0.3"

# Aufteilen ging mit split()

print(text.split())
# ['0.2,', '0.8,', '0.3']
# Uff, die haben ja noch Kommata... Die kriegen wir auch mit split weg!
print(text.split(","))
# ' 0.3' so sind die Leerzeichen noch da... Wir brauchen beides!
print(text.split(", "))

# Ok, aber wie machen wir aus den drei Strings jetzt Kommazahlen?

zahlen = []
for ziffernschnipsel in text.split(", "):
    kommazahl = float(ziffernschnipsel)
    zahlen.append(kommazahl)
    
print(zahlen)

ergebnis = (0.2, 0.8, 0.3)


# Erster Tipp: Schaut, wie wir das mit den Wörtern für den Weisheits-
# generator gemacht haben!
```
Was nicht klappt:
```python
a = "0.3, 0.7, 0.1"

b = tuple(a)

print(b)
```