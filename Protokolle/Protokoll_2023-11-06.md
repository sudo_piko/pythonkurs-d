# Pikos Python Kurs am 06.11.2023

## Protokoll

### Organisatorisches

In Gitlab gibt es eine neue Seite, auf der alle Pads gesammelt zu finden
sind:
https://gitlab.com/sudo_piko/pythonkurs-d/-/blob/main/Pads.md?ref_type=heads

Um Infos zu Haecksen-Treffen zu bekommen, eine E-Mail an:
[*info@haecksen.org*](mailto:info@haecksen.org) schreiben.

Ausstellung der Haecksen auf dem CCC Kongress: alle die mögen dürfen
etwas beitragen, z. B. Tiles malen; Piko gibt Ideen Input

### Anmerkungen zur Hausaufgabe

ttile mit Ecken zusammen gemacht; fertige Datei in Ordner `Projekte/Tiles/`

### Zu Aufgabe 2 (letztes Tile)

Vorgehen: Zeilen von links unten nach rechts anschauen. Feststellung:
hier ist eine Regelmäßigkeit zu erkennen.

In Zeile 0, 2, 4 (gerade Zahlen) drehen sich die Tiles im Uhrzeigersinn,

in den ungeraden Zeilen drehen sich die Tiles gegen den Uhrzeigersinn

Mit dieser Info die Aufgabe nochmal üben. Wenn es nicht klappt, dann
Mail an Piko.

Es gibt ein Video dazu unter https://diode.zone/c/pykurs_d/videos

### Strings (Texte)

wort = "flauschbaellchen"

print(wort[10])

Der 10. Buchstabe wird ausgegeben

print(wort[5:10])

Die Buchstaben von 5 bis 10 werden ausgegeben

[5:10] ist das hier ist das gleiche wie:

for i in range(5,10):

> print(i)

range( ) ist eine Funktion

[ ] ist eine extra Syntax die sagt: lieber string, gib mir diesen Teil
von dir

print(wort[3:10:2])

Angefangen vom 3. Buchstaben, wird jeder zweite Buchstabe bis zum 10.
Buchstaben ausgegeben

wort[3:10:2] ist das hier ist das gleiche wie:

for i in range(3, 10, 2):

> print (i)

### Indizierung/Indexing und Slicing

Aufgabe 1:

Hole aus "sinneszelle" insel raus
```
wort = "sinneszelle"

print(wort[1:10:2])
```
(mehr davon als Hausaufgabe)

### Negative Zahlen funktionieren auch

```
Wort[-1]
```

Der letzte Buchstabe wird ausgegeben

```
Wort[-4:-1]
```

Die Zahlen vom viert-letzten Buchstaben zum letzten werden ausgegeben

```
Wort[-4:10]
```

Die Zahlen vom viert-letzten Buchstaben zum letzten werden ausgegeben

[-4:-1] ist das gleiche wie [-4:10]

### Indizierung/Indexing

```
wort[0]
```

Das ist ein Index; ein Wert wird ausgegeben

### Slicing

```
Wort[1:10]
```

Das ist ein Slice (eine Scheibe)

### Indexing und Slicing im Vergleich:

```
wort = "flauschbaellchen"
#       01234567890123456

print(wort[7])

print(wort[0:7])
```
**b**

**flausch**

Bei [0:7] ist der erste Buchstabe dabei, der siebte Buchstabe nicht
mehr.

→ Anfang ist dabei, Ende nicht mehr

```
wort[4:14:2]
```

ist das gleiche wie:

```
wort[4:-1:2]
```

```
wort[4::2]
```
hier ist der letzte Buchstabe dabei, weil wir mit :: sagen bis zum Ende

### Listen

Das sind Listen:

```
rgb_farben = ["rot", "grün", "schwarz"]

nette_zahlen = [5, 23, 42, 1337]

dingsies = [23, 42.5, "hallo", "schokolade"]
```
Listen sind toll und flexibel, und sie können korrigiert werden.

```
rgb_farben[2] = "blau"
```

Hier wird die Liste rgb_farben korrigiert. Aus "schwarz" (zweite Stelle
in der Liste, weil wir bei 0 das Zählen anfangen), wird "blau"



## Pikos Datei
Hausaufgabe
```python
from turtle import *
from random import randint

seitenlaenge = 50
dicker_stift = 10

pensize(dicker_stift)
# pu()

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        
def viertelquadrat():
    fd(...)
    lt()
    begin_fill()
    ...
    
def vier_eckchen_in_die_ecken():
    for i in range(4):
        viertelquadrat()
        fd(seitenlaenge)
        lt(90)
    
def ttile_1():
    pu()
    vier_eckchen_in_die_ecken()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()

        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    
    ttile_1()
    
    rt(90)
    back(seitenlaenge)
    
def ttile_kreuzung():
    pu()
    fd(seitenlaenge/2)
    pd()
    lt(90)
    fd(seitenlaenge)
    pu()
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    fd(seitenlaenge/2)
    pd()
    rt(90)
    fd(seitenlaenge)
    pu()
    lt(90)
    fd(seitenlaenge/2)
    lt(90)
    
    
for j in range(10):
    for i in range(10):
        pu()
        goto(i*seitenlaenge-200, j*seitenlaenge-200)
        a = randint(0, 2)
        if a == 0:
            ttile_1()
        elif a == 1:
            ttile_2()
        else:
            ttile_kreuzung()
```
Den Ausgangscode für die Aufgabe mit den Dreiecks-Mustern findet Ihr im Ordner `Projekte/Tiles/` in der Datei `Truchet_Tiles_Dreiecke.py`.

###  Inhalt: Strings!
Indizierung/Indexing und Slicing 
```python
wort = "flauschbaellchen"
#       0123456789012345

dings = wort[8]  # genauso für 0, -1, -6, etc => INDIZIERUNG
dongs = wort[5:10]  # von 5 bis 10  => SLICING

print(dongs)

for i in range(5, 10):  # von 5 bis 10
    print(i)
```
Slicing mit Schrittweite
```python
wort = "flauschbaellchen"
#       0123456789012345

dings = wort[3:10:2] 
print(dings)


# for i in range(5, 40, 3):
#     print(i)
for i in range(3, 10, 2):
    print(i)
```
```python
wort = "flauschbaellchen"
#       0123456789012345

dings = wort[-4:] 
print(dings)
```
```
Indexing
wort[0]
wort[-3]
wort[10]

Slicing
wort[3:10]
```
```python
wort = "flauschbaellchen"
#       0123456789012345
# print(wort[7])
# print(wort[0:7])

# 02468
# print(wort[0:10:2])
# 468 10 12 14
print(wort[4::2])
print(wort[4:14:2])
print(wort[4:-1:2])
```
Auch mit Listen:
```python
rgb_farben = ["rot", "grün", "schwarz"]
nette_zahlen = [5, 23, 42, 1337]
dingsies = [[23, 42.5, "hallo", "shokolade"], 2]

print(rgb_farben[2])
print(dingsies[0][3][2])
```
```python
rgb_farben = ["rot", "grün", "schwarz"]

print(rgb_farben[2])  # Den Fehler nochmal anzeigen
rgb_farben[2] = "blau"  # Das falsche Element ersetzen

print(rgb_farben[2])  # das korrigierte Element anzeigen

print(rgb_farben)  # die ganze korrigierte Liste anzeigen

wort = "frauschbaellchen"

print(wort[1])
wort[1] = "l"
print(wort[1])
print(wort)
# !! Führt zu einer Fehlermeldung, weil das Item-Assignment nur bei Listen funktioniert, nicht bei Strings!
```