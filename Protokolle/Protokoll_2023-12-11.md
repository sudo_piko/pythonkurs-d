# Pikos Python Kurs am 11.12.2023

Deadline für die Ausstellungsstücke ist der 15.12.2023. Bitte schreibt Ceryo eine Mail mit dem Programm oder dem Bild. 
Gerne auch mehr als eins!

## Pads
Schaut zur Inspiration


## Pikos Datei
```python
text = "Montag Dienstag Dingsdongstag Donnerstag Freitag Samstag Sonntag"

print(text)

text = text.replace("Dingsdongstag", "Mittwoch")

print(text)



string1 = "')[1].split(' | ')] for line in the_puz.splitlines()]]]))"

string1 = string1.replace("the_puz", "my_new_puzzle")
string2 = "')[1].split(' | ')] for line in my_new_puzzle.splitlines()]]]))"
print(string1)
print(string2)
```
```python
from random import choice

woerterliste = ["Stephenschlüpfer", "Meisendickkopf", "Fiskalwürger", "Rotkopfwürger", "Torobülbül", "Strichelkehlsopranist", "Ultramarinbischof", "Maskensaltator", "Cinderellaastrild"]

ausgewuerfelt = choice(woerterliste)

satz = "Der Vogel des Tages ist der Sperling."

print(satz.replace("Sperling", ausgewuerfelt))
```


Der fertige Weisheitsgenerator ist `Projekte/Weisheitsgenerator/Weisheits_SVG_Generator.py`.
Die Dateien, die gestern noch beim Sternhimmelbasteln entstanden sind, findet Ihr im Ordner `Projekte/Congressprogramme/`.