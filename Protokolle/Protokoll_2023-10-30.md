# Pikos Python Kurs am 30.10.2023

## 30.10. Protokoll


Orga-Frage von CerYo: Gibt’s Ideen bez möglicher Beiträge der Gruppe zum Chaos Communication Congress dieses Jahr? Ausstellungsraum der Haecksen nutzen?

Piko >> Nächste Sitzung einmal darüber diskutieren, Ideen sammeln bis nächste Sitzung



### Frage von letzter Woche: 
Farbverlauf / Farbübergang erstellen bei zufällig verteilten Tiles

Piko >> Wenn jemand eine Lösung hat, wie man die Tiles bunt macht, gerne teilen (Später hat sich herausgestellt, dass die Frage genau auf das Thema der heutigen Stunde gezielt hat; demnach: siehe unten)

### Erklärung “==”

im Kontext von “ x == 2”: Für Python ist das eine Rechnung, die entweder “True” oder “False” ergeben kann

z.B. “ich sage, dass x plus y acht ergibt” // “Python, überprüfe, ob x plus y acht ergibt”
```
x = 4

y = 4

x + y == 8

Rechnung Python: 4+4 == 8 => True
```

“ich sage, dass vier plus fünf acht ergibt” // “Python, überprüfe, ob vier plus fünf acht ergibt”
```
4+5 == 8 => False
```

### Erklärung Modulo mit `==`

Wenn wir Modulo verwenden und schreiben 5 % 3 == 0, meinen wir:

“Wenn 5 / 3 gerechnet wird, dann kommt 0 dabei raus”

Python rechnet das und sagt dann “False”, weil 5/3 nicht sauber ohne Rest teilbar ist


“True” und “False” sind **boolesche Werte** ("boolean values" oder auch nur "boolean". `a == 2` ist ein **boolescher Ausdruck** ("boolean expression").

Das heißt: Das Ergebnis einer "booleschen Rechnung" kann nur eins sein, entweder “True” ODER “False”. Es gibt keine 3. Option.

5%3 == 0 ist ein boolescher Ausdruck

Das heißt: Wenn Python diese Zeile liest, muss Python ausrechnen, ob die Aussage richtig oder falsch ist.



## Pikos Datei
Hausaufgabe
```python
from turtle import *


setup(800, 800, 2560, 10)
pu()
seitenlaenge = 40
dicker_stift = 8
duenner_stift = 1

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        pensize(duenner_stift)
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    ttile_1()
    rt(90)
    back(seitenlaenge)
    


for j in range(10):
    for i in range(4):
        goto(i*seitenlaenge, j*seitenlaenge)
        ttile_1()
```
Was beim if wirklich passiert:
```python
a = 2 * 5 + 7**4  - int( "4242"*3)/2000  #> 0
a =   10  + 2401  - int("424242424242")/2000
a =   10  + 2401  - 424242424242/2000


a = 10 == 2*5
a = 10 ==  10
a = True

b = 42 < 1337-2342
b = 42 < -1005
b = False

c = 5 % 3 == 0
c = 2 == 0  # boole'scher Ausdruck
c = False


if False

alter = 12
if alter > 18:
if  12   > 18:
if    False  : 
```

Regelmäßige Anordnung; interessant sind die Zahlen 5 (in `for i in range(5):`) und 4 (in `if counter % 4 == 0:`) – die verändern das Muster, wenn Ihr damit herumspielt.
```python
from turtle import *

setup(800, 800, 2560, 10)
speed(0)
pu()
seitenlaenge = 40
dicker_stift = 8
duenner_stift = 1

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        pensize(duenner_stift)
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    ttile_1()
    rt(90)
    back(seitenlaenge)
    
counter = 0

for j in range(10):
    for i in range(5):
        goto(i*seitenlaenge, j*seitenlaenge)
        if counter % 4 == 0: # Wenn counter durch 4 teilbar ist:
            ttile_1()
        else:
            ttile_2()
        print(counter)
        counter = counter + 1
```

Vollständig zufällig:
```python
from turtle import *
from random import randint

setup(800, 800, 2560, 10)
speed(0)
pu()
seitenlaenge = 40
dicker_stift = 8
duenner_stift = 1

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        pensize(duenner_stift)
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    ttile_1()
    rt(90)
    back(seitenlaenge)
    

for j in range(10):
    for i in range(5):
        goto(i*seitenlaenge, j*seitenlaenge)
        zufallszahl = randint(1, 2)
        if zufallszahl == 1: # Wenn counter durch 3 teilbar ist:
            ttile_1()
        else:
            ttile_2()
```
Mit Farbübergang
```python
from turtle import *
from random import randint

setup(800, 800, 2560, 10)
speed(0)
pu()
seitenlaenge = 40
dicker_stift = 8

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    ttile_1()
    rt(90)
    back(seitenlaenge)
    

breite = 5

for j in range(10):
    for i in range(breite):
        goto(i*seitenlaenge, j*seitenlaenge)
        color(i/breite, 0, 0)
        zufallszahl = randint(1, 2)
        if zufallszahl == 1: # Wenn counter durch 3 teilbar ist:
            ttile_1()
        else:
            ttile_2()
```

Mit Kreuzungen
```python
from turtle import *
from random import randint

setup(800, 800, 2560, 10)
speed(0)
pu()
seitenlaenge = 40
dicker_stift = 8

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    ttile_1()
    rt(90)
    back(seitenlaenge)
    
def ttile_3():
    pu()
    fd(seitenlaenge/2)
    lt(90)
    pd()
    fd(seitenlaenge)
    pu()
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    pd()
    fd(seitenlaenge)
    pu()
    lt(90)
    fd(seitenlaenge/2)
    lt(90)
    


breite = 5

for j in range(10):
    for i in range(breite):
        goto(i*seitenlaenge, j*seitenlaenge)
        color(i/breite, 0, 0)
        zufallszahl = randint(1, 3)
        if zufallszahl == 1: # Wenn counter durch 3 teilbar ist:
            ttile_1()
        elif zufallszahl == 2:
            ttile_2()
        else:
            ttile_3()
```

Mit Punkten
```python
from turtle import *
from random import randint

setup(800, 800, 2560, 10)
speed(0)
pu()
seitenlaenge = 40
dicker_stift = 8

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    ttile_1()
    rt(90)
    back(seitenlaenge)
    
def ttile_3():
    pu()
    fd(seitenlaenge/2)
    lt(90)
    pd()
    fd(seitenlaenge)
    pu()
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    pd()
    fd(seitenlaenge)
    pu()
    lt(90)
    fd(seitenlaenge/2)
    lt(90)
    


breite = 5

for j in range(10):
    for i in range(breite):
        goto(i*seitenlaenge, j*seitenlaenge)
        color(i/breite, 0, 0)
        dot()
        pensize(2)
        pencolor(1, 1, 1)
        dot()  # very hacky, eigentlich nicht richtig.
        pensize(dicker_stift)
        color(i/breite, 0, 0)
        
        zufallszahl = randint(1, 3)
        if zufallszahl == 1: # Wenn counter durch 3 teilbar ist:
            ttile_1()
        elif zufallszahl == 2:
            ttile_2()
        else:
            ttile_3()
```