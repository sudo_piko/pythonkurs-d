# Pikos Python Kurs am 18.09.2023
## Am kommenden Montag, den 25.09. ist kein Kurs! Stattdessen werde ich Euch hier auf Gitlab Videolinks zur Hausaufgabenbesprechung und zu den Inhalten hochladen. Ihr könnt Euch aber natürlich trotzdem treffen und die Hausaufgaben besprechen!

## Pads
Schöne Kreise aus der Hausaufgabe: https://md.ha.si/6hVuSirgRrKxgsdwg9NF3w?both#

## Protokoll
Protokoll 18.09.23 - Dingsdongs

Veranstaltungshinweise

FINTA\* Personen in HAMBURG: Geekfem 21.09. ab 19 Uhr\>\> die Hamburger
Haecksen werden sich kommende Woche treffen im Vereins-Club des CCC HH
Siehe auch die Webseite des CCCHH: https://www.hamburg.ccc.de/

Piko: Thema Lizenzen

-   Die in den Pads bei der letzten HA angefertigten Bilder sind NICHT
    privat, sondern zugänglich für alle, die den Link oder Zugang dazu
    haben

-   Das heißt NICHT, dass Fremde ohne Erlaubnis z.B. solche Bilder
    verwenden dürfen.

-   Wenn z.B. Bilder explizit verwendet werden dürfen, kann man das über
    eine Kennzeichnung mit einer Creative Commons Lizenz an alle
    Menschen mit Zugriff auf das Bild kommunizieren

Ein hilfreicher Link dazu, wie creative commons Lizenzen funktionieren:

[*https://creativecommons.org/licenses/?lang=de*](https://creativecommons.org/licenses/?lang=de)

Piko: Wenn kein Entsprechender Hinweis an einem Bild z.B. aus dem Kurs
dran steht, wird nicht geteilt das was im Kurs intern veröffentlicht
wird

### Inhalte

Frage Verwendung Anführungszeichen. Wann " und wann '

\>\> Piko: Kein gurndsätzlicher Unterschied. Wenn wir aber ein Apostroph
' oder Anführungszeichen " innerhalb eines Strings verwenden will,
braucht aber beide

Bsp:

Funktioniert nicht:

`'They're nice'`

\>\> Abbruch bei 'They', Fehlermeldung

Funktioniert: `"They're nice"`

Piko Tipp für Python / Thonny: Kommentare

Wenn man in Python vor einen Text ein # setzt, dann wird dieser Teil
von Python nicht gelesen.

Nützlich für Beschreibungen, Überlegungen, Entwürfe, Kommentare

for i in range (199): # 199 hab ich absichtlich gewählt

\>\> Python liest nur bis zum :

Zählung von Schleifendurchläufen in Python

In Schleifen (z.B. "for i in range (10):" fängt die Zählung der 10
Schleifendurchläufe bei Python i.d.R. mit 0 an

\>\> die erste Schleife startet mit 0, der letzte, zehnte Durchlauf
bekommt die Nummer 9

### Eine Schleife in einer Schleife

Man kann Schleifendurchläufe auch "hintereinander schalten" :

z.B.

In meiner Schleife "for i in range (8):" kann ich in der nächsten Zeile
zusätzlich zum Beispiel angeben "for j in range (3):"

Dann zählt die "äußere Schleife" 0-7 und die "innere Schleife" 0-2

\>\> So kann z.B. jede Zahl in der Äußeren Schleife dreimal gedruckt
werden

### Zusammensetzung von Schleifen

Es gibt einen Schleifen-Kopf und einen Schleifen-Körper.

Im Kopf kann eine Integer-Zahl stehen oder auch eine "Sammlung" von
Begriffen, die in einer Schleife nacheinander behandelt werden soll

Im Körper steht, was Python mit den Inhalten aus dem Kopf tun soll

Schleifen-Kopf, Zusammensetzung

"einfache" for-Schleife

`for gegenstand in ("kleber", "brille", "tesafilm"):`

\>\> hier wird gesagt "in der Schleife gegenstand gibt es eine Box mit
den 3 Inhalten "kleber", "brille", "tesafilm")"

\>\> die 3 Inhalte werden nacheinander verwendet und im Schleifen-Körper
ausgeführt

### "vor-definierte" for-Schleife

```
kiste= ("messer", "gabel", "löffel")

for gegenstand in kiste:
```

\>\> hier wird AUSSERHALB der Schleife festgelegt, welche 3 Gegenstände
in der Kiste sind. In der Schleife ist kiste dann eine Variable, die
erst "messer", dann "gabel" und dann "löffel" verwendet



## Pikos Dateien
```python
for i in range(8):  # das hier ist eine for-Schleife
    print(i)
    print(i)
    print(i)
    # Hier wird i um 1 erhöht


for i in range(8):  # das hier ist eine for-Schleife
    for j in range(3):
        print(i, "(", j, ")")
    # Hier wird i um 1 erhöht

# 
# print(0)
# print(1)
# print(2)
# print(3)
# ...
```
```python
from turtle import *

pensize(10)

for farbe in ("red", "green", "blue"):
    color(farbe)
    forward(200)
    left(120)


######################## Das gleiche wie:
farbe = "red"  # !
color(farbe)
forward(100)
left(120)
farbe = "green"  # !
color(farbe)
forward(100)
left(120)
farbe = "blue"  # !
color(farbe)
forward(100)
left(120)
```
```python
for gegenstand in ["Schachtel", "klebeband", "Farbtube"]:
    print(gegenstand)
    
karton = ("Kompass", "Fernglas", "Schlüssel")
for gegenstand in karton:
    print(gegenstand)
```
```python
for i in range(8):  # das hier ist eine for-Schleife
    print(i)

for i in [0, 1, 2, 3, 4, 5, 6, 7]:  # das hier ist eine for-Schleife
    print(i)
```
```python
from turtle import *
pensize(10)

farbpalette = ("red", "green", "blue")

for farbe in farbpalette:
    color(farbe)
    forward(200)
    left(120)

# for schleifenvariable in wertevorrat:
#      spasshabenmit(schleifenvariable)
```
```python
for etwas in ("DINGS"):
    print(etwas)
    print("hui")
    
# DINGS
# oder?
# D
# I
# N
# G
# S
```