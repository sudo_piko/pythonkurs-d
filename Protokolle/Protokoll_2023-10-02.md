# Pikos Python Kurs am 02.10.2023

# Hausaufgaben-Videos
Aufgabe 1: Begrüßungsautomat  
https://diode.zone/w/tiyZPMcPhpMp7SP3yvwXzv

Aufgabe 2: Gestrichelte Linie  
https://diode.zone/w/51iimveqGYNVmHPoWkNrHQ

# Protokoll
## RGB
RGB-Farbangaben sind Tupel: (Rotwert, Grünwert, Blauwert)

Übergang von Blau nach Rot (siehe Hausaufgaben vom letzten Mal)
Anzahl der schritte lässt sich mit einer Variablen (z.B. anzahl\_schritte) eleganter und leichter verändern
Welche Zeichen können in Variablen nicht verwendet werden? 
 - Bindestrich ist schon mit Minus besetzt
 - Leerzeichen: damit wird aus einer Variable zwei

## Notiz zu for-schleifen mit range(): 
range(x) zählt nur bis 1 vor x

## Kreise mit Farbübergängen
Aufgabe: Kreis mit Farbübergang von schwarz nach rot (ohne circle() zu verwenden)
Ansatz: Einen Kreis kann man im Prinzip als Vieleck mit sehr, sehr vielen Ecken abbilden
Wie kann ich den Übergang von schwarz nach rot nach Schwarz machen? (5 Minuten)

## Kommazahlen in ganze Zahlen umwandeln mit int()
range möchte ganze Zahlen als Input haben! Gibt man ihm float als Input, gibt es eine Fehlermeldung. 
Die Umwandlung von float in int mit int() schneidet eventuelle Nachkommazahlen einfach ab - int() rundet nicht!

## for-schleifen und Wertevorräte
Zahlenfolge ausgeben (von 0 bis 9) mit ```for i in range()```
Buchstabenfolge "abcdef": auch strings sind "iterabel", d.h. können Zeichen für Zeichen in einer Schleife ausgegeben werden 
Strings verketten: 1 + "string" gibt eine Fehlermeldung, "1" + "string" ergibt "1string"
print() mit dem Argument "end": statt neuer Zeile druckt print damit den angegebenen String

Aufgabe: 2 for-Schleifen mit folgender Ausgabe:
1a
1b 
1c
1d
1e
1f
2a 
2b
2c
2d
2e
2f

## Warum verwenden wir i nur für Variablen, die hochzählen?
1. Konvention
2. Variablen sollten aussagekräftige Namen haben (wird bei längeren Programmen wichtig)



## Videolinks für Hausaufgaben
Kommen demnächst hier und auf der Hauptseite.


## Pikos Dateien
Hausaufgabenbesprechung

``` 
i      i/10     Blauwert = 1 - (i/10)

0	0.0	1
1	0.1	0.9
2	0.2	0.8
3		0.7
4		0.6
5		0.5
6		0.4
7		0.3
8		0.2
9	0.75	0.1
10	1	0
```

``` 
R   G  B
(0, 0, 1) : blau
(1, 0, 0) : rot



i	rotwert	   grünwert	blauwert

0	   0			  1		(0, 0, 1)
1       ...
2
3
4
5
6
7
8
9
10	  1			   0		(1, 0, 0)
```

```python
from turtle import *
pensize(20)
# 
# for i in range(11):
# #     color(0, 0, i/10)   # 0 => 1
#     color(0, 0, 1-(i/10))  # 1 => 0
#     fd(10)
    
anzahl_schritte = 50

# for i in range(anzahl_schritte + 1):
#     color(i/anzahl_schritte, 0, 1-(i/anzahl_schritte))
#     fd(10)

rot = 0
blau = 1
for i in range(11):
    color(rot, 0, blau)
    fd(10)
    rot = rot + 0.05
    blau = blau - 0.05
```
```python
# Lasst die Turtle einen Kreis malen, ohne circle() zu verwenden
# 
# So ein Kreis ist ja auch nur ein Vieleck mit sehr vielen Ecken...
# Also, für die Turtle jedenfalls.
# Erstmal ein Viereck.

from turtle import *
pensize(20)
# 
# anzahl_ecken = 10
# 
# for i in range(anzahl_ecken):
#     color(i/anzahl_ecken, 0, 0)
#     forward(20)
#     left(360/anzahl_ecken)

# Zwei Möglichkeiten:
# *eine* for-Schleife (komplizierter) oder
# *zwei* for-Schleifen mit jeweils einem halben Kreis...

# Lösung mit ZWEI for-Schleifen
anzahl_ecken = 50
kantenlaenge = 20

for i in range(int(anzahl_ecken/2)):
    color(i/(anzahl_ecken/2), 0, 0)
    forward(kantenlaenge)
    left(360/anzahl_ecken)

for i in range(int(anzahl_ecken/2)):
    color(1-i/(anzahl_ecken/2), 0, 0)
    forward(kantenlaenge)
    left(360/anzahl_ecken)
```
### Schreibt ein Programm, das folgende Ausgaben erzeugt
```
0
1
2
3
4
5
6
7
8
9
```
```python
for i in range(10):
    print(i)
```
```
1a
1b
1c
1d
1e
1f
```
```python
s = "abcdef"
# strings sind auch 'iterabel', dh Ihr könnt sie in for-Schleifen
# packen wie range(xxx) auch!

for buchstabe in s:
    print("1" + buchstabe, end="")
```
`end=""` macht, dass nach dem `print()`-Befehl kein Zeilenumbruch kommt. Das, was nach dem `=` kommt, wird jedesmal angehängt, wenn `print()` aufgerufen wird. Wenn wir dieses `end=` nicht verwenden, ist da ein Zeilenumbruch; das ist sozusagen die Default-Einstellung. Wenn wir was eingeben, wird der Zeilenumbruch dadurch ersetzt. So können wir verhindern, dass nach jedem `print()` ein Zeilenumbruch kommt und deswegen die Ausgabe so ewig lang wird...

### Exkurs: Was tut eine for-Schleife mit der Schleifenvariable?
```python
for buchstabe in "abcdef":
    print("1" + buchstabe, end="")

# ist das Gleiche wie:
buchstabe = "a"
print("1" + buchstabe, end="")

buchstabe = "b"
print("1" + buchstabe, end="")

buchstabe = "c"
print("1" + buchstabe, end="")

buchstabe = "d"
print("1" + buchstabe, end="")

buchstabe = "e"
print("1" + buchstabe, end="")

buchstabe = "f"
print("1" + buchstabe, end="")
```
[Exkurs Ende]
```
1a 1b 1c 1d 1e 1f

2a 2b 2c 2d 2e 2f
```
```python
for buchstabe in "abcdef":
    print("1" + buchstabe, end=" ")

print()

for buchstabe in "abcdef":
    print("2" + buchstabe, end=" ")
```