# Pikos Python Kurs am 11.03.2024

## Protokoll

Protokoll 11.03.24

### 1) Info zum Haecksenfrühstück am 23.03. 14:00

Samstag, 23. März 2024 um 14:00 in Ada
https://meeten.statt-drosseln.de/b/cri-uye-ogv-vfh
<https://meeten.statt-drosseln.de/b/cri-uye-ogv-vfh>
"Awesome Ada" ist der am häufigsten genutzte virtuelle Meetingraum der Haecksen
(bigbluebutton via meeten statt drosseln), der über obigen Link erreichbar ist.
Den Link zu Ada findet ihr in Rocket, zB in den Channeln #announce und
#haecksen, jeweils ganz oben beim Channelnamen.
Frühstücke sind vor allem für Neuhaecksen interessant, und für alle finta*, die
künftig Teil des Haecksenuniversums sein möchten. Es wird darum gehen, wer und
was die Haecksen sind, welche Projekte und Pläne es grade so gibt, und wie
Menschen auf die Mailingliste kommen. Und natürlich ist Zeit für Fragen und
Antworten und zum Kennenlernen.


### 2) Fragen nach Hausaufgaben & erfolgreicher Installation von gensound

```python
from gensound import Sine

s = Sine('D5 C# A F# B G# E3 C# F#', duration=500.0)*0.4

# wichtig: duration muss als float angegeben werden.
# Die Einheit sind ms (Millisekunden).

# "0.4" bezieht sich auf das audio output volume (Lautstärke),
# der Wert darf nicht >1 sein.

# Die Tonhöhe der Melodie kann verändert werden, indem hinter einen Ton eine andere Zahl geschrieben wird
# wie hier z.B. "E3"

s.play()

# play() ist eine Methode für das Objekt s, was ein Sine-Objekt ist
# Das ist der Typ von s:
print(type(s))
```

### 3) zunächst: Aufgabe 2.4

bei Piko: Fehlermeldung '...device or resource is busy' (simpleaudio), weil etwas anders darauf zugreift
mögliche Lösung: andere Programme, die audio erfordern, schließen

Alternative zu simpleaudio: playsound

#### Hintergrundinfos: 
*im engl. ist der Ton 'B' der, der im dt. 'H' ist
*zB. 'D#' bedeutet einen Halbtonschritt höher als D, (engl. 'D sharp', dt. 'Dis')
*die 5 in 'D5' bezieht sich auf die Oktave - auch hier gibt es Unterschiede zw. dt und engl
 (dt. C1 ist engl. C4, das "mittlere" C)  


### 4) Aufgabe 3 der Hausaufgabe: Hauptinhalt der Stunde 

```python
from gensound import Sine

class Melodie:
    
    """
    __init__() ist der constructor, der immer beim Erzeugen eines neuen Objekts einer Klasse aufgerufen wird.
    """
    def __init__(self, toene): 
        self.toene = toene
        # 'self' ist ein Objekt (der Klasse 'Melodie'), steht immer an erster Stelle bei Def. von Methoden,
        # wird aber nie beim Aufruf von Methoden als Argument angegeben.
        # 'toene' ist ein Attribut (des Objektes 'self').
        # -> weitere Infos: Cheat sheet Klassen
        
        # self ist ein Alias für das Objekt.
    
    def erhoehen(self, intervall=1): # Das ist eine Methode, die jeden Ton der Melodie um das angegebene intervall erhöht.
        neue_toene = []
        for ton in self.toene:
            neue_toene.append(ton+intervall)
        self.toene = neue_toene
        
    def print_toene(self): # eine Methode, die die Töne einer Melodie ausgibt.
        print("Hier die Töne der Melodie:", self.toene)
        
    def ausgeben_fuer_sine(self): # eine Methode, die die Tonnamen einer Melodie ausgibt.
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
        # hier: Anfang mit B, weil wir dadurch das C der 1 zuordnen können,
        # so, wie das in der Musik normalerweise gemacht wird
        
        ergebnis = ""
        for ton in self.toene:
            ergebnis += tonnamen[ton] + " "  # '+=' bedeutet: 'ergebnis = ergebnis + ...'
        return ergebnis


    # Zusätzlich von uns in der Stunde definierte Methoden:
    
    # Baut eine Methode in die Klasse ein, die die Töneliste zweimal printet:
    def print_toeneListe_zweimal(self): # eine neue Methode
        print("Hier die Töne der Melodie 2x:", self.toene, self.toene)        
    
    # Baut eine Methode, die die Zahl des ersten Tons printet:
    def print_ersterTon(self):
        ersterTon = self.toene[0]
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
       
        print("Erster Ton der Melodie:", ersterTon)
        print("Name des ersten Tons der Melodie:", tonnamen[self.toene[0]])
        
    # Baut eine Methode, die die Länge der Melodie printet:
    def print_melodielaenge(self):
        print("Die Länge der Melodie ist:", len(self.toene) )
    


kleine_melodie = Melodie([5, 3, 1, 4, 5, 3, 1, 2, 3, 1, 2, 0, 1])

# Was bedeutet der Großbuchstabe bei Melodie?
# Melodie ist eine Klasse. Mit Melodie() wird ein Objekt der Klasse 'Melodie' erzeugt
# (dazu wird implizit  'Melodie.__init__()' ausgeführt).


kleine_melodie.print_toene()
# hier wird eine Methode aufgerufen. Beachte: kein 'self' als Argument!

kleine_melodie.print_toeneListe_zweimal()

kleine_melodie.print_ersterTon()

kleine_melodie.print_melodielaenge()


melodiestring = kleine_melodie.ausgeben_fuer_sine()
# Ist das wirklich ein string? um das herauszufinden:
# print(type(melodiestring))
# --> ja. Das sind Tonnamen im melodiestring.

print("So sieht die Ausgabe von 'ausgeben_fuer_sine' aus:", melodiestring)

# Folgendes kennen wir schon aus Aufgabe 2:
s = Sine(melodiestring, duration=500.0)*0.2
s.play() # Abspielen der Melodie
```

+ Cheatsheet zu den Klassen

* kleine Aufgaben: 
Baut eine Methode in die Klasse ein, die die Töneliste zweimal printet.
kleine_melodie.print_toeneListe_zweimal()
-> Tipp: vorhandene Methode kopieren und abwandeln.

Baut eine Methode, die den ersten Ton der Melodie printet.
Baut eine Methode, die die Länge der Melodie printet.


### 5) Aus dem Chat:
gesi schreibt:
**Kurze Zwischen-Anfrage nicht direkt zur Stunde:**
liebe alle, wir sind eine dreier-kleingruppe hier aus dem kurs und sind schon etwas wehmütig, dass der kurs bald vorbei ist. und weil wir wissen, dass uns alleine weitermachen nicht so gut gelingen wird, überlegen wir, wie wir vielleicht in zukunft selbstorganisiert noch ein wenig weiter zusammen programmieren und miteinander weiterlernen können. es gibt keine großen pläne bisher, aber die einladung, euch einfach noch anzuschließen und gemeinsam zu überlegen in welchem rahmen das nach dem kurs stattfinden kann. wer interesse hat kann gerne eine mail schreiben und dann können wir ja mal schauen wann wir uns wie sehen zum weitermachen.



![Klassen_Cheatsheet.png](..%2FCheatsheets%2FKlassen_Cheatsheet.png)

## Pikos Datei

```python
from gensound import Sine

s = Sine('C4 F E F# G3 B C', duration=500.0)*1.0
s[0].play()

#"C# DEFGABC"
# Deutsches C1 == Englische C4 "mittleres C"
```
```python
from gensound import Sine

class Melodie:
    def __init__(self, toene):
        self.toene = toene
    
    def erhoehen(self, intervall=1):
        neue_toene = []
        for ton in self.toene:
            neue_toene.append(ton+intervall)
        self.toene = neue_toene
        
    def print_toene(self):
        print("Hier die Töne der Melodie:", self.toene)  # Attribut
        
    def print_toeneliste_zweimal(self):
        print(self.toene, self.toene) 
        
        
    def print_ersten_ton(self):
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
        print("Die Zahl des ersten Tons ist:", self.toene[0]) 
        name_des_ersten_tons = tonnamen[self.toene[0]]
        print("Der Name des ersten Tons ist:", name_des_ersten_tons)
        
    def print_laenge_der_melodie(self):
        print("Die Länge der Melodie ist:", len(self.toene))
        
        
    def ausgeben_fuer_sine(self):
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
        
        ergebnis = ""
        for ton in self.toene:
            ergebnis += tonnamen[ton] + " "  # "G E C "
        return ergebnis


kleine_melodie = Melodie([5, 3, 1, 4, 5, 3, 1, 2, 3, 5, 0, 1])
kleine_melodie.print_toene()
kleine_melodie.print_ersten_ton()  

kleine_melodie.print_laenge_der_melodie()
# Die Länge der Melodie ist: 12


melodiestring = kleine_melodie.ausgeben_fuer_sine()
print("So sieht die Ausgabe von 'ausgeben_fuer_sine' aus:", melodiestring)

s = Sine(melodiestring, duration=500.0)*0.5
s.play()
```