# Pikos Python Kurs am 27.11.2023

## Pads
Ausprobierpad für Markdown, gerne drin rumkritzeln!  
https://md.ha.si/L5Nz2-cuQi-hz9ccLElFKQ?both

Das Importe-Cheatsheet findet Ihr im Ordner Cheetsheets.

System: Hedgedoc
Sprache: MarkDown (Auszeichnungssprache für Text)


Hier kann jede* eine Notiz anlegen: https://md.ha.si/


## Veranstaltungshinweise

- CCC Kongress Tickets Verkauf 
- kommendes Wochenende PyLadies Konferenz: https://conference.pyladies.com/
- Deadline für CCC Kongress Ausstellung: 15.12.

## Hausaufgaben

### Aufgabe 1

Für Aufgabe 1 hat dieser Code-Schnipsel noch gefehlt

```python
for regel in gebote_auswahl:
	print("Du sollst nicht " + regel[0].lower() + regel[1:] + ".")
    
```

Slicing Prinzip nochmal veranschaulicht:

```python
a = "1234567"

print("Gegeben ist a =", a)

b = a[0:3] # => "123"
print("b =", b)

c = a[3:8] # => "4567"
print("c =", c)
```
Ziemlich cool, bei Listen kann man auch rückwärts "zählen"

```python
liste = [10, 11, 12, 13]
print(liste[:-1]) 
```

### Aufgabe 2.2

(Aufgabe 2.1 kommt als Video)

Eintrag aus Liste entfernen:

```python
liste = [10, 11, 12, 13]

print(liste[:-1]) # - 1 zählt Rückwärts => letzter Listeneintrag

liste.remove(13)

print(liste)
```

Aus string int machen:

```python
s ="123"
print(int(s) + 1)
print(s)
```

Sortieren:

```python
l = ["a", "r", "b", "m", "A", "c", "d"]
l.sort(reverse=True) # sortiert absteigend
print(l)

l.sort() # sortiert aufsteigend
print(l)

l.reverse() # sortiert absteigend
print(l)
```
Unterschied zwischen extend und append:

l.extend(zusatz) 
Add the elements of a list (or any iterable), to the end of the current list

l.append(zusatz) 
Adds an element at the end of the list


# Import von Funktionen

Es gibt verschiedene Möglichkeiten zum Importieren von Funktionen

```python
from random import * 
# => alles vom random module wird importiert
# z. B. sample(), choices(), randint()

from random import sample 
# => hier wird nur sample aus random importiert

import random
# um eine Funktion aus dem Modul aufzurufen:
# z. B. random.sample(), random.choices(), random.randint()
```

random.sample() => bezieht sich auf ein Modul und ruft eine Funktion auf
l.sort() => bezieht sich auf eine Liste und ruft eine Methode auf

Mehr dazu im Cheatsheet



## Pikos Dateien

```python
from random import sample

quelle = """   Immer alles richtig machen
                Die Zähne putzen
Das Fusselsieb reinigen
Das Eisfach abtauen
Die Dichtungen mit Silikonöl behandeln
Zur Vorsorge-Untersuchung gehen
Das Verfallsdatum kontrollieren
Ein Backup machen
Lüften
Die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 Die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 Es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
Deine Eltern anrufen
 Bei Auszug renovieren
Die Kassette zurück spulen
Es 15 Minuten einwirken lassen
2m Rangierabstand lassen
     Es Fest verschließen
 Es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
Bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
Kein Öl in den Abfluss gießen
Es nur mit wasserlöslichen Schmierstoffen verwenden
      Sowieso nur zum äußerlichen Gebrauch"""


# print(quelle)
quellenliste = quelle.splitlines()
quellenliste.pop(31)

# # Oder, wenn wir davon ausgehen, dass "Sowieso nur zum ..." immer die letzte Zeile sein wird:
# quellenliste = quelle.splitlines()[:-1]

alle_gebote = []

for zeile in quellenliste:
    # die jeweilige Zeile strippen:
    gestrippte_zeile = zeile.strip()
    # Hier muss der erste Buchstabe der Zeile kleingeschrieben werden.
    
    kleingeschriebene_zeile = gestrippte_zeile[0].lower() + gestrippte_zeile[1:]
    
    # die fertig bearbeitete Zeile an die neue Liste anhängen:
    alle_gebote.append(kleingeschriebene_zeile)


gebote_auswahl = sample(alle_gebote, k=10)

print(gebote_auswahl)

# for schleifenvariable in wertevorrat:
for gebot in gebote_auswahl:
    print("Du sollst " + gebot + "!")
```
```python
a = "1234567"

b = a[:3] # => "123"
print(b)

c = a[3:]  # => "4567"
print(c)

liste = [10, 11, 12, 13]
l2 = liste[:-1]
l3 = liste[0:3]

print(l2)
```
```python
l = [5, 4, 9, 2, 1, 5, 7, 5, 8]

print(l)
dings = l.count(5)
print(l)
print(dings)
```
```python
l = ["a", "r", "b", "m", "A", 6, "c", "d", "B", 1]
print(l)
dings = l.sort()
print(l)
print(dings)
```
```python
from random import *  # => alles

a = sample(...)
b = choices(...)
c = randint(1, 6)



from random import sample

a = sample(...)



import random

a = random.sample(...)
b = random.choices(...)
c = random.randint(1, 6)
```
```python
from random import randint

a = randint(1, 6)



from random import random

b = random()
print(b)

```
```python
import random

a = random.random()
print(a)
```