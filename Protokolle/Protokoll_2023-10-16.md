# Pikos Python Kurs am 16.10.2023

## Protokoll
Sitzung 7 - von Tiles und Grids

Montag, 16. Oktober 2023
19:29

Organisatorisches:
-   Alle Infos, Hausaufgaben und Ausfälle gibt es im Pad

> https://gitlab.com/sudo_piko/pythonkurs-d

Hausaufgaben
-   Aufgabe 1 - Mandala
    -   Pad mit Mandalas:
> https://md.ha.si/cuv6MATUTCqjEiL_HyUKsw?both
-   Die Form der Turtle kann mit *shape()* verändert werden
    -   Z.B. *shape(\"turtle\")*
    -   Auch Größe und Farben der Turtle lassen sich verändern
        -   Unter den Begriffen "Python turtle shape ..."
            findet man im Internet verschiedenste Dinge dazu
    -   Mit *stamp()* kann die Form der Turtle abgestempelt werden
        -   Ähnlich wie *dot()* einen Punkt interlässt
        -   Mit *color()* kann auch die Farbe dieses Stempels verändert
            werden
            -   Z.B. *color(\"red\", \"green\")*
                -   Erstes Argument = Kontur, zweites Argument =
                    Füllfarbe
-   Die Fenstergröße der Turtle lässt sich mit *setup()* verändern
    -   Z.B. setup(800, 800, 2500, 28)
-   Aufgabe 2 - Tiles
    -   Programm mit einem Kreis in einem Viereck
        -   Dieses Programm hat drei Teile
            -   Import statement
            -   setup und einige Variablen im global scope
            -   Eigentliches Programm (mit *for*-Schleifen)
    -   Diese Aufteilung kann verändert werden
        -   Mit der *def*- Funktion/Stempel
            -   Funktionskopf schreiben (normalerweise in einer Zeile)
                - *Def*
                - *Name der Funktion*
                - *():*
            - Alle anderen Zeilen einrücken
-   Wichtig, das Schreiben der Funktion führt nicht zur Ausführung des
    Selbigen, weil es damit ins Setup rückt
    -   Damit die Funktion ausgeführt wird muss sie nocheinmal ins
        Porgramm geschrieben werden
-   Metapher dazu: Muffin-Rezept
    -   Das Setup bzw. die def-Funktionen ist eine Art Rezeptdatenbank
        -   Das Setup ist das Rezept und erst, wenn wir befehlen, dass
            es ausgeführt wird, werden auch Muffins gebacken
    -   *Def* ist quasi eine Möglichkeit eigenen Rezepte ins Rezeptbuch
        von Python zu schreiben
-   Man kann auch *def*-Funktionen in *def*-Funktionen verwenden
    -   Z.B. wenn man die eine *def*-Funktion zuvor schon gebaut hat
        -   Dabei darf man nicht vergessen, dass definierte Parameter
            aus der einen Funktion, auch in der nächsten Funktion neu
            definiert werden müssen
            -   Es sei denn es ist durch den global scope definiert

-   Tile-Tauschbörse
    -   https://md.ha.si/Qk9tc_6jSCWmNMlqvoTflQ
    -   Alle können hier def-Funktionen einstellen und andere
        def-Funktionen mit nutzen

-   Zum grid:
    -   Programm:
```python 
from turtle import *
pensize(10)
seitenlaenge = 100
pu()
for j in range(5):
    for i in range (7):
        goto((i*30, j*30)
        dot()
```
Dot() kann hier nun durch verschiedenste *def*-Funktionen
        ersetzt werden
        -   Dabei nicht vergessen, dass sich auch die Seitenlänge
            verändert!



## Pikos Datei
Diverse Erläuterungen am Anfang:
```python
from turtle import *

setup(800, 800, 2560, 20)
speed(1)
fd(100)

color("red", "yellow")

rt(360)
fd(100)

shape("turtle")
stamp()

fd(100)
# python turtle change shape  larger   longer  rectangle

color("blue")
```
```python
from turtle import *
# import turtle


setup(800, 800, 2560, 20)




def kreis_im_quadrat():
    seitenlaenge = 50

    for i in range(4):
        fd(seitenlaenge)
        lt(90)
            
    fd(seitenlaenge/2)

    circle(seitenlaenge/2)
    
kreis_im_quadrat()
```
```python
from turtle import *


pensize(3)
seitenlaenge = 100

setup(800, 800, 2560, 20)


# 1. Funktionskopf schreiben:
#     def
#     Name der Funktion
#      ():
# 2. Alle anderen Zeilen einrücken


def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)

def kreis_im_quadrat():
    quadrat()
            
    fd(seitenlaenge/2)

    circle(seitenlaenge/2)
    back(seitenlaenge/2)


kreis_im_quadrat()



def quadrat_mit_strich():
    quadrat()
    lt(45)
    fd(seitenlaenge * 1.414)  #Piko googelt kurz "Wurzel von 2" :D))
    back(seitenlaenge * 1.414)
    rt(45)
    
quadrat_mit_strich()


def quadrat_mit_streifen():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
    for i in range(6):
        lt(45)
        fd(0.65*seitenlaenge)
        bk(0.65*seitenlaenge)
        rt(45)
        fd(seitenlaenge/10)

    bk(seitenlaenge/2 + seitenlaenge/10)
    lt(90)
    fd(seitenlaenge)
    rt(90)
    fd(seitenlaenge)
    rt(180)
    for i in range(6):
        lt(45)
        fd(0.65*seitenlaenge)
        back(0.65*seitenlaenge)
        rt(45)
        fd(seitenlaenge/10)

    fd(4*seitenlaenge/10)
    lt(90)
    fd(seitenlaenge)
    lt(90)

quadrat_mit_streifen()

# Für alle, die das Protokoll anschauen, gibt es einen Muffin :D
def muffin():
    quadrat()
    # Basis
    fd(seitenlaenge/2)
    circle(seitenlaenge*0.6, 30)
    lt(50)
    fd(seitenlaenge*0.4)
    bk(seitenlaenge*0.4)
    rt(50)
    rt(180)
    circle(-seitenlaenge*0.6, 60)
    rt(50)
    fd(seitenlaenge*0.4)
    lt(50)
    
    # Oberteil
    circle(seitenlaenge*-0.2, 120)
    circle(seitenlaenge*-0.75, 60)
    circle(seitenlaenge*-0.2, 120)
    circle(seitenlaenge*-0.75, 60)
    
    # Schokostückchen
    rt(135)
    pu()
    fd(seitenlaenge*0.1)
    pd()
    circle(seitenlaenge * 0.05)
    pu()
    fd(seitenlaenge*0.5)
    pd()
    circle(seitenlaenge * 0.05)
    pu()
    # Zurück
    bk(seitenlaenge*0.6)
    lt(180+135-50)
    fd(seitenlaenge*0.4)
#     dot()  # Wo bin ich jetzt? Ich habe die Orientierung verloren...
    lt(50)
    circle(seitenlaenge*0.6, 30)
    bk(seitenlaenge/2)
    
muffin()
```

Thema: Wohin mit penup/pu und pendown/pd?
```python
from turtle import *
seitenlaenge = 50


def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)

def kreis_im_quadrat():
    pd()
    seitenlaenge = 50

    quadrat()

    fd(seitenlaenge/2)
    circle(seitenlaenge/2)
    pu()

def quadrat_mit_strich():
    seitenlaenge = 50
    pu()
    goto(100,200)
    pd()
    quadrat()
    for j in range(1):
        left(45)
        fd(70.71)

kreis_im_quadrat()
pu()
quadrat_mit_strich()
```

Grid
```python
from turtle import *


pensize(10)
seitenlaenge = 100
speed(10)
setup(800, 800, 2560, 20)

pu()

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)

def kreis_im_quadrat():
    pd()
    quadrat()
            
    fd(seitenlaenge/2)

    circle(seitenlaenge/2)
    back(seitenlaenge/2)
    pu()
    
def quadrat_mit_halbkreisblume():
    pd()
    for i in range(4):
        forward(seitenlaenge)
        left(90)

    for i in range(4):
        circle(seitenlaenge/2,180)
        left(90)
    pu()
    



for j in range(5):
    for i in range(7):
        goto(i * seitenlaenge, j * seitenlaenge)
        quadrat_mit_halbkreisblume()
```

Nochmal für die Nachzüglies ein kurzer Primer
```python


zahl = "42"
zahl2 = 984372.0

# print(type(zahl2*2))


# for dings in ("Hallo", "Tschüss", "Pythonkurs!"):
#     print(dings)
#     print("huiuiui")
#     


# print("Hallo")
# print("Tschüss")
# print("Pythonkurs!")



for i in range(10):
    print(i*"hallo")


# print(0)
# print(1)
# print(2)
# print(3)
```

