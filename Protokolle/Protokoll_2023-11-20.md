# Pikos Python Kurs am 20.11.2023

## Links
### Pad
Ideensammelpad für Ausstellung auf dem Congress:  
https://md.ha.si/0sMJtLdyRqqx_uVaT-p4XA#

### GeeksforGeeks
https://www.geeksforgeeks.org/difference-between-casefold-and-lower-in-python/  
https://www.geeksforgeeks.org/randomly-select-elements-from-list-without-repetition-in-python/

### Frage zu Rundung
Einer Person ist aufgefallen, dass Python sich beim Runden komisch verhält: 
`round(1.5)` rundet auf, also zu 2. Dann würden wir erwarten, dass `round(2.5)` zu 3 aufrundet - tut es aber nicht! Es rundet ab, auch zu 2. Warum das so ist, wird hier knapp erklärt:  
https://www.reddit.com/r/learnpython/comments/g0c8y1/why_does_round235_1_return_24_rounded_up_but/

Und hier etwas ausführlicher:  
https://softwareengineering.stackexchange.com/questions/256265/why-do-some-languages-round-to-the-nearest-even-integer

## Protokoll
1) Veranstaltungshinweise

-- Wenn eins Interesse an Tickets für den C3 (grooooße Zusammenkunft
von Menschen, die sich für Nerd-Zeug interessieren) in Hamburg Ende
Dezember hat, darf es sich sehr gerne (auch für FLINTA-Freunde) bei Piko
melden. **Piko vermittelt dann Haecksen-Vouchers, die zum Ticket-Kauf
berechtigen.** Der Ticket-Verkauf läuft in mehreren Runden. Das erste
Ticket-Kontingent wird am Donnerstag, 23. November 2023, online
erhältlich sein.

Infos und Tickets unter:

https://www.ccc.de/de/updates/2023/37C3

-- Nochmal der Hinweis auf die PyLadyCon Anfang Dezember; Kostenfreie
(!) Anmeldung und Infos unter:

https://conference.pyladies.com/

2) Ausstellung auf dem C3

Eins von uns plant, Kunstwerke aus dem Kurs beim C3 (siehe oben)
auszustellen.

Holt euch hier gerne kreative Impulse, falls ihr selbst etwas ausstellen
möchtet:

<https://md.ha.si/0sMJtLdyRqqx_uVaT-p4XA>#

Wer ausstellen möchte, wendet sich bitte --** BIS SPÄTESTENS 15.
DEZEMBER 2023 -** per Mail oder über Rocket-Chat an Ceryo.

Rocket: Ceryo

Bitte denkt auch daran, für euer Kunstwerk eine CreativeCommon-Lizenz zu
vergeben (oder zumindest darüber nachzudenken):

<https://creativecommons.org/licenses/?lang=de>

Kunstwerke können anonym ausgestellt werden oder eins gibt einen
Künstlernamen oder ein Social an.

3) Hausaufgaben

-- Zu einem Teil der Hausaufgaben wird Piko ein Video online stellen.
Das findet eins dann hier, sobald es fertig ist:

<https://diode.zone/c/pykurs_d/videos?s=1>

Tolle Tipps bei Schwierigkeiten oder offenen Fragen findet eins unter
anderem bei GeeksforGeeks:

<https://www.geeksforgeeks.org/>

Es kann sehr oft hilfreich sein, den Typ des Objekts bzw. der Objekte
anzeigen zu lassen, mit denen eins gerade arbeiten will. Dadurch kann
eins gegebenenfalls Rückschlüsse ziehen, warum der Code nicht tut, was
eins will.

`print(type(...))`

4) Zurück zum Gebote-Automaten von letzter Stunde

Wenn eins aus einer großen Liste einzelne Elemente „herausfischen" will,
kann eins dafür die Funktionen „sample" oder „choices" nutzen.

Gemeinsamkeiten:
- kommt beides aus dem Modul „random": Braucht also den import, zB `from random import sample`

| Sample                                                                   | Choices                                                                     |
|--------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| Wählt zufällig eine vorgegebene Anzahl an Elementen aus einer  Liste aus | Wählt zufällig eine vorgegebene Anzahl an Elementen aus einer Liste aus     |
| Wiederholt Elemente nicht!                                               | Wiederholt Elemente!                                                        |
| zB 1 bis 10 =>   4, 9, 7                                                 | zB 1 bis 10 => 3, 9, 3                                                      |
| („Alex muss nur ein Mal putzen.")                                        | („Wenn Alex Pech hat, muss er die ganze Wohnung alleine putzen.") |

Die vorgegebene Anzahl an Elementen wird durch „k" ausgedrückt. Wenn „k
= 3", dann müssen, zum Beispiel, drei Leute putzen. Wenn in der Liste
aber nur zwei Leute vorkommen, dann kommt „sample" damit nicht klar. Bei
der Funktion „sample" muss die Länge der Liste größer oder gleich groß
sein als die Anzahl an Elementen, die eins „rausfischen" will.

Python zeigt dir die Länge der Liste, wenn du „print(len(listenname))"
eingibst.

Wenn du als bei Tupeln (mit runden Klammern) nur ein einziges Element angibst und danach kein Komma
setzt, dann erkennt Python nicht, dass du eine Liste machen willst. Es
zählt dann stattdessen die Buchstaben in dem Element anstelle der Länge
des Tupels. Bei Listen ist das nicht so, da reichen die eckigen Klammern und es braucht kein Komma, wenn die Liste nur ein Element enthält.

```python
dings = ("aaa")  # => "aaa", also ein String
len(dings)  # => 3 (drei Elemente: a, a, a)
dongs = ("aaa",)  # => ("aaa",), also ein Tupel
len(dongs)  # => 1 (ein Element: "aaa")
dings2 = ["aaa"]  # => ["aaa"], also eine Liste
len(dings2)  # => 1 (ein Element: "aaa")
```


5) Wie For-Schleifen aussehen müssen, damit sie funktionieren:

wertevorrat = \[„schleifenvariable", „schleifenvariable",
„schleifenvariable", „schleifenvariable"\]

for schleifenvariable in wertevorrat:

tu dies(schleifenvariable)

und tu das(schleifenvariable)

## Pikos Datei
### Hausaufgabe
```python
# Amsel Drossel Fink Star => ["amsel", "drossel", "fink", "star"]

ausgangsstring = "Amsel Drossel Fink Star"
liste = ausgangsstring.split()
print(liste)

ergebnisliste = []

for wort in liste:
    neues_wort = wort.lower()
    ergebnisliste.append(neues_wort)
#     ergebnisliste = ergebnisliste + [neues_wort]

print(ergebnisliste)

#split(", ") führt zu ['Amsel Drossel', 'Fink Star']
```
```python
# Amsel Drossel Fink Star => ["Amselamsel", ...

ausgangsstring = "Amsel Drossel Fink Star"
liste = ausgangsstring.split()
print(liste)

ergebnisliste = []

for wort in liste:
    doppelwort = wort + wort.lower()
    ergebnisliste.append(doppelwort)
#     ergebnisliste = ergebnisliste + [neues_wort]

print(ergebnisliste)#  => ['Amsel Drossel', 'Fink Star']
```
```python
s = "FlAmAnGoLD"
print(s.swapcase())

s = "FlAmAnGoLD"
neu = s.swapcase()
print(neu)

# suchen: python strings convert upper to lower and lower to upper case
```
Der Putzplan:
```python
from random import sample

leute = ["Alex", "Maxi", "Jascha", "Robin", "Kim"]
print(type(leute))
putzen = sample(leute, k=3)

print(type(putzen))
print("Heute sind mit Putzen dran:", putzen)

for person in putzen:
    print("Du sollst " + person + " beim Putzen helfen!")
```
### Gebote-Automat
```python
from random import sample

quelle = """   immer alles richtig machen
                die Zähne putzen
das Fusselsieb reinigen
das Eisfach abtauen
die Dichtungen mit Silikonöl behandeln
zur Vorsorge-Untersuchung gehen
das Verfallsdatum kontrollieren
ein Backup machen
Lüften
die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
deine Eltern anrufen
 Bei Auszug renovieren
die Kassette zurück spulen
es 15 Minuten einwirken lassen
2m Rangierabstand lassen
     es Fest verschließen
 es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
Kein Öl in den Abfluss gießen
es Nur mit wasserlöslichen Schmierstoffen verwenden
      Sowieso nur zum äußerlichen Gebrauch"""


# print(quelle)
quellenliste = quelle.splitlines()

alle_gebote = []

for zeile in quellenliste:
    # die jeweilige Zeile strippen:
    gestrippte_zeile = zeile.strip()
#     print(neu[0].lower())
    kleingeschriebene_zeile = gestrippte_zeile[0].lower() + gestrippte_zeile[1:]
    # die gestrippte Zeile an die neue Liste anhängen:
    alle_gebote.append(kleingeschriebene_zeile)


gebote_auswahl = sample(alle_gebote, k=10)

print(gebote_auswahl)

# for schleifenvariable in wertevorrat:
for gebot in gebote_auswahl:
    print("Du sollst " + gebot + "!")
```
### Strings sind unveränderlich
```python
s = "hAUbenlAUcher"

s2 = s.replace("AU", "EI")

print(s)
print(s2)


l = [1, 2, 3]
l.append(4)
print(l)
```