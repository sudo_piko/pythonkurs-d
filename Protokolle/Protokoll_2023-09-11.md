# Pikos Python Kurs am 11.09.2023


## Pad für Kursnamen
https://md.ha.si/WxIt6t6xSrechO1xJzyqTA#

## Protokoll

1) Organisatorisches:

- bei Abwesenheit ist Bescheid zu sagen nicht nötig!
- Anwesenheit wird nicht geprüft und ist nicht verpflichtend; wenn es am Ende ein Zertifikat gibt, werden wir das selbst bauen.
- Der "pythonkurs-d" sucht einen Namen beginnend mit D (gerne ein Fabelwesen); Vorschläge können noch bis einschließlich 18. September '23 in folgende Liste eingetragen werden: 

https://md.ha.si/WxIt6t6xSrechO1xJzyqTA?view

- Wer während des Kurses mal auf's Klo muss, darf den Handzeichen-Knopf unten rechts am Bildschirm nutzen, um Piko darauf aufmerksam zu machen

2) Hausaufgabe:

-- Rechnen mit Python -- 

Zitat Piko: "Zwei Sternchen sind doppelt so krass wie Multiplizieren."; ** bedeuten "hoch"

Beispiel: 4**2 bedeutet "Vier hoch Zwei"

Python rechnet nach der Regel "Punkt vor Strich", deshalb: Klammern verwenden (wo gewünscht)!

Der Vorgang 5//2 nennt sich "ganzzahliges Teilen", das bedeutet, dass keine Kommastellen im Ergebnis angezeigt werden.

Beispiel: 5/2 ist gleich 2,5 ; Aber 5//2 ist gleich 2

Der Vorgang 5%2 nennt sich "Modulo", das bedeutet, dass im Ergebnis NUR ein Rest angezeigt wird, der beim Teilen "übrig" bleibt.

Beispiel: Die 2 hat nur zwei Mal Platz in der 5; Es bleibt dann ein Rest von 1 übrig.

Vorsicht: Im englischsprachigen Raum (also in Python!) werden anstelle von Kommas (zum Beispiel bei Nachkommastellen) Punkte verwendet!!!

Beispiel: 2.5 anstatt 2,5 (im Deutschen)

Zitat Piko: "Dinge, die man nicht sieht (zum Beispiel Leerzeichen oder die Tabulator-Taste), beeinflussen Python normalerweise nicht. In Einzelfällen jedoch schon, also seid wachsam!"

-- Turtle --

Warum wir vor der Nutzung von Turtle das Kommando "from turtle import *" eingeben:

:Lebensmittel-Analogie:

Built-in-Funktionen (liegt schon in der Küche) / Standard Libraries (muss man noch aus der Vorratskammer holen) / herunterladbare Libraries (gibt's im Supermarkt zu kaufen)

Die Mehrzahl der Funktionen, die eins mit Turtle nutzen kann, benötigen in Klammern so genannte Argumente. 

Im Falle der Funktion 'circle' sind es - bis zu - drei:

Das - erste - bestimmt den Radius des Kreises, den eins zeichnen lassen will

Das - zweite - bestimmt, wie weit der Kreis gezeichnt wird (zum Beispiel ein Viertel, zur Hälfte, vollständig, oder sogar mehrere)

Das - dritte - bestimmt, wie viele Ecken der "Kreis" bzw. das Vieleck haben soll

Setzt eins ein Minus vor das zweite Argument, ändert sich die Richtung, in die der Kreis gezeichnet wird (quasi rückwärts)

Die Fehlermeldung "...turtle.terminator..." bedeutet, dass das Zeichenfenster geschlossen wurde, dass Turtle / Python also keine Fläche hat, auf der es weiterzeichnen kann. Einfach ein neues öffnen und neu beginnen.

Auch nützlich:

Der "Stopp"-Knopf oben rechts in Thonny bricht den Zeichenvorgang ab.

Das Kommando 'exitonclick()' sorgt dafür, dass das Zeichenfenster erst durch einen Klick geschlossen wird, anstatt sich nach dem Ende des Zeichnens selbständig zu schließen.

Das Kommando 'reset()' löscht das gesamte Bild. 

Das Kommando 'undo()' löscht lediglich den letzten vorangegangenen Striche.

Für viele Funktionen gibt es Abkürzungen oder mehrere Bezeichnungen, zum Beispiel  'fd' für 'forward' oder 'width' statt 'pensize'.

-- Informationen suchen --

Zitat Piko: "Youtube ist oft nicht so sinnvoll, wenn man Hilfe bei einem Python-Problem sucht, weil die Darstellungen dort sehr ausführlich sind."

Zitat Piko: "Manche Hilfe-Seiten sind für Kinder gestaltet, lasst euch davon nicht abhalten!"

3) Neuer Lernstoff:

-- Turtle --

Es gibt verschiedene Arten von Dingen, die man für Python-Befehle nutzen kann. Im Wesentlichen:

Literale und Variablen

Literale, das sind "echte Dinge" wie Zahlen (integer: 8; float: 8.0) oder Text ("Hallo Welt!").

Variablen sind wie Boxen, die eins mit Inhalten befüllt. (Argumente sind aber KEINE Boxen!!!)

Beispiele für Boxen, die eins erstellen kann: 

Dings = 5

Dongs = 10

box1 = "radiergummi"

Zeichen bzw. Worte oder Wortschnipsel in einer Box nennt eins "string" (Deutsch: Zeichenkette)

-- Hedgedoc --

Mit Copy & Paste oder als Link lassen sich Bilder ins Hedgedoc einfügen.

Mit Formatierungszeichen wie einem # lassen sich Dinge formatieren bzw. hervorheben und zum Beispiel als Überschrift festlegen.



## Pikos Notizen
// Ganzzahliges Teilen
% Modulo

### Verschiedene Sorten von Bibliotheken
Built-in , zB print()
Standard Library, zB turtle
Libraries, zB gensound


Funktionen: funktionenname()
Argumente: funktion3(argument)
funktion4(argument1, argument2, argument3)

Variablen


## Pikos Datei
```python
from turtle import *

speed(0)
color("red")
schleifendurchlaeufe = 5000
kreisradius = 160

for i in range(schleifendurchlaeufe):
    circle(kreisradius)
    forward(5)
    left(5)
    

exitonclick()
```