# Pikos Python Kurs am 05.02.2024

Fortsetzung Farbratespiel

Problem:
-   Noch tun Teile unseres Spieles so als gäbe es nur einen Spieler
    -   In der letzten Sitzung haben wir bereits Spielende in einem
        Dictionary angelegt
    -   Jetzt wird es Zeit etwas mit dem Dictionary zu tun
-   Rateversuche
    -   Wir können die Raterunden in ein Dictionary packen:\

```python
rateversuche = {}
for person in spielende: 
    rate_string = input("Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
    print("Dein Rateversuch: " + rate_string)
```

-   Neue Begriff: Literal
    -   Ein echter/konkreter Wert, z.B. der String \"Faltrad\"
        -   Variablen sind z.B. KEINE Literale, weil es auf etwas
            anderes verweist, eine Variable kann aber z.B. auf ein
            Literal verweisen
    -   Auch ein Dictionary und sein Inhalt sind ein Literal?
-   Neues zu Dictionarys
    -   Der Name kommt nicht von Ungefähr dictionary - engl. Für
        Wörterbuch
    -   Z.B. :\
        deutsch_nach_englisch = {\"rot\":\"red\", \"gelb\":\"yellow\"}
    -   WICHTIG: ich kann immer nur vom Key zum Value gehen, also
        hier von Deutsch nach Englisch, Englisch nach Deutsch geht
        NICHT, dafür braucht es ein anderes Dictionary!
-   Jetzt brauchen also alle Spielende einen Rateversuch:

```python
# Raten lassen
rateversuche = {}
for person in spielende: #Alex, Kim, Robin
    versuch = input("Rateversuch " + person + ": ")  # zB "30 89 10 "
    print("Dein Rateversuch: " + rate_string)
    rateversuch[person] = versuch
```

-   Jetzt können wir einen ganzen Teil des Codes ersetzen und in einer
    neue Funktion speichern:

```python
 # Raten lassen
rateversuche = {}
for person in spielende: #Alex, Kim, Robin
    versuch = input("Rateversuch "+ person + ": ")  # zB "30 89 10 "
    print(person + "s Rateversuch war: " + rate_string)
    versuch = versuch_verarbeiten(versuch)
    rateversuch[person] = versuch
```

-   Die Variable rate_string fällt nun weg und kommt in eine neue
    Funktion:

```python
def versuch_verarbeiten(rate_string):
    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]
    ergebnis = tuple(farbe_geraten)
    return ergebnis
```

-   Kurze Anmerkung zur Funktionen:

    -   Es ist eine Konvention, dass in der ersten Zeile nach der
        Kopfzeile einer Funktione eine Erklärung folgt, was die Funktion
        tut
-   
- Jetzt können wir uns überlegen wie wir die Rateversuche anzeigen:
    -   Frage: Wie wollen wir die Dots in den geratenen Farben aufmalen,
        um sie mit der Zufallsfarbe zu vergleichen?
        -   Siehe auch Video dazu
 
-   Und jetzt können wir auch die Reaktion auf das Ergebnis in eine
    Funktion packen

```python
def reagieren(rateversuche_dict, spielende_dict, farbe_richtig):
    for person in rateversuch_dict:
        farbe_geraten = rateversuch_dict[person]

    # Ergebniswerte ausrechnen
    differenz = farben_differenz(farbe_richtig, farbe_geraten)
    punkte = punkte_ausrechnen(differenz)
    spielende_dict[person] = spielende_dict[person] + punkte
    
    # Auf Ergebnis reagieren
    print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57
    print("Du lagst um", differenz, "daneben.")
    bewerten(differenz)
    print("Du bekommst", punkte, "Punkte!")
    print("Du hast jetzt", punktestand, "Punkte.")
    print()
```


-   Jetzt kann man das dict noch flexibel gestalten

    -   So muss man nicht immer in den Code, um die Spielenden
        einzugeben:


```python
spielende = {}
while True:
    name = input("Wie heißt du? (weiter mit 'Weiter')")
    if name.lower() == "weiter":
        break
    spielende[name] = 0
```

## Pads

Literal als neues Wort kommt in unsere Wörterliste:  
https://md.ha.si/03OdwDaaQdC3kC7qZ7P7eg#

## Pikos Datei

Wir hatten in der Stunde die Frage, was genau 'Literale' sind. Ich habe ein paar Erklärungen gesucht, die meisten sind leider auf Englisch. Viele erklären, dass Literale "raw data" sind, also der direkte Blick auf Werte, die nicht irgendwie über einen Variablennamen vermittelt werden. Sozusagen nackte Werte oder die Inhalte von Variablen; der String "hallo", ohne die Variable `gruss = "hallo"`.  
Wenn wir `wunsch = "Croissant"` schreiben, wird in `wort` ein Literal reingespeichert – `wort` selbst ist aber eine Variable. Wenn wir dann `wirklichkeit = wunsch` schreiben, ist `wunsch` eben eine Variable, die auf das Literal "Croissant" zeigt... Literale sind also ganz konkrete Sachen; zum Beispiel:
* Die Zahl `5` oder `5.8`
* Der String `"Haselnussplätzchen"`
* Ein Boolean `True` oder `False`
* Ein Dictionary `{"Apfel": 3, "Birne": 5, "Banane": 2}` oder eine Liste `[3, 5, 2]`, oder ein Tupel `(200, 150)`
* Der Wert `None`

Wir jonglieren ganz viel mit Variablen und Sachen, die Python für uns ausrechnen soll, aber (oft ganz am Anfang) müssen wir Python irgendwelche Werte geben, die es dann verarbeiten kann. Das sind Literale.

```python
fahrradkeller = {"Rennrad":3, "Hollandrad":5, "Lastenrad":3}

fahrradkeller["Faltrad"] = 27  # 27 ist ein Literal
# rateversuche[person] = versuch

print(fahrradkeller)
```

Dictionaries sind wie Wörterbücher:
```python
deutsch_nach_englisch = {"rot":"red", "grün":"green", "gelb":"yellow"}
#                          key : value
```
```python

```
```python

```
```python

```
```python

```
```python

```
```python

```