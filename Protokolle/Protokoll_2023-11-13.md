# Pikos Python Kurs am 13.11.2023

## Diverses
Die Webseite:  
https://www.w3schools.com/python/python_strings_methods.asp


Wer entscheidet bei Python? Guido van Rossum, der benevolent dictator for life
IDE = Programm zum Programmieren, zB Thonny

## Protokoll
1) Veranstaltungshinweise:

Zu Kongressen oder lokalen Haecker*innen-Treffen zu gehen, ist immer eine gute Möglichkeit, Leute, mit denen eins online zu tun hat, auch im realen Leben kennenzulernen.

Es lohnt sich außerdem, auch mal Vorträge anzuhören, zu denen eins thematisch (noch) keinen Bezug hat bzw. noch kein Vorwissen mitbringt.

- PyLadiesCon: 1. bis 3. Dezember 2023; (Für Deutschaland) nur online, überwiegend nachts

Veranstaltung mit feministischem Themenschwerpunkt; Infos zum Event:

https://conference.pyladies.com/


2) Hausaufgaben:

Zum Erklärvideo geht's hier:

https://diode.zone/w/9cfM5GFa75kBifEp3fmd3b


3) Neuer Inhalt:
####  Paradigmen 

Bei Programmiersprachen lassen sich im Wesentlichen drei Paradigmen
(Stile) unterscheiden:

- 1 -- Imperativ

→ Tu dies! Tu das! Tu jenes! (Befehle)

→ Assembler, Fortran und Cobol sind Beispiele für
Programmiersprachen, die auf das imperative Programmierparadigma
ausgelegt sind.

- 2 -- Funktional
→ zB Haskell
→ Funktionen bauen

- 3 -- Objektorientiert

→ Objektorientiertes Programmieren gilt als die modernste Art. Manche
sehen in ihr jedoch lediglich eine Unterart des funktionalen
Programmierens. Es herrscht Uneinigkeit über die Begrifflichkeit(en).

Manche Programmiersprachen erzwingen einen bestimmten Programmierstil.
Mit Python lässt sich in allen drei Stilen schreiben.

Grundsätzlich ist Python aber objektorientiert. **Alle „Dinge" in Python
sind Objekte. **

-- Methoden --

Eine **Methode** ist die „kleine Schwester" der Funktion, das heißt, sie
verhält sich ähnlich wie eine Funktion, ist aber stets auf ein Objekt
bezogen. Funktionen und Methoden werden als „Callable" bezeichnet, manchmal auch „Funktionen im weiteren Sinn“. Eine Sammlung von Methoden, die sich auf den Objekttyp „String" anwenden lassen, findet eins hier:

<https://www.w3schools.com/python/python_strings_methods.asp>

Manche Methoden verändern das Objekt, auf das sie sich beziehen. Andere
tun dies nicht.

→ Solarium-vs-Orangensaftpresse-Analogie

Bei !! String-Methoden!! wird das Objekt üblicherweise nicht verändert.

Wenn du eine Funktion nutzen möchtest, benutze bevorzugt eine
vorhandene, anstatt selbst eine zu bauen. Vordefinierte Funktionen sind
im Regelfall effizienter, das bedeutet, sie benötigen weniger
Rechenleistung und werden schneller ausgeführt.

-- Der Gebote-Automat --

Wir haben begonnen, einen Gebote-Automaten zu bauen.

Dazu haben wir bisher folgende Schritte (Methoden) auf einen
„Textbatzen" angewendet:

(1) Zeilen trennen (quelle.splitlines)

(2) for-Schleife zum „Leerzeichen strippen" bauen


## Pikos Dateien
Objekte

objektorientierte Programmiersprache


Programmierstile:
- imperative Programmierung: 
- funktionale Programmierung:
- objektorientierte Programmierung: Java

In Python kann eins in allen drei Stilen programmieren, aber eigentlich ist es für objektorientierte Programmierung gedacht. In Python ist _alles_ ein Objekt.

Funktionen, die an Objekten hängen: METHODEN


### Methoden-Beispiele
```python
wort = "Hallo"

wort2 = wort.upper()  # Methode, die das Objekt nicht verändert
print(wort2)

anzahl = wort.count("l")
print(anzahl)

print(wort.center(50))
```
_keine_ einzige String-Methode verändert die zugehörigen Objekte – denn Strings sind "unveränderliche" Objekte...
Viele andere Objekttypen sind veränderlich, zB Listen. Das macht
sie zu interessanterem Spielzeug, aber manchmal ist das auch sehr
nervig...

Beispiel für Veränderung durch Methoden:
```python
liste = ["eins", "zwei", "drei"]

liste.append("vier")  # Methode, die das Objekt verändert

print(dings)
print(liste)
```


Dass sich das Objekt "liste" verändert, kann eins hieran ganz gut sehen
```python
liste = ["eins", "zwei", "drei"]

liste.append("vier")  # Methode, die das Objekt verändert

print(dings)  # => None: Es wird nix neues erzeugt
print(liste)  # => erweiterte Liste: liste wurde verändert
```


Mehr String-Methoden: https://www.w3schools.com/python/python_strings_methods.asp

Anwendungen:
```python
s =  "   Hallo   !      .     "

t = s.strip()  # strip nimmt Leerzeichen weg
print(t*5)  # Durch das `*5` sieht eins recht gut, welche Leerzeichen weg sind.
```

```python
a = "32 "
print(a.isnumeric())  # => bool

b = "pandadata"
print(b.isalpha())  # => bool

c = "uae ueai eaiu eau"
print(c.split())  # => list

d = "IMMER ALLES RICHTIG MACHEN"
print(d.lower())  # => str

e = "Erstsemesterbestenlese"
print(e.find("e"))  # => int
print(e.count("e"))
```






### Der Gebote-Automat
Ziel:
``` 
Du sollst die Zähne putzen!
Du sollst für den Urlaub sparen!
Du sollst deine Eltern anrufen!
Du sollst den Tag nicht vor dem Abend loben!
```
Quelle:
``` 
immer alles richtig machen
die Zähne putzen
das Fusselsieb reinigen
das Eisfach abtauen
die Dichtungen mit Silikonöl behandeln
zur Vorsorge-Untersuchung gehen
das Verfallsdatum kontrollieren
ein Backup machen
Lüften
    die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
deine Eltern anrufen
 Bei Auszug renovieren
die Kassette zurück spulen
es 15 Minuten einwirken lassen
2m Rangierabstand lassen
 es Fest verschließen
 es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
     Öl nicht in den Abfluss gießen
es Nur mit wasserlöslichen Schmierstoffen verwenden
           Sowieso nur zum äußerlichen Gebrauch
```

Wie for-Schleifen aussehen:
```python
wertevorrat = [1, 2, 3, 4, 5]
for variable in wertevorrat:
    tu_dinge(variable)
```


```python
quelle = """immer alles richtig machen
die Zähne putzen
das Fusselsieb reinigen
das Eisfach abtauen
die Dichtungen mit Silikonöl behandeln
zur Vorsorge-Untersuchung gehen
das Verfallsdatum kontrollieren
ein Backup machen
Lüften
die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
deine Eltern anrufen
 Bei Auszug renovieren
die Kassette zurück spulen
es 15 Minuten einwirken lassen
2m Rangierabstand lassen
 es Fest verschließen
 es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
Öl nicht in den Abfluss gießen
es Nur mit wasserlöslichen Schmierstoffen verwenden
Sowieso nur zum äußerlichen Gebrauch"""

# => ["Knicken und abreißen", "Fest zubeißen", ...

a = quelle.splitlines()  # kann weniger => wahrscheinlich einfacher
b = quelle.split("\n")  # kann mehr => wahrscheinlich komplexer aufgebaut

print(a == b)  # Ist a das gleiche wie b? => True
```

```python
quelle = """   immer alles richtig machen
                die Zähne putzen
das Fusselsieb reinigen
das Eisfach abtauen
die Dichtungen mit Silikonöl behandeln
zur Vorsorge-Untersuchung gehen
das Verfallsdatum kontrollieren
ein Backup machen
Lüften
die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
deine Eltern anrufen
 Bei Auszug renovieren
die Kassette zurück spulen
es 15 Minuten einwirken lassen
2m Rangierabstand lassen
     es Fest verschließen
 es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
Öl nicht in den Abfluss gießen
es Nur mit wasserlöslichen Schmierstoffen verwenden
      Sowieso nur zum äußerlichen Gebrauch"""

quellenliste = quelle.splitlines()

# strip schneidet die Leerzeichen nur ganz am Anfang des Strings ab.
# Ich brauche also einzelne Strings, die ich *alle* *einzeln*
# strippen muss... Ich muss also durch die gesamte Liste gehen,
# jedes einzelne Element der Liste in die Hand nehmen, und strip
# drauf ausführen...

neue_liste = []

for zeile in quellenliste:
    # die jeweilige Zeile strippen:
    neu = zeile.strip()
#     print(zeile)
#     print(neu)
    # die gestrippte Zeile an die neue Liste anhängen:
    neue_liste.append(neu)

print(quellenliste)
print(neue_liste)
```