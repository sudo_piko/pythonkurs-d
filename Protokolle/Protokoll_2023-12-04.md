# Pikos Python Kurs am 04.12.2023

## Ankündigungen
Es gibt eine Auffrischungsstunde für Fadenverlorene und Neudazugekommene am 19.12. um 19:00.

Die Musterlösung für die Aufgabe 2 von letzter Woche findet sich in [Aufgaben_2023-11-27.md](..%2FAufgaben%2FAufgaben_2023-11-27.md)


## Protokoll

### 1. Veranstaltungen/ Hinweise
#### Kurse über die Feiertage
**am 25.12 und am 01.01.2024 findet kein Kurs statt!**
Um nicht 3 Wochen Python-frei leben zu müssen bekommen wir ein Video online gestellt, dass wir bearbeiten können wann es uns passt. 

#### kurzer Primer - Die Neueinsteiger oder Aussteiger Runde
**Dienstag 19.12.2023 um 19Uhr**
Für alle Neueinsteiger*innen und Kursteilnehmer*innen die den Faden verloren haben. In ein-einhalb Stunden erklärt Piko nocheinmal was bisher geschah. 


#### Coding Adventskalender
https://adventofcode.com/
kann sehr herausfordernd sein, nicht davon frustrieren lassen aber gerne mal vorbeischauen.

#### C3 Congress Ausstellung
**bitte bis 15.12 Bescheid geben** was wer beim Congress "ausstellen" möchte. 

oder Ceryo bei Rocket

### 2. Hausaufgaben
#### Aufgabe 1

Vorgehen:
Pre-Processing braucht sehr viel Zeit. Gerade in der echten Welt muss man wirklich erst mal untersuchen, ob die daten in einer sauberen Form vorliegen. 

Im ersten Schritt werden also ersteinmal die beiden Listen 'brauchbar' gemacht.
```python
import random

nomen = """Wünsche
Träume
Ängste
Ziele
Freunde
Hoffnungen
Fragen
Antworten
Träume, 
Erfahrungen, 
Geschichten, 
Missgeschicke, 
Zähne, 
Freunde, 
Verwandte, 
 Kinder, 
 Geister, 
Bücher, 
Koffer, 
Betten, 
Essen, 
Käse, 
Gesetze, 
Fässer, 
Weine, 
Haare, 
Beine, 
Klischees, 
"""

praedikate = """gehen auf
frieren im Winter
krümeln
brauchen Schokolade
haben keine Ahnung
fallen nicht weit vom Stamm
machen allein auch nicht glücklich
ersetzen einen Zimmermann
wurden von der Werbeindustrie frei erfunden
heilen alle Wunden
teilt man gern unter Freunden
machen noch keinen Sommer
werden auch von Vögeln gemieden
fügen sich ganz von allein
ergeben einen schmackhaften Auflauf
haben nur andere Vorzeichen"""

praedikatliste = praedikate.splitlines()
nomenliste_unbereinigt = nomen.splitlines()
nomenliste_bereinigt = []

for nomen in nomenliste_unbereinigt:
    nomen = nomen.strip()
```
*jetzt müssen wir die Komata loswerden. 
Wir wollen also erst einmal wissen, ist das Letzte Zeichen ein Komma?*
Entweder:
```python 
nomen = nomen.rstrip(",")
```

Oder:

```python
if nomen [-1] == ",":
   nomen = nomen[:-1]
nomenliste_bereinigt.append(nomen)
```
**if nomen [-1] == ",":**  
*hier wird nur betrachtet ob das allerletzte Zeichen ein Komma ist, der Körper von dem if wird nur dann ausgeführt, wenn es sich um ein Komma handelt sonst nicht und es wird direkt mit .append fortgefahren*  
**nomen = nomen[:-1]**  
 *dieser Teil ist wichtig, weil ich sonst nur die Kommata auswähle*

Nach dem wir nun den Text geklärt haben, generieren wir Weisheiten: 
Code:
```python
nomen1 = random.choices(nomenliste_bereinigt, k = 1)[0]
nomen2 = random.choice(nomenliste_bereinigt)
praedikat = random.choice(praedikatliste)
```
**nomen1 = random.choices(nomenliste_bereinigt, k = 1)[0]**  
Choice**s** wählt mehr aus. Durch den Nachtrag [0] (Index) wählen wir nur das nullte Element
(Metapher Pralinenschachtel: Hier wird Praline0 aus der Schachtel gegriffen)  
**nomen2 = random.choice(nomenliste_bereinigt)**  
choic**e** wählt automatisch nur eins

Zum Generieren der Weisheit fehlt dann nur noch: 

```python
print(nomen1 + " sind wie " + nomen2 + ": sie " + praedikat + ".")
```

Nochmal betont: Python rechnet es hier Stückchen für Stückchen aus, deshalb kann ich an eine random.choice auswahl aus einer liste auch einfach eine [0] index anhängen

Man könnte also auch `nomen = nomen.strip().rstrip(",")` schreiben, aber das ist alles sehr verwirrend und deshalb ist es einfacher alles Schritt für Schritt Stückchenweise zu gehen.

```python
nomenliste_bereinigt.append(nomen.sprip().rstrip(","))
```
→ sieht man soetwas in einem Code hat sich vorher meistens jemand den Anfang im Detail hingeschrieben 
→ **Code golfing** heißt, dass man versucht einen möglichst kurzen effizienten Code zu schreiben, der ist aber dann meist komplex nachzuvollziehen/schwer lesbar, und es gilt im allgeminen nicht als fein in Python

##### Pralinenschachtel Metapher:
```python=
pralienenschachtel = ["praline0", praline1", "praline2", "praline3"]
print(pralinenschachtel[1])
print(pralinenschachtel[2:])                      
```
**(pralinenschachtel[1])**  
Hier bekomme ich nur den Inhalt. Also nur eine Praline aus der Schachtel

**print(pralinenschachtel[2:])**  
Hier bekomme ich eine Schachtel drum rum. Also eine Neue Pralinenschachtel mit den ausgewählten Pralinen 2 bis 3
```python
liste =[1, 2, 3, 4, 5]
liste[1] #--> Hier kriege ich eine einzige Praline ohne Schachtel
liste[:1] #--> Hier kriege ich eine Praline in einer Neuen Schachtel
liste[:3] #--> Hier habe ich mehrere ausgewählte Pralinen in einer neuen Schachtel
```

##### Wiederholung Slicing und Indexing
`a = "0123456`

`a[1:]`  
vom "einsten" Element bis zum letzten (also im Beispiel die "1", weil das vorderste, "nullte" Element die "0" ist)
`a[:6]`  
alles bis zur fünf ohne die 6 
→ exklusive der 6 
→ alternative: [a:-1]
Vorteil: man muss nicht drübernachdenken und rumrechnen das wieviele Element jetzt das letzte ist.
```
>>> a = "0123456"
Beispiele: 
a[0]
'0'
>>> a[1]
'1'
>>> a[3:5]
'34'
>>> a[-1]
'6'
>>> a[-2]
'5'
>>> a[2:4]
'23'
>>> a[6]
'6'
>>> a[-1]
'6'
>>> a[:6], a[:-1]
('012345', '012345')
```

Unterschied einmal mit doppelpunkt  einmal ohne [:-1]  [-1]  
[-1]] nimmt nur dieses Element  
[:-1] ist ein Abschnitt

Je öfter man das selbst macht desto klarer wird es.


### Sonstiges
##### Rastergrafik 
svg = scalable vector graphic

Ähnlich wie bei der Turtle: „mach da mal nen bogen, usw.“
So eine svg dadurch das das nicht einfach nur Punkte sind, kann ich die mit einem Texteditor erstellen. Der Witz ist, das in der Editor-Datei einfach nur Buchstaben sind und daraus eine Grafik wird.   
Ich könnte mit Python jetzt so eine Editor-Datei erstellen und mir die anzeigen lassen   
**Spoiler:** Das wird eine der nächsten Hausaufgaben.

##### Fragen aus der Stunde
Welche art von Anwendung behandeln wir gerade warum?

Ganz typische Anwendung für for schleife, ok wir befüllen eine liste   
wo sind wir? Immernoch in Grundlagen Techniken/Training, vgl Klavierspielen Fingerübungen macht noch kein Lied hilft aber beim späteren spielen  
for schliefen werden zukünftig einfach helfen flüssiger zu coden und es macht Spaß lustigen unsinn zu haben  

piko baut manchmal sogar ganze Websiten mit Python  
Durch Python verschiedene HTML Stücken zusammen stückeln. Ähnlich wie jetzt in der hausaufgabe bei den weisheiten.



## Pikos Datei
```python
import random

nomen = """Wünsche
Träume
Ängste
Ziele
Freunde
Hoffnungen
Fragen
Antworten
Träume, 
Erfahrungen, 
Geschichten, 
Missgeschicke, 
Zähne, 
Freunde, 
Verwandte, 
 Kinder, 
 Geister, 
Bücher, 
Koffer, 
Betten, 
Essen, 
Käse, 
Gesetze, 
Fässer, 
Weine, 
Haare, 
Beine, 
Klischees, 
"""

praedikate = """gehen auf
frieren im Winter
krümeln
brauchen Schokolade
haben keine Ahnung
fallen nicht weit vom Stamm
machen allein auch nicht glücklich
ersetzen einen Zimmermann
wurden von der Werbeindustrie frei erfunden
heilen alle Wunden
teilt man gern unter Freunden
machen noch keinen Sommer
werden auch von Vögeln gemieden
fügen sich ganz von allein
ergeben einen schmackhaften Auflauf
haben nur andere Vorzeichen
"""

praedikatliste = praedikate.splitlines()
nomenliste_unbereinigt = nomen.splitlines()

nomenliste_bereinigt = []

for nomen in nomenliste_unbereinigt:
#     print(nomen)
    nomen = nomen.strip()
#     print(nomen)
    # entweder: nomen = nomen.rstrip(","))
#     nomen = nomen.strip().rstrip(", ")  # Don't try this at home
    if nomen[-1] == ",":
        nomen = nomen[:-1]
    nomenliste_bereinigt.append(nomen)
    
    
    
nomen1 = random.choices(nomenliste_bereinigt, k=1)[0]

nomen2 = random.choice(nomenliste_bereinigt)
praedikat = random.choice(praedikatliste)


print(nomen1 + " sind wie " + nomen2 + ": sie " + praedikat + ".")
```

```python
pralinenschachtel = ["praline1", "praline2", "praline3"]

print(pralinenschachtel[1])
print(pralinenschachtel[2:])
```

Das Experiment, das nicht geklappt hat, weil das Modul PIL fehlt.
```python
# https://www.appsloveworld.com/coding/python3x/146/how-can-i-save-a-turtle-canvas-as-an-image-png-or-jpg
from turtle import *

speed(0)

for i in range(20):
    fd(10)
    rt(10)
    circle(100, 350)
    
canvas = getscreen().getcanvas()

canvas.postscript(file="turtle_img.ps")  # Saves as a .ps file

from PIL import Image
turtle_img = Image.open("turtle_img.ps")
turtle_img.save("turtle_img.png", "png")
# Auch:   turtle_img.save("turtle_img, "jpeg")
```
Schreiben mit der Turtle
```python
from turtle import *

write("hallo")
```