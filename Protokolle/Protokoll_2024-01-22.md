# Pikos Python Kurs am 22.01.2023


# 1. Hinweise
Demnächst kommt wieder eine Neueinsteiger oder Aussteiger Wiederholungsstunde  
https://dudle.ccc-mannheim.de/Pykurs_Auffrischung_Quiz/

# 2. Hausaufgabenbesprechung

## Wichtiges 'neues' aus den Besprechungen:

**Methode Entpacken genauere Erklärung**
-> Siehe Aufgabe 1

**round()-Funktion**

> Am einfachsten kannst du Zahlen in Python runden, indem du die eingebaute Funktion round verwendest.
> ```
> round(Zahl, Dezimalstellen)
> ```
> Zahl (Erforderlich) Die Zahl, die gerundet werden soll.
> Dezimalstellen (Optional) Anzahl der Dezimalstellen welche zum Runden genutzt werden sollen. Standard ist 0
> So können wir zum Beispiel das in der Einleitung erwähnte π/Pi in Python aufrunden.
> ```
> from math import pi
> print(round(pi, 2), f"= round({pi}, 2)")
> print(round(pi, 3), f"= round({pi}, 3)")
> print(round(pi, 10), f"= round({pi}, 10)")
> 
> Output:
> 3.14 = round(3.141592653589793, 2)
> 3.142 = round(3.141592653589793, 3)
> 3.1415926536 = round(3.141592653589793, 10)
Quelle: https://codegree.de/runden-in-python/

**exitonclick()**
 -> beendet Fenster durch einen Klick
 
**setup()**
Pikos Standard setup Einstellung als Beispiel: 
```
setup (80, 80, 2580, 200)
```
Was bedeutet was?
- 80, 80 Größe Anzeigefenster
- 2580 Wie weit rechts auf dem Monitor
- 500 Wie weit vom oberen Rand des Monitors entfernt
-> Pikos Monitor ist sehr lang, dafür aber nicht so hoch
```
setup(breite, höhe, ort_links_rechts, ort_oben_unten)
```

**Leinwandfenster-Titel Verändern**
```
title("Pikos Farbratespiel")
```

## Aufgabe 1
Dazu wird es ein extra Erklär Video geben. 

**Fragen aus der Stunde zu der Aufgabe:**
Wir hatten einen Error, den wir gerne verstehen würden:
”Too many values to unpack (expected 2)”
Was macht man, wenn man flexibel viele Container möchte?

```
def f(x="example@example.com"):
  a, b = x.split("@")
  c, d = b.rsplit(".", 1)
  return a, c, d
```

Hier hatten wir uns gefragt, wenn man bei jedem Punkt splitten möchte, könnte man dann festlegen man macht beliebig viele Container, in die die String-Stücke eingeteilt werden?

**--> Entpacken-Methode**

```
rucksack = ("Schokoriegel", "Marzipankartoffeln", "Muffins")

dings, dongs, dangs = rucksack

dings = rucksack[0]
dongs = rucksack[1]
dangs = rucksack[2]

print (dings)

zahl1, zahl 2 = 3, 5

das gleiche wie: 
zahl1 = 3
zahl2 = 5
```
--> Diese Art zu schreiben ist kürzer und im prakitschen Umgang praktikabler

Was passiert wenn es zwei @ gibt?
```
def f(x="ex@mple@example.com"):
  bestandteile = x.split("@")
  c, d = bestandteile[-1].rsplit(".", 1)
  return a, c, d
```
-> man kann immer nur dann gut enpacken, wenn man weiß wie viele Bestandteile man hat!

## Aufgabe 2: Das Farbratespiel

```
from turtle import *
from random import random

setup(400, 400, 3580, 500)
title("Pikos Farbenratspiel")

# Eine Zufallsfarbe generieren und anzeigen
farbe_richtig = (random(), random(), random())
zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93
bgcolor(zufallsfarbe)

# Raten lassen
rate_string = input("Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "0.3, 0.0,..."
print("Dein Rateversuch: " + rate_string)

# Den Rateversuch verarbeiten
farbe_geraten = []

ratestring_stripped = rate_string.strip()
liste_ratestrings = ratestring_stripped.split(" ")

# farbe_geraten = farbe_geraten.strip().split(" ")

for ziffernschnipsel in liste_ratestrings:  # ["30", "89", "10"]
    kommazahl = int(ziffernschnipsel) / 100
    farbe_geraten.append(kommazahl)  # [0.3, 0.0, ...
    
    
# print(type(farbe_geraten), type(zahlen))  # Was ist in den beiden Variablen drin?
# Den Rateversuch aufmalen lassen:
color(farbe_geraten)
pensize(100)
dot()


def farbe_verzehnfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste
    

#differenz = farbe_richtig - farbe_geraten


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
        print(unterschied) # :D
    return summe
  
farben_differenz(farbe_richtig, farbe_geraten)

print("Richtiges Ergebnis", farbe_verzehnfachen(farbe_richtig))  # 80 94 57

exitonclick()

```
- es ist immer gut eine Skala die von 0 bis 1 geht in etwas anderes umzuwandeln z.B. mal10 nehmen um bei einer 'angenehmeren' Skala zu landen. 

Was wurde verändert:
- Die Benutzenden wurden in Kenntnis gesetzt:
```
input("Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein:")
```
- Die zahlen wurden zur angenehmeren Eingabe in Ganze Zahlen geändert:
```
kommazahl = int(ziffernschnipsel) /100
```
- die Differenz zwischen dem Tatsächlichen Ergebnis und dem Geratenen wird angezeigt:
```
#differenz = farbe_richtig - farbe_geraten  # wär schön, funktioniert aber nicht.

def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
        print(unterschied) # :D
    return summe
  
farben_differenz(farbe_richtig, farbe_geraten)

print("Richtiges Ergebnis", farbe_verzehnfachen(farbe_richtig))  # 80 94 57
```
Ganz Typisches Vorgehen für Programm Erstellung: 
Wir möchten jetzt noch fix die Differenz ausrechnen, und fangen dann mal mit der Funktions definiton an. Es ist hilfreich mit einem Notiz Zettel zu arbeiten und sich manchmal händisch Notizen zu machen, bzw einmal aufzumalen. (quasi schriftlich/bildlich denken)
Also hat man jetzt den Ansatz für diese Differenz Funktion und verlässt kurz den PC an den Zettel und denkt da nochmal drüber nach bevor dann wieder an den PC zur weiter bearbeitung der Funktion geht. 
```
Richtig:   0.7, 0.1, 0.5
Geraten:   0.6, 0.1, 0.3
Untersc:   0.1 + 0 + 0.2 = 0.3 
def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):  mit i gehe ich die zahlen durch
        # nur den Unterschied der ersten Stelle
        unterschied = farbe1[]
        print(unterschied) # :D
        # Ist noch nicht fertig – aber ein guter Zwischenstand, das print am Ende gibt uns aus, was wir bisher 
        # erreicht haben.
```





## Pikos Datei
Werte entpacken:
```python
def f(x="example@example.com"):
    a, b = x.split("@")
    c, d = b.rsplit(".", 1)
    return a, c, d


# ENTPACKEN

rucksack = ("Schokoriegel", "Marzipankartoffeln", "Muffins")

dings, dongs, dangs = rucksack

dings = rucksack[0]
dongs = rucksack[1]
dangs = rucksack[2]


print(dings)


zahl1, zahl2 = 3, 5

zahl1 = 3
zahl2 = 5
```
Das Farbenratespiel:
```python
from turtle import *
from random import random

setup(400, 400, 3580, 500)
title("Pikos Farbenratspiel")

# Eine Zufallsfarbe generieren und anzeigen
farbe_richtig = (random(), random(), random())
zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93
bgcolor(zufallsfarbe)

# Raten lassen
rate_string = input("Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "0.3, 0.0,..."
print("Dein Rateversuch: " + rate_string)

# Den Rateversuch verarbeiten
farbe_geraten = []

ratestring_stripped = rate_string.strip()
liste_ratestrings = ratestring_stripped.split(" ")

# farbe_geraten = farbe_geraten.strip().split(" ")

for ziffernschnipsel in liste_ratestrings:  # ["30", "89", "10"]
    kommazahl = int(ziffernschnipsel) / 100
    farbe_geraten.append(kommazahl)  # [0.3, 0.0, ...
    
    
# print(type(farbe_geraten), type(zahlen))  # Was ist in den beiden Variablen drin?
# Den Rateversuch aufmalen lassen:
color(farbe_geraten)
pensize(100)
dot()


def farbe_verzehnfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste
    




def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
        print("Unterschied:", unterschied)
        print("Summe ist jetzt:", summe) # Hier könnt Ihr der Summe beim Wachsen zuschauen.
        # Am Ende sollten diese beiden Zeilen auskommentiert sein – die sind nur dazu da, dass wir nochmal sicher gehen
        # können, dass die Funktion auch das tut, was wir glauben...
    return summe
    
farben_differenz(farbe_richtig, farbe_geraten)
    
print("Richtiges Ergebnis", farbe_verzehnfachen(farbe_richtig))  # 80 94 57

exitonclick()
```