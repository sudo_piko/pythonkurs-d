# Pikos Python Kurs am 25.03.2024

# Links
Empfehlung von Annette:  
https://www.amygoodchild.com/art  
https://www.amygoodchild.com/blog/what-is-generative-art

Nächster Kurs:  
https://gitlab.com/sudo_piko/pythonkurs-e

Feedback-Pad: 
https://md.ha.si/WA5TBVlyTw2Pht1UElOBMA#

# Fragen aus der Stunde
## Möglichkeiten, zusammen an Code zu arbeiten 
### Zeitgleich am gleichen Code arbeiten ("Pair Programming")
Piko kann da leider keine Webseite oder Programm empfehlen, aber Ihr könnt gerne ein paar ausprobieren. Die Suchbegriffe, die Ihr braucht, sind zum Beispiel:
collaborative coding  
remote pair programming  
online coding together  

Falls Ihr mal eins findet, mit dem Ihr glücklich seid, dann gebt gern bescheid, das würde mich auch sehr interessieren!

### Zeitversetzt am gleichen Code arbeiten
Die Antwort ist "git". Das ist ein Programm, das fast alle Programmierer*innen verwenden. Es ist anfangs etwas verwirrend und schwer zu lernen, aber wenn Ihr mehr mit Code macht und viel mit anderen zusammenarbeitet, dann werdet Ihr es früher oder später brauchen. Es gibt viele Tutorials dazu; hier nur zwei:
- gitlab: https://docs.gitlab.com/ee/tutorials/learn_git.html
- ohmygit, ein Spiel: https://ohmygit.org/
- wenn diese beiden nicht gut für Euch sind, dann sucht gerne im Internet nach anderen!

## PIL
Das Paket, das wir für PIL brauchen, heißt `pillow`. Das könnt Ihr ganz normal über "Verwalte Pakete" in Thonny installieren. 

![pillow_paket_installieren.png](..%2FAbbildungen%2Fpillow_paket_installieren.png)

Wenn Ihr das Problem mit dem ausgegrauten Button "Suche im PyPI" habt (so wie Piko in der Stunde), dann hilft es, Thonny einmal zu schließen und neu zu starten. Wenn es dann immer noch Probleme gibt, schreibt Piko gern eine Mail!


# Wie weiter?

## Hinweise
- bleibt als Gruppe zusammen und sucht Euch zusammen neue Herausforderungen (siehe unten). Wenn Eure Gruppe zu klein wird, haut gerne Piko an, ob noch andere Gruppen Leute suchen.
- bleibt dran! Wenn Ihr Euch damit nicht beschäftigt, vergesst Ihr es allmählich. Es wieder neu zu lernen, wird zwar schneller gehen als beim ersten Mal, aber muss halt trotzdem auch getan werden. 
- Tutorials: Sind sicher interessant, aber sie bringen Euch selten Selbstständigkeit bei, sondern zeigen meistens nur, wie eins das machen würde. Versucht auch bei Tutorials (genau wie ich das bei meinen Videos ab und zu sage), anzuhalten und zu überlegen, wie Ihr das machen würdet. Kopiert möglichst keinen Code, sondern tippt ihn ab, so muss er zumindest einmal durch Euren Kopf...
- Wenn Ihr gute Ressourcen findet, gebt bescheid! Ich bin immer auf der Suche...
- Wenn Ihr Lust habt, kommt einfach zum nächsten Kurs vorbei. Wir fangen wieder bei Null an, aber das kann ja auch mal ganz interessant sein. Oder Ihr guckt Euch die Aufgaben von den alten Kursen an.

## Ressourcen zum Weiterlernen
### Python
- Kleine Übungsaufgaben
    - Pikos frühere Kurse: 
        - Abraxas: https://gitlab.com/sudo_piko/pykurs
        - Bython: https://gitlab.com/sudo_piko/pythonkurs-b
        - Clownfisch: https://gitlab.com/sudo_piko/pythonkurs-c
    - Project Euler (mathematiklastig): https://projecteuler.net/
    - Diverse Ideen: https://md.ha.si/9L_GnOccQCOMxE_bwkgkAQ#
- spezielle Themen: Es gibt wahnsinnig viele Ressourcen zu Python im Netz. Werft einfach die entsprechenden Wörter in eine Suchmaschine Eurer Wahl, eventuell noch mit "example" oder "tutorial" zB "Python pygame tetris tutorial"
    - Genart: https://www.youtube.com/watch?v=Cm_SzDlQ2cM
    - Nerdiges: Conway's Game of life
    - Spiele, zB Schere-Stein-Paper etc.
    - Rekursion: https://mundyreimer.github.io/blog/lindenmayer-grammars-1
- Bücher von Al Sweigart (auf Englisch): https://inventwithpython.com/
- weitere Kurse auf Webseiten:
    -  Deutschsprachig
        -  Ausführlich, viele Spiele: https://www.python-lernen.de
        -  Kurz, einfach, interaktiv: https://cscircles.cemc.uwaterloo.ca/de/
        -  Für Kinder gedacht => anschaulich erklärt: https://open.hpi.de/courses/pythonjunior2020
        -  Recht kondensiert, viel Inhalt, wenig Übungen: https://www.python-kurs.eu/
        -  Sehr gründlich, wirkt etwas trocken: https://openbook.rheinwerk-verlag.de/python/ 
    -  Englischsprachig
        -  https://www.learnpython.org/
        -  Tutorial mit Jupyter Notebooks (statt Thonny): https://www.youtube.com/watch?v=Z1Yd7upQsXY
        -  w3schools; interaktiv, ausführlich: https://www.w3schools.com/python/default.asp
        -  ganzer Kurs, mit Lessons, Quizzes und Aufgaben: https://www.codecademy.com/learn/learn-python-3


### Computer allgemein
- Lernspielesammlung: https://github.com/yrgo/awesome-educational-games






## Pikos Datei

```python

```
```python

```
```python

```
```python

```
```python

```
```python

```
```python

```
```python

```