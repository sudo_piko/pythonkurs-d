# Pikos Python Kurs am 26.02.2024


## Pikos Datei

```python
my_file = open("/home/piko/Downloads/Adorno.txt", encoding="utf-8")  # Mac+Linux
text = my_file.read()

woerterliste = text.split()
# print(woerterliste)

# Ein Dictionary, das die Wörter und nicht die Buchstaben zählt...
d = {}

for wort in woerterliste:  # Aber evtl. muss ich das Wort noch von einem Komma oder so befreien...
    bereinigtes_wort = wort.lower().strip(".,;–()?!")
    if len(bereinigtes_wort) == 1 and bereinigtes_wort.isalpha():
#         print(bereinigtes_wort)
        pass
    elif bereinigtes_wort.isnumeric():
        pass
    elif bereinigtes_wort == "seitenzahl":
        pass
    elif bereinigtes_wort[0] == "§":
        pass
    else:
#     print(bereinigtes_wort)
        d[bereinigtes_wort] = d.get(bereinigtes_wort, 0) + 1  # auch bei den Wörtern ist .lower() ganz sinnvoll...
    
print(d["recht"])

# Wenn ich das alles noch anzeigen will, dann brauche ich [:100] an *genau* der richtigen Stelle:
import matplotlib.pyplot as plt
sortiertes_dictionary = dict(sorted(d.items(), key=lambda x: x[1], reverse=True)[:100])
print(sortiertes_dictionary)
plt.bar(sortiertes_dictionary.keys(), sortiertes_dictionary.values(), 0.5, color='b')
plt.show()
```
```python
text = """Jeder Mensch hat das Recht, Fehler zu machen.
Jeder Mensch hat das Recht, einzigartig zu sein.
Jeder Mensch hat das Recht zu lieben."""


# my_file = open("/home/piko/Downloads/Adorno.txt", encoding="utf-8")  # Mac+Linux
# text = my_file.read()



woerterliste = text.split()
# print(woerterliste)

# Akkumulatorschleife
bereinigte_woerterliste = []  
for wort in woerterliste:
    neu = wort.strip(".,?!–-;()").lower()
    bereinigte_woerterliste.append(neu)
# print(bereinigte_woerterliste)

d = {}

for i in range(len(bereinigte_woerterliste)-1):  # i ist ein Index, also eine Zahl, die wir in der Liste nachschlagen können
#     print(i, bereinigte_woerterliste[i], bereinigte_woerterliste[i+1])
    erstes_wort = bereinigte_woerterliste[i]
    zweites_wort = bereinigte_woerterliste[i+1]
    
    d[erstes_wort] = d.get(erstes_wort, []) + [zweites_wort]

print(d)

# {"zu":["machen", "sein", "lieben"], 




# import matplotlib.pyplot as plt
# sortiertes_dictionary = dict(sorted(d.items(), key=lambda x: x[1], reverse=True)[:100])
# print(sortiertes_dictionary)
# print(d)
```
```python
nested_list = [["Schuhschrank", "Regal", "Stuhl"],
               ["Sofa", "Sofatisch", "Bücherregal", "Schwertständer"],
               ["Küchentisch", ["Erdbeeren", ["Himbeeren", "Zucker", "Joghurt"], "Käsekuchen"], "Mikrowelle"],
               ["Bett", "Kleiderschrank", "Nachttisch"]]

print(nested_list[1][2])  # => Bücherregal
# print(["Sofa", "Sofatisch", "Bücherregal", "Schwertständer"][2])
# print(                 "Bücherregal")
print(nested_list[2][1][1][0][3])

# a = (5 + 7) * 12 + 13 / (2+15) - (22 - 3 * (10/3))
# a =    12   *  12  13  /  17   -

langweiliges_dictionary = {"rot": 3, "grün": 25, "blau":0, "gelb":23}

langweiliges_dictionary["grün"]

nested_dictionary = {"zimtschnecken": {"zimt": 1, "teig":4, "zucker": 3},
                     "apfelkuchen": {"apfel": 5, "teig": 3, "zucker": 2},
                     "erdbeerkuchen": {"erdbeere": 10, "teig": 3, "zucker": 2},
                     "käsekuchen": {"quark": 5, "teig": 3, "zucker": 2}}

# print(nested_dictionary["erdbeerkuchen"]["erdbeere"]) # => 10
# print({'erdbeere': 10, 'teig': 3, 'zucker': 2}["erdbeere"])
print(nested_dictionary.values())
```
```python
from random import choice

text = """Jeder Mensch hat das Recht, Fehler zu machen.
Jeder Mensch hat das Recht, einzigartig zu sein.
Jeder Mensch hat das Recht zu lieben."""

d = {
    'jeder':   ['mensch', 'mensch', 'mensch'],
    'mensch':  ['hat', 'hat', 'hat'],
    'hat':     ['das', 'das', 'das'],
    'das':     ['recht', 'recht', 'recht'],
    'recht':   ['fehler', 'einzigartig', 'zu'],
    'fehler':  ['zu'],
    'zu':      ['machen', 'sein', 'lieben'],
    'machen':  ['jeder'],
'einzigartig': ['zu'],
    'sein':    ['jeder'],
    "lieben":  ["zu"]
    }


anfangswort = "zu"
text = anfangswort + " "
neues_wort = anfangswort
for i in range(20):
    moegliche_woerter = d[neues_wort]
    neues_wort = choice(moegliche_woerter)
    text = text + neues_wort + " "
    
print(text)
```