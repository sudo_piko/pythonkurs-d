# Pikos Python Kurs am 04.03.2024

## 25.03. ist der letzte Termin!

Beim letzten Termin wird Piko weitere Optionen zum selbstständigen Weitermachen vorstellen.

Generelle Empfehlung: Viel selbst online raussuchen und recherchieren, gängige Quellen für Python-Themen finden. 

## Besprechung der Aufgaben

Wiederholung: Beim INDEXING von “Listen in Listen” (Aufgabe 1.1.) kann man sich neben dem ganzen Listen-Inhalt auch Inhalte auf unterschiedlichen Detail-Ebenen ausgeben lassen oder mit ihnen arbeiten:
* Ganze Listen 				(eine von 3 Listen in der Aufgabe) 
* einzelne Elemente aus Listen  	(ein Wort in Liste 3 in der Aufgabe) 
* Teile der Elemente 			(ein Buchstabe von einem Wort in der Aufgabe)

Debugging >> warum funktioniert meine Dict-Methode nicht, wenn ich mehrere Dictionaries zusammen in einer Liste habe und auf die gesamte Liste zugreife?

Wenn eine Liste mehrere Dictionairies enthält, dann kann man über Indexing ein einzelnes Dictionary als ein Listenelement auswählen. Wenn man (wie in Aufg. 1.2) also ein Dictionary anwählt, kann man nach Auswahl auch Dict-Methoden anwenden (das funktioniert nicht, solange Python eine ebene höher, nämlich auf der Ebene “Eine Liste mit mehreren Dictionaries” schaut

Wiederholung: Nummerierung der String-Stellen beim Slicing   
Beim SLICING kann man Elemente eines Strings (von – bis) abfragen oder mit ihnen arbeiten; wenn man NUR “bis” fragt und das “von” nicht definiert, dann ist die am Ende erwähnte Stelle zum Beispiel “4” für die vierte Stelle des Strings, auch wenn die erste Stelle im String die “0” ist.

Beispiele (Achtung, der String fängt mit "0" an!)
```python
w = "0123456"
print(w[4])
```
Ausgabe: "4" 

```python
v = "0123456"
print(v[1:4])
``` 
Ausgabe: "123"

## Quelltext variieren
Aufgabe in der Stunde: Aus der Liste mit Dicts soll ein Zufalls-Wort von den Dict-Keys (das Wort, das vorne im Dict steht) auswählen und dann zufällig ergänzen um einen der Values (eins der Listen-Wörter hinter dem Key).

Piko-Tipp: Wenn wir weiter bauen wollen an den variierten Texten, sollten wir immer 2 Wörter gemeinsam betrachten. 


## Intro: gensound 
Paket zu Thonny hinzufügen



## Pikos Datei

```python
text = """Jeder Mensch hat das Recht, Fehler zu machen.
Jeder Mensch hat das Recht, einzigartig zu sein.
Jeder Mensch hat das Recht zu lieben."""

woerterliste = text.split()
# print(woerterliste)

# Akkumulatorschleife
bereinigte_woerterliste = []  
for wort in woerterliste:
    neues_wort = wort.strip(".,?!–-;()").lower()
    bereinigte_woerterliste.append(neues_wort)
# print(bereinigte_woerterliste)

d = {}

for i in range(len(bereinigte_woerterliste)-1): 
    erstes_wort = bereinigte_woerterliste[i]
    zweites_wort = bereinigte_woerterliste[i+1]
    d[erstes_wort] = d.get(erstes_wort, []) + [zweites_wort]

print(d)
```
```python
text = """Jeder Mensch hat das Recht, Fehler zu machen.
Jeder Mensch hat das Recht, einzigartig zu sein.
Jeder Mensch hat das Recht zu lieben."""


d = {'jeder':    ['mensch', 'mensch', 'mensch'],
 'mensch':   ['hat', 'hat', 'hat'],
 'hat':      ['das', 'das', 'das'],
 'das':      ['recht', 'recht', 'recht'],
 'recht':    ['fehler', 'einzigartig', 'zu'],
 'fehler':   ['zu'],
 'zu':       ['machen', 'sein', 'lieben'],
 'machen':   ['jeder'],
 'einzigartig': ['zu'],
 'sein':     ['jeder']}


print(d["zu"])

l = ['machen', 'sein', 'lieben']
print(d.keys())
```
```python
terrassentisch = [["Wasser", "Tee", "Honig"], 
                  ["Wasser", "Kaffeepulver", "Sahne"], 
                  ["Limonade"], 
                  ["Milch", "Kakao", "Zucker"], 
                  ["Wasser", "Schwarztee", "Milch", "Zucker", "Kardamom", "Zimt", "Ingwer", "Pfefferkörner", "Indische Lorbeerblätter", "Nelken", "Muskat"]]


print(terrassentisch[-1][0:4])
```
```python
keller = [{"Rennrad": 3, "Mountainbike": 5, "Trekkingrad": 2, "E-Bike": 1}, 
          {"Waschmaschine": 2, "Wäscheständer":5, "Wäschekorb": 2}, 
          {"Werkbank":1, "Schraubendreher": 5, "Hammer": 2, "Säge": 1}, 
          {"Stromkasten": 11, "Gastherme": 1, "Wasseruhr": 10}]

# 
# print(                 keller[2]                                  .keys())
# print({"Werkbank":1, "Schraubendreher": 5, "Hammer": 2, "Säge": 1}.keys())
# 
# 
# 
# # print(9 - 3 / 1.5  + (28 + 2) / 15)
# 
# print(keller[2].values())

print(              keller[0]                                         )
print({'Rennrad': 3, 'Mountainbike': 5, 'Trekkingrad': 2, 'E-Bike': 1}["Rennrad"])
print(                               keller[0]                        ["Rennrad"])
```
```python
favorite_things = {"Maria": ["Raindrops on roses", "whiskers on kittens", "bright copper kettles", "warm woolen mittens"],
                   "Salbei": ["Sonne", "Luftiger Boden", "Nährstoffreicher Boden"], 
                   "Katzen": ["Kartons", "Tastaturen", "Streicheln", "Futter"],
                   "Piko": ["Python", "Matherätsel", "Bücher", "Kuchen"]}

# print(favorite_things["Salbei"][2])

dings= list(favorite_things.keys())

print(dings)

print(type(dings))
```
```python
import random

wortfolgen = {
    'jeder':   ['mensch', 'mensch', 'mensch'],
    'mensch':  ['hat', 'hat', 'hat'],
    'hat':     ['das', 'das', 'das'],
    'das':     ['recht', 'recht', 'recht'],
    'recht':   ['fehler', 'einzigartig', 'zu'],
    'fehler':  ['zu'],
    'zu':      ['machen', 'sein', 'lieben'],
    'machen':  ['jeder'],
'einzigartig': ['zu'],
    'sein':    ['jeder'],
    "lieben":  ["zu"]
    }

drei_woerter = wortfolgen["recht"]

# print(drei_woerter)
# print(type(drei_woerter))
# print(random.choice(drei_woerter))

text = ""
neues_wort = random.choice(list(wortfolgen.keys()))
for i in range(20):  # zehnmal ausführen
    neues_wort = random.choice(wortfolgen[neues_wort])
    text =            text + neues_wort + " "
    #   bisheriger text ^  +  zufälliges wort   ^        + Leerzeichen

print(text)
```
```python
import random

wortfolgen = {
    'jeder':   ['mensch', 'mensch', 'mensch'],
    'mensch':  ['hat', 'hat', 'hat'],
    'hat':     ['das', 'das', 'das'],
    'das':     ['recht', 'recht', 'recht'],
    'recht':   ['fehler', 'einzigartig', 'zu'],
    'fehler':  ['zu'],
    'zu':      ['machen', 'sein', 'lieben'],
    'machen':  ['jeder'],
'einzigartig': ['zu'],
    'sein':    ['jeder'],
    "lieben":  ["zu"]
    }

# drei_woerter = wortfolgen["recht"]

# Aufgabe: ein zufälliges Wort von den keys. random.choice(

# Erstmal die Keys ausgeben.
print(wortfolgen.keys())
print(list(wortfolgen.keys()))
# Kann ich das eventuell direkt in random.choice() reinwerfen? Oder muss ich da noch eine Liste draus machen?
print(random.choice(list(wortfolgen.keys())))
```
```
print("hallo")
max([5, 2, 1, 5,6])
sum([5, 2, 1, 5,6])
= Küchentisch; eingebaute Funktionen


import random
random.randint()
= Speisekammer; Module, die direkt importiert werden können


PyPI
Python Package Index
= Supermarkt; Pakete, die erst runtergeladen werden müssen
```