# Pikos Python Kurs am 30.01.2023

## Anmerkung
Piko ist noch ein Fehler aufgefallen: Die Funktion "farbe_verzehnfachen" verzehnfacht sie ja gar nicht, sondern verhundertfacht sie – und das soll sie auch. Nur der Name ist Unsinn; deswegen habe ich ihn hier im Code geändert. Natürlich muss er dann nicht nur im Funktionskopf geändert wreden, sondern auch da, wo wir die Funktion aufrufen.

# Protokoll
## Hausaufgabenbesprechung: Farbratespiel

Reihenfolge der Programmelemente:

 * setup, globale Variablen
 * Funktionsdefinitionen
 * eigentlicher Code

Debugging: Rateversuch mit Kommata wirft eine Fehlermeldung
eine Funktion für die Bewertung der Fehlergröße
mehrere Runden

## Konstanten
üblich: Konstanten werden groß geschrieben
Konstanten sind Variablen, die sich über das ganze Programm hinweg nicht ändern
In unserem Fall ist die Anzahl der Runden ein Kandidat für eine Konstante.

exitonclick() beendet das ganze Programm. Deshalb darf es nicht innerhalb einer Schleife stehen.

## Punktefunktion
Punktestand ist eine Variable, die als globale Variable vor der großen for-Schleife für mehrere Runden definiert wird.

Mechanik für Punkte: 
```
Fehlergröße > 0.50 = 0
> 0.20 = 20
> 0.1 = 40
< 0.1 = 70
```

## Mehrspielermodus
Wir nehmen an, wir haben immer drei Personen: Alex, Kim, Robin

## Iteration über ein Dictionary

wichtige Dictionary-Methoden:
.items()
.keys()
.values()

Unterschied zwischen Liste und Dictionary




## Pikos Datei
Hausaufgabenbesprechung zum Farbratespiel

Ausgangsdatei
```python
from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")


# Nützliche Funktionen
def farbe_verhundertfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


# Eine Zufallsfarbe generieren und anzeigen
farbe_richtig = (random(), random(), random())
zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93
bgcolor(zufallsfarbe)


# Raten lassen
rate_string = input(
    "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
print("Dein Rateversuch: " + rate_string)


# Den Rateversuch verarbeiten
farbe_geraten = []

ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
# farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

for ziffernschnipsel in liste_ratestrings:  # ["30", "89", "10"]
    kommazahl = int(ziffernschnipsel) / 100  # zB 30 => 0.3
    farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


# Den Rateversuch aufmalen lassen:
color(farbe_geraten)
pensize(100)
dot()

print(farben_differenz(farbe_richtig, farbe_geraten))

print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57

exitonclick()
```
Herausfinden, was denn `zufallsfarbe` eigentlich sein sollte und wo der Unterschied ist: Die Werte ausgeben lassen
```python
from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")


# Nützliche Funktionen
def farbe_verhundertfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


# Eine Zufallsfarbe generieren und anzeigen
farbe_richtig = (random(), random(), random())
zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93
bgcolor(zufallsfarbe)


# Raten lassen
rate_string = input(
    "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
print("Dein Rateversuch: " + rate_string)


# Den Rateversuch verarbeiten
farbe_geraten = []

ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
# farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
    ziffernschnipsel = ziffernschnipsel.strip(",")
    kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
    farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


# Den Rateversuch aufmalen lassen:
color(farbe_geraten)
pensize(100)
dot()


print("farbe_richtig:", farbe_richtig)
print(f"{farbe_geraten=}")
print(f"{zufallsfarbe=}")

differenz = farben_differenz(farbe_richtig, farbe_geraten)

print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57

exitonclick()
```
Funktionierende Version für ein*e Spieler*in, aber mehrere Runden; gleicher Code wie `Projekte/Spiele/Farbratespiel_3_Runden.py`.
```python
from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")
ANZAHL_RUNDEN = 3  # eine Konstante


# Nützliche Funktionen
def farbe_verhundertfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


def bewerten(fehlergroesse):
    # printet eine Bewerung anhand der fehlergroesse
    if fehlergroesse > 0.5:
        print("Das ist aber nicht besonders gut!")
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        print("Du hast ok geschätzt.")
    elif fehlergroesse > 0.1:
        print("Fast!")
    else:
        print("Wow, übermenschlich!")

def punkte_ausrechnen(fehlergroesse):
    # Rechnet von der fehlergroesse aus einen Punktzahl aus.
    # Hier kommt noch Mathematik
    if fehlergroesse > 0.5:
        return 0
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        return 20
    elif fehlergroesse > 0.1:
        return 40
    else:
        return 70
    
punktestand = 0
for i in range(ANZAHL_RUNDEN):
    # Eine Zufallsfarbe generieren und anzeigen
    clear()
    farbe_richtig = (random(), random(), random())
    # zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93 
    bgcolor(farbe_richtig)


    # Raten lassen
    rate_string = input(
        "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
    print("Dein Rateversuch: " + rate_string)


    # Den Rateversuch verarbeiten
    farbe_geraten = []

    ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
    liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
    # farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


    # Den Rateversuch aufmalen lassen:
    color(farbe_geraten)
    pensize(100)
    dot()

    # Ergebniswerte ausrechnen
    differenz = farben_differenz(farbe_richtig, farbe_geraten)
    punkte = punkte_ausrechnen(differenz)
    punktestand = punktestand + punkte
    
    # Auf Ergebnis reagieren
    print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57
    print("Du lagst um", differenz, "daneben.")
    bewerten(differenz)
    print("Du bekommst", punkte, "Punkte!")
    print("Du hast jetzt", punktestand, "Punkte.")
    input("Weiter mit Enter ... ")

exitonclick()
```

Mehrspielende-Modus angefangen:
```python
from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")
ANZAHL_RUNDEN = 3  # eine Konstante
spielende = {"Alex":0, "Kim":0, "Robin":0}

# Nützliche Funktionen
def farbe_verhundertfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


def bewerten(fehlergroesse):
    # printet eine Bewerung anhand der fehlergroesse
    if fehlergroesse > 0.5:
        print("Das ist aber nicht besonders gut!")
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        print("Du hast ok geschätzt.")
    elif fehlergroesse > 0.1:
        print("Fast!")
    else:
        print("Wow, übermenschlich!")

def punkte_ausrechnen(fehlergroesse):
    # Rechnet von der fehlergroesse aus einen Punktzahl aus.
    # Hier kommt noch Mathematik
    if fehlergroesse > 0.5:
        return 0
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        return 20
    elif fehlergroesse > 0.1:
        return 40
    else:
        return 70
    
punktestand = 0
for i in range(ANZAHL_RUNDEN):
    # Eine Zufallsfarbe generieren und anzeigen
    clear()
    farbe_richtig = (random(), random(), random())
    # zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93 
    bgcolor(farbe_richtig)


    # Raten lassen
    rate_string = input(
        "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
    print("Dein Rateversuch: " + rate_string)


    # Den Rateversuch verarbeiten
    farbe_geraten = []

    ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
    liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
    # farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


    # Den Rateversuch aufmalen lassen:
    color(farbe_geraten)
    pensize(100)
    dot()

    # Ergebniswerte ausrechnen
    differenz = farben_differenz(farbe_richtig, farbe_geraten)
    punkte = punkte_ausrechnen(differenz)
    punktestand = punktestand + punkte
    
    # Auf Ergebnis reagieren
    print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57
    print("Du lagst um", differenz, "daneben.")
    bewerten(differenz)
    print("Du bekommst", punkte, "Punkte!")
    print("Du hast jetzt", punktestand, "Punkte.")
    
    
    for person in spielende:
        print("Punktestand für " + person +":")
        print(spielende[person])
 
 
    input("Weiter mit Enter ... ")

exitonclick()
```
```python
fahrradkeller = {"Faltrad": 3, "Rennrad": 4, "Lastenrad": 1}  
#                    key1 : v1,   key2: val2,   key3   : val3

dongs =    fahrradkeller["Faltrad"]
# var_name = dic_name[fahrradtyp]
print(dongs)

for dings in fahrradkeller:
    print(dings)
# ergibt:
# "Faltrad" 
# "Rennrad" 
# "Lastenrad"

for dings in fahrradkeller:
    print(dings, fahrradkeller[dings])
# ergibt:
# "Faltrad" 3
# "Rennrad" 4
# "Lastenrad" 1

for dings in fahrradkeller.items():
    print(dings)
# ergibt:
# ("Faltrad", 3)
# ("Rennrad", 4)
# ("Lastenrad", 1)

for dings in fahrradkeller.values():
    print(dings)
# ergibt:
# 3
# 4
# 1

# Interessante Dictionary-Methoden
# .items()
# .keys()
# .values()
# 
# Dictionaries: key:value
```