# Pikos Python Kurs am 09.10.2023
## Organisatorisches:

### Alles zum Kurs unter:

https://gitlab.com/sudo_piko/pythonkurs-d
### Veranstaltungshinweise:

1) Chaos Communication Congress 27. bis 30. Dezember 2023 in Hamburg

       Tickets ab 120 Euro; Hier Infos:
       https://www.ccc.de/updates/2023/37C3

2) FireShonks23, ebenfalls vom 27. bis 30. Dezember 2023, nur online!

        Mittendrin, statt nur dabei - es werden noch Helfer*innen gesucht; Alle Details:
        https://pads.haecksen.org/cDTL_m9bQBuxyFFKSUsWaw#

## Anmerkungen zur Hausaufgabe:

Vorsicht, Ziffern und Zahlen sind (in Python) nicht dasselbe! 

Immer, wenn eine Zahl als Ziffer, und nicht als Zahl, in Python vorkommt, verhält sie sich identisch wie ein Buchstabe.

Indem eins die Funktion `str(zahl)` anwendet, kann es Python zwingen, eine Zahl als Ziffer zu verwenden.

Python reagiert unterschiedlich auf die Schreibweisen "a", "b", "c", "d" und "a, b, c, d"

Im ersten Fall betrachtet Python die Buchstaben als einzelne Elemente von einem Tupel, im zweiten Fall sieht Python einen Gesamttext.

Farbwerte (in Python) können nicht größer als 1 sein (bzw. werden).

Dinge, die ich eingerückt habe (zum Beispiel Code / Text), kann ich mit der Tastenkombination Shift + Tab wieder an den linken Rand zurückholen bzw. das Einrücken schrittweise zurücknehmen.

Mit der Funktion def kann eins eine Art Stempel kreieren. Das heißt, eins kann ein graphisches Objekt definieren, dass sich dann beliebig oft wiederverwenden lässt. Zum Beispiel ring()

Die Definition lässt sich jederzeit abändern. Alles, was zur Definition gehört, wird auf das selbe Level eingerückt.

Graphische Objekte lassen sich per Koordinaten an eine gewünschte Stelle setzen. Hierzu dient die Funktion `goto()`; In die Klammer werden zwei Koordinaten eingetragen - x und y-Achse.

Wichtig ist die Reihenfolge: Erst `goto()` und dann das Objekt, das gezeichnet werden soll, zum Beispiel `dot()` oder `ring()`.

Code, den Python im Augenblick (noch) nicht ausführen soll, der jedoch auch nicht gelöscht werden soll, lässt sich mit dem Shortcut Strg+3 "auskommentieren", also für Python unlesbar machen.

Wenn mein String einen Zeilenumbruch enthalten soll, kann ich diesen durch das Kommando \n erzeugen. Wenn ich einen Zeilenumbruch in der Textausgabe benötige, kann ich print() schreiben.

Übrigens: In Thonny kann eins auf mehreren Tabs arbeiten.



## Pikos Datei

Hausaufgabe 1
```python
for zahl in range(1, 5):
    for buchstabe in "abcdef": # oder: ["a", "b", "c", "d", "e", "f"]
        print(str(zahl) + buchstabe, end=" ")

    print()
```

Zeilenumbruch durch `\n`
```python
text = "Hallo! \n Ich bin Pi\nko"

print(text)
```

Den Farbring in eine Funktion packen
```python
from turtle import *

def ring():
    pensize(30)
    anzahl_ecken = 65
    kantenlaenge = 10

    rot = 0
    farbaenderung = 0.1

    for i in range(anzahl_ecken):
        color(rot, 0, 0)
        forward(kantenlaenge)
        left(360/anzahl_ecken)
        rot = rot + farbaenderung
        if rot > 1:
            farbaenderung = -0.1
            rot = 1
        if rot < 0:
            farbaenderung = 0.1
            rot = 0

ring()
penup()
fd(200)
pendown()

ring()
penup()
lt(90)
fd(100)
rt(90)
pendown()

ring()
```

Unterschiedliche Arten, Dinge zu "verpacken"
```python
text = "eaiunrdaeiurnteui"

t = "e", "a", "i", "u"
a = 1, 2, 4
b = "a   b   c"
```

`goto()` und `dot()`
```python
from turtle import *

pu()
fd(600)
pd()

pensize(20)

dot()

penup()  # pu()
goto(800, 200)
color("red")
dot()
```

Mehrere Reihen von Punkten
```python
from turtle import *

pensize(20)
pu()

for i in range(10):  # unterste Reihe (y=0)
    goto(i*50, 0)
    dot()
        
    
for i in range(10):  # zweite Reihe (y=50)
    goto(i*50, 50)
    dot()
    
    
for i in range(10):  # dritte Reihe (y=100)
    goto(i*50, 100)
    dot()
    
    
for i in range(10):  # vierte Reihe (y=150)
    goto(i*50, 150)
    dot()
# und so weiter...
```
Mit zwei "verschachtelten" for-Schleifen
```python
from turtle import *

pensize(20)
pu()

for j in range(30):
    for i in range(10):
        goto(i*50, j*50)
        dot()
```
Kopiert die Ring-Funktion (den Stempelbau) in eine neue Datei
und verwendet sie (das Stempeln) in einer doppelten for-Schleife
wie oben, statt dem `dot()`

```python
from turtle import *

speed(0)
def ring():
    pensize(30)
    anzahl_ecken = 22
    kantenlaenge = 5

    rot = 0
    farbaenderung = 0.1

    for i in range(anzahl_ecken):
        color(rot, 0, 0)
        forward(kantenlaenge)
        left(360/anzahl_ecken)
        rot = rot + farbaenderung
        if rot > 1:
            farbaenderung = -0.1
            rot = 1
        if rot < 0:
            farbaenderung = 0.1
            rot = 0
    
pu()

for j in range(30):

    for i in range(5):
        goto(i*50, j*50)
        pd()
        ring()
        pu()
```
``` 
- j = 0,
    - i = 0
    - i = 1
    - i = 2
    - i = 3
    - i = 4
- j = 1,
    - i = 0
    - i = 1
    - i = 2
    - i = 3
    - i = 4
- j = 2,
    - i = 0
    - i = 1
    - i = 2
    - i = 3
    - i = 4
```
for-Schleife, Koordinaten der dot-Reihe
``` 
i    koordinaten	x-koordinate

0	(0, 0)		0		0*50
1	(50, 0)		50		1*50
2	(100, 0)	100		2*50
3	(150, 0)	150		3*50
4	(200, 0)	200
```