# Pikos Python Kurs am 04.09.2023

# Pikos Präsentation


## Python für absolute Anfänger*innen

---

# Ablauf
* Vorstellung Piko, Haecksen
* Läuft Thonny bei allen?
* Kurs-Ablauf
* Kleingruppen
* Fragen

---

# Piko
hat das auch nicht studiert  
Musik, CCC, Hamburg, Haecksen

----

# Haecksen
Gruppe von technikinteressierten FINTA-Personen  
Mehr Informationen auf haecksen.org

Umfeld des Chaos Computer Club

----

# Wau-Holland-Stiftung
Umfeld des Chaos Computer Club  
Mehr Informationen auf wauland.de

---

# Thonny

```
print("Hallo Welt!")
import turtle
```

***
(in den unteren Fensterteil "Kommandozeile", 
nach `>>>`)

---

## Ablauf des Kurses

* montags 19:30 bis 21:00
* jeweils eine Person schreibt freiwillig ein Protokoll 
    (gerne Markdown, muss aber nicht)
* Hausaufgaben ab dem nächsten Tag

----

# Adressen

Fragen: Piko@riseup.net

Aufgaben und Protokolle: 
https://gitlab.com/sudo_piko/pythonkurs-d

---

# Kleingruppen

* 3 bis 5 Personen
* wöchentlich treffen

----

## Aufgaben in den Kleingruppen

(in aufsteigender Wichtigkeit)
* Hausaufgaben besprechen (möglichst vorher schon gemacht haben)
* Unklares aus Stunde besprechen
* Spaß haben, sich gut vertragen

----

## Aufgaben für erstes Treffen

zusätzlich: Vorstellungsrunde, zB:
* vier wichtige Bereiche
* was wollt Ihr vom Kurs

----

## Gruppeneinteilungsballett

* den Namen (wie in BBB) in den *einen* präferierten Termin schreiben
* Piko sortiert
* E-Mail-Adressen austauschen per privater Nachricht im BBB (möglichst jede* an jede*) 
* per Mail erstes Treffen verabreden

---

# Trivia

Name mit D

---

# Generative Art

https://www.youtube.com/watch?v=meqHPIOzk-U

---

### NFTs und Generative Art

Line goes up: https://www.youtube.com/watch?v=YQ_xWvX1n9g
Generative Art und NFTs: https://jugendhackt.org/blog/community-talk-19-nft-crypto/

---

# Fragen?

---

# Thonny anwerfen


















