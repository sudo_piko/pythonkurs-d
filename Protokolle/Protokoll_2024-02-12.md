# Pikos Python Kurs am 12.02.2024


## Hinweis

Zum Farbenratespiel sind ~~zwei~~ drei neue Videos verfügbar:
https://diode.zone/c/pykurs_d/videos 

Das vollständige Farbenratespiel:  
https://diode.zone/w/nYKuAYy3YFpdyiFE3L3c2t

Die Funktion, die die Rateversuche aufmalt; interessant wegen des Vorgehens – wie komme ich von einer Idee (das soll so und so sein) zu Code?:    
https://diode.zone/w/sinNkZXP3yB485hEyCueYb

Eine Fehlersuche; ist vielleicht interessant nicht nur wegen des Fehlers, sondern auch wegen des Vorgehens:  
https://diode.zone/w/73aEVcZQSLXmraNaZYeXmK

## Hausaufgaben Recap

```python
 .get() 
```
    
kann direkt auf keys zugreifen

```python
 .values() 
```

gibt dict_values zurück. Wir können aber durch list() einfach eine Liste daraus machen

### List Comprehension und Dictionaries Comprehensions

= Kurzschreibweise

Statt eine Liste mit .append(dings) zu befüllen können wir alles in eine Zeile schreiben:

```python
neue_liste = [dings * 2 for dings in quelle]
```

> Problem: schwieriger zu lesen und for-Schleifen können schon fies genug sein. Lieber nicht machen und schönen Code schreiben.

### .isalpha()

.isalpha() gibt True oder False zurück und ist anwendbar auf Variablen und Literale.

## Aufgabe

```python

text = "Bei jedem klugen Wort von Sokrates rief Xanthippe zynisch: Quatsch!"

d = {}

for buchstabe in text:
    # print(buchstabe) # gibt jeden Buchstaben aus
    # print(buchstabe.isalpha()) # gibt nur den Wert True oder False zurück
    d[buchstabe] = buchstabe.isalpha() # befüllt das dictionary

print(d)
    
# Buchstaben zählen:

d = {"e": 0, "a":0, "o":0}

for buchstabe in text:
    if buchstabe in "eao":
        d[buchstabe] = d[buchstabe] + 1
        # hier muss kein .append() verwendet werden wie bei Listen
        # die eckigen Klammern packen das direkt in das Dictonary
    
print(d)

```

    


## Pikos Datei

```python
fahrradkeller = {"Rennrad":3, "Hollandrad":5, "Lastenrad":2}
#                  key    value
print("Rennrad:", fahrradkeller.get("Rennrad", "nicht da"))
print("Faltrad:", fahrradkeller.get("Faltrad", "nicht da"))

print(fahrradkeller["Rennrad"])
print(fahrradkeller["Faltrad"])  # KeyError
```
```python
fahrradkeller = {"Rennrad":3, "Hollandrad":5, "Lastenrad":2}
dings = fahrradkeller.values()

# print(fahrradkeller.keys())

print(dings)
print(type(dings))
print(list(dings))
print(type(list(dings)))
```
```python
# List Comprehension

eine_sammlung = [0, 1, 2, 3]  # Wir haben irgendeine Auflistung (Liste, Tupel, String, etc)

neue_sammlung = []  # Wir legen eine leere Liste an
for dings in eine_sammlung:  # wir gehen die erste Auflistung Stück für Stück durch
#     dongs = dings * 2  # machen was mit dem jeweiligen Stück 
    neue_sammlung.append(dings*2)  # und hängen es an die neue Liste an



neue_sammlung_2 = {dings:str(dings * 2)*3 for dings in eine_sammlung}


print(neue_sammlung)  # dann haben wir eine neue Liste!
print(neue_sammlung_2)
```
```python
woerter = ["Apfel", "Banane", "Clementine", "Dattel", "Erdbeere", "Feige", "Gurke", "Himbeere", "Ingwer", "Johannisbeere"]

d = {}
for wort in woerter:
    l = len(wort)
    print(wort, l)
    d[wort] = l
    
print(d)
```
```python
import random

woerter = ["Apfel", "Banane", "Clementine", "Dattel", "Erdbeere", "Feige", "Gurke", "Himbeere", "Ingwer", "Johannisbeere"]

d = {}
for wort in woerter:
    d[wort] = random.randint(1, 999)
    
print(d)
```
```python
zeichen_liste = ["a", "7", "l", "!", "B", "3", "x", " ", "Z", "ä"]

d = {}

for zeichen in zeichen_liste:
    print("\n\n\n")
    print("neuer Schleifendurchlauf!")
    print("Zeichen ist jetzt:", zeichen)
    print("d am Anfang:", d)
    
    d[zeichen] = zeichen.isalpha()
#     d ['7'] = False
    print("d am Ende:", d)
    input("Nächster Durchlauf?")
    
    
print(d)
```
```python
dings = "a"

dings.isalpha()
"a".isalpha()
```
```python
input("Was ist dein name?")  # Wartet auf die Eingabe, speichert sie aber nicht.

print("Hallo, Fred")
```
```python
text = """Findet heraus, was die Dictionary-Methode .values() macht. ⚡ Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

d = {}
for dings in text:
#     print(dings, dings.isalpha())  # Erstmal rausfinden, was das eigentlich macht...
    d[dings] = dings.isalpha()

print(d)
```
```python
# Zähle alle e, a und o in einem Text

text = """Findet heraus, was die Dictionary-Methode .values() macht. ⚡ Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

d = {"e": 0, "a": 0, "o": 0}
for zeichen in text:
#     print(dings, dings.isalpha())  # Erstmal rausfinden, was das eigentlich macht...
#     if zeichen == "e" or zeichen == "a" or zeichen == "o":
    if zeichen in "eao":
        d[zeichen] += 1
#         d[zeichen] = d[zeichen] + 1
    
print(d)
```