# Pikos Python Kurs am 19.12.2023

Auffrischung

## Pads



## Pikos Datei

``` 
Python = Programmiersprache
Thonny = IDE Integrated Devolpment Environment


Funktion
blabla()


Unterschied zwischen

7*4

print(7*4)




print(5*haus)
print(5* 7 )
print(  35 )

print(5*b)
print(5* "haus")
print("haushaushaushaushaus")


builtins: Küchentisch
Standard Library: Speisekammer
PyPI (Python package Index): Supermarkt
```

```python
haus = 7

a = 5  # int = Ganzzahl
a2 = 5.0  # float = Fließkommazahl

b = "haus"  # string
b2 = "c"

c = ["eins", "zwei", "drei"]  # list = Liste
d = (12, 23)  # tuple = Tupel
```
```python
wort = "Überschallknall"
#       0123456789   # "Null-Indizierung"
print(wort[4])  # Indizierung

# ! Off-by-One-Error

print(wort[4:7])  # Slicing
```
```python
liste = [10, 11, 12, 13, 14, 15, 16]
#        0    1   2   3   4   5   6789   # "Null-Indizierung"
print(liste[4])  # Indizierung

# ! Off-by-One-Error

print(liste[:])  # Slicing
```
```python
from random import randint
# ODER
import random
# ODER
from random import *

print(random.randint(1,3))  # wenn "import random"
```
```python
from turtle import *



# for schleifenvariable in wertevorrat:
    
for schleifenvariable in range(12):
    print(schleifenvariable * "*")
    
wertevorrat =  [["uiae", "nrtd", "asdf"], ["hui", "hui", "huiuiuiui", "hui"]]

for schleifenvariable in wertevorrat:
    print(schleifenvariable)
```
```python
from turtle import *

setup(800, 800, 2580, 10)

def ellipse_vom_rechten_rand():
    radius = -66 # Durch Ausprobieren gefunden
    pd()
    begin_fill()
    circle(radius/2, 45)
    circle(radius, 90)
    circle(radius/2, 90)
    circle(radius, 90)
    circle(radius/2, 45)
    end_fill()
    pu()
    
    
ellipse_vom_rechten_rand()

goto(100, 100)
ellipse_vom_rechten_rand()

# Aufgabe: Grün gefülltes Dreieck als Funktion
# Tipps: for-Schleife verwenden,
# forward(...) für Vorwärtsgehen, left(...) fürs Linksdrehen
```
```python
from turtle import *
setup(800, 800, 2580, 10)

pensize(20)

def gruenes_dreieck():
    color("green", (1, 0, 1))
    begin_fill()
    for i in range(3):
        forward(100)
        left(120)
    end_fill()
    
# forward fd
# left lt
# right rt
# backward bk

gruenes_dreieck()
```

```python
text = """Python = Programmiersprache
Thonny = IDE Integrated Devolpment Environment


Funktion
blabla()


Unterschied zwischen

7*4

print(7*4)




print(5*haus)
print(5* 7 )
print(  35 )

print(5*b)
print(5* "haus")
print("haushaushaushaushaus")


builtins: Küchentisch
Standard Library: Speisekammer
PyPI (Python package Index): Supermarkt"""

liste1 = text.split()
# print(liste1)

liste2 = []

for wort in liste1:
    if wort.isalpha():
        liste2.append(wort)
    elif len(wort) > 1:
        for zeichen in wort:
            if not zeichen.isalpha():
                ...
``` 

```python
alter = int(input())

...

if alter < 16:  # 123456789 10 11 12 13 14 15
    gib_osaft()
    
elif alter < 18:  # 16 17
    gib_bier()
    
elif alter < 66:
    gib_wein()

else:
    gib_schnaps()
    
    
    
if alter < 16:
    gib_osaft()
else:
    if alter < 18:
        gib_bier()
    else:
        if alter < 66:
            gib_wein()
```