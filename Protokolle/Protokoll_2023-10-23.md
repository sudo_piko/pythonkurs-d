# Pikos Python Kurs am 23.10.2023

## Frage
Im Kurs war noch eine Frage zu den letzten Hausaufgaben aufgekommen:
Mich würde bei der Aufgabe 3 interessieren, wie du dir den Farbverlauf genau vorgestellt hast. 

Pikos Antwort: Da wollte ich auch keinen Farbverlauf haben – am Ende sollen die Kacheln zufällig zusammengesetzt werden, da würde ein Farbverlauf eher stören. Mir fällt auf die Schnelle jetzt auch keine elegante Lösung ein – ich würde das vielleicht aus ganz vielen einzelnen Strichen zusammenbauen, aber das wird dann ziemlich fummelig... Wenn Du Lust hast, dann schick mir gern Deine Lösung, dann schaue ich mir das an!

## Protokoll
### Housekeeping

   - Haecksen Treffen in Berlin
   - FireShonks und Congress-Orga läuft an; bei Fragen gerne Mail an Piko


### If & Else
- Passwort-Programm
   - Möglichkeit 1: Passwort ist richtig, willkommen! Programm ist dann
   fertig.
   - Möglichkeit 2: Passwort war falsch, Programm bricht ab
   - input will input :) Im unteren Teil des Thonny-Fensters (Komandozeile)
   - bei "==" wird etwas verglichen. Wenn der Vergleich stimmt, wird
   “Willkommen” ausgegeben, wenn nicht wird “Das Passwort war falsch!” ausgegeben
   - **Zwei Gleichheitszeichen sind eine Frage, ein Gleichheitszeichen ist eine Aufforderung**
   - "if" ist wenn, "else" ist andernfalls
   - Einrückung ist wichtig weil es klarstellt, wozu etwas gehört.
- fizz
  - % Modulo-Operator: gibt den Rest an, der nicht durch eine Zahl geteilt
  werden kann: 13%5 = 3 weil 2*5 = 10 und es bleiben noch 3 übrig, die nicht
  ‘sauber’ durch 5 geteilt werden können
- Kekse:
  - Wenn ich nur print(anzahl*2) mache, nutzt Python den String und
  multipliziert nicht den input mit 2 sondern wiederholt den Input
  - Wenn es mehrere Fälle gibt, kann man diese Fälle voneinander
  unterscheiden mit elif. Bedeutet: wenn vorher nichts zutrifft und nichts
  ausgeführt wird: NUR DANN führe das aus. Ein If führt immer aus (solange die zugehörige Bedingung zutrifft) egal was vorher passiert ist.

### Hausaufgaben

   - Extra Aufgaben werden nicht im Kurs besprochen, bei Fragen Piko eine
   Email schreiben



## Pikos Dateien
### Passwort-Programm

Ziel:
```
Hallo!
Wer bist Du?
> Piko

Hallo Piko!
Bitte gib dein Passwort ein:
> supersicher


[1. Möglichkeit]
Willkommen! 

[2. Möglichkeit]
Das Passwort war falsch!
```
Programm:
```python
richtiges_passwort = "supersicher"

print("Hallo!")
name = input("""Wer bist du?
> """)

print("Hallo " + name + "!")

eingegebenes_passwort = input("""Bitte gib dein Passwort ein:
> """)

print("Dein Passwort ist: " + eingegebenes_passwort)



if richtiges_passwort == eingegebenes_passwort:  # wenn
    print("Willkommen!")
else:  # andernfalls
    print("Das Passwort war falsch!")
```

Variante:
```python
print("Hallo!")
name = input("Wer bist du? ")

print(name)

print("Hallo", name)

passwort = input("Bitte gib dein Passwort ein: ")


if passwort == "supersicher":   # direkt vergleichen => Passwort ist "hard coded"
    print("Willkommen!")
else:
    print("Das Passwort war falsch!")
    print("Du kannst es nicht zweimal versuchen!")  # Einrückung entscheidet, was zum if gehört und was nicht
```
### fizz
Ziel:
``` 
fizz
1
2
fizz
4
5
fizz
7
8
fizz
10
11
...
```
Tipps:
```
# Tipp 1
for i in range(20):
    print(i)
    

Tipp 2:
Findet heraus, was dieser Modulo-Operator nochmal macht: 3 % 2 und
so weiter...

Tipp 3:
for i in range(30):
    print(i%3)
```
Modulo:
```python
for i in range(20):
    print(i, i%5)
```
Das Programm:
```python
for i in range(16):
#     print(i, i%3)
    if i%3 == 0:
        print("fizz")
    else:
        print(i)
```
### Keksbeurteilungsprogramm
```python
anzahl = input("Wie viele Kekse?")
anzahl = int(anzahl)

# print(anzahl*2)

if anzahl < 10:  # == != < > <= >=
    print("Das sind zu wenig Kekse!")
elif anzahl < 100:
    print("Das sind ok viele Kekse!")
elif anzahl < 200:
    print("Das sind schon fast zu viele Kekse!")
else:
    print("Hui! Zu viele Kekse!")
```

### Cheatsheet  
![elif-Cheatsheet](../Cheatsheets/elif_Cheatsheet.png)