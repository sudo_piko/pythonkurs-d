# Pikos Python Kurs am 19.02.2024

## Protokoll
### Allgemeines
Wenn wir individuelle Fragen haben, gerne in den E-Mails dazuschreiben,
ob Piko ein öffentliches Video für alle machen darf.
Der Kurs geht voraussichtlich bis Ende März/Mitte April.

### Hausaufgabenbesprechung
#### 1 - Buchstaben zählen
Tipp: Zwischenschritte printen, um Probleme sichtbar zu machen, z.B.:
```python
d = {"e":0, "a":0, "o":0}
for zeichen in text:
    print("In Zeichen ist jetzt:", zeichen)
    if zeichen in "eao":
        print("Führe das if aus...")
        print("d ist anfangs:", d)
        d[zeichen] = d[zeichen] + 1 # Das, was rechts von "=" steht wird in d eingetragen
```

`d[zeichen] = d.get(zeichen, 0) + 1 # .get(argument1, argument2)`
---> Bekommt zwei Argumente:
argument1 schlägt String im Dictionary nach
Argument 2 enthält, was wir kriegen wollen, wenn es noch nicht im Dictionary ist.
Bsp.: telefonbuch.get("Bundeskanzler", "Hab ich nicht gefunden"/Fehlermeldung)
---> Dictionary kann hier leer bleiben: d = {} # wird befüllt

#### 2 - Mehr Fahrradkeller
While-Schleifen
=> ähnlich zu if
=> etwas kann True oder False werden:
```python
while True: # Wenn eins sich noch nicht entscheiden will am Anfang
    print("hello") # Endlos-Schleife kann mit Stopp beendet werden
```

#### 3 - Histogramm

-   Ansicht kann verändert werden: z.B.: ,,, 0.5, color=‚blue') #
    Balken werden schmaler, Farbe lässt sich ändern.

-   Überblick zur Buchstabenverteilung

-   Python hat sehr viele Pakete, mit denen eins Vieles machen kann -
    matplotlib gehört zu den weniger Verständlichen
    
### Wörterzählen
Tipp zum Satzzeichenputzen:
```
>>> w = ", hallo."
>>> w.strip(",.") # w.strip("§$%T§_:,-") wäre möglich
    # returnt: "hallo" geputzt
```    
    
### Aufgabe Grundgesetz
Kommandozeile: Speicherkapazität ist begrenzt, nur zum Ausprobieren
gedacht. Befehle zum Grundgesetz werden zum Teil nicht komplett angezeigt.


## Pikos Datei

```python
text = """Findet heraus, was die Dictionary-Methode .values() macht. ⚡ Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

d = {}
for zeichen in text:
#     print(dings, dings.isalpha())  # Erstmal rausfinden, was das eigentlich macht...
#     if zeichen == "e" or zeichen == "a" or zeichen == "o":  # Das funktioniert genauso wie die Zeile darunter
    if zeichen.isalpha():
        # d[zeichen] += 1  # Das funktioniert genauso wie die Zeile darunter
        d[zeichen.lower()] = d.get(zeichen.lower(), 0) + 1
#         d[zeichen] = d[zeichen] + 1
#         d[   "e" ] = d[  "e"  ] + 1
#         d[   "e" ] =     0      + 1
#         d[   "e" ] =        1
    
print(d)
```
```python
fahrradkeller = {"Rennrad": 3, "Mountainbike": 5, "Trekkingrad": 2, "E-Bike": 1}

print(fahrradkeller)

while True:
    fahrradtyp = input("Welcher Fahrradtyp? ").capitalize()
    fahrradkeller[fahrradtyp] = fahrradkeller.get(fahrradtyp, 0) + 1
#   d[zeichen.lower()] = d.get(zeichen.lower(), 0) + 1

#     fahrradkeller[fahrradtyp] = fahrradkeller[fahrradtyp] + 1
#   d            [zeichen]    = d            [zeichen]    + 1

    print(fahrradkeller)
```
```python
text = """Findet heraus, was die Dictionary-Methode .values() macht. ⚡ Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

d = {}
for zeichen in text:
#     print(dings, dings.isalpha())  # Erstmal rausfinden, was das eigentlich macht...
#     if zeichen == "e" or zeichen == "a" or zeichen == "o":  # Das funktioniert genauso wie die Zeile darunter
    if zeichen.isalpha():
        # d[zeichen] += 1  # Das funktioniert genauso wie die Zeile darunter
        d[zeichen.lower()] = d.get(zeichen.lower(), 0) + 1
#         d[zeichen] = d[zeichen] + 1
#         d[   "e" ] = d[  "e"  ] + 1
#         d[   "e" ] =     0      + 1
#         d[   "e" ] =        1
    
print(d)

import matplotlib.pyplot as plt

sortiertes_dictionary = dict(sorted(d.items(), key=lambda x: x[1], reverse=True))
print(sortiertes_dictionary)
plt.bar(sortiertes_dictionary.keys(), sortiertes_dictionary.values(), 0.5, color='b')
plt.show()
```
```python
text = """Findet heraus, was die Dictionary-Methode .values() macht. ⚡ Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

# woerterliste = text....

# Ein Dictionary, das die Wörter und nicht die Buchstaben zählt...
d = {}

# ["Findet", "heraus",...

for zeichen in text:  # Hier geh ich irgendwie alle einzelnen Zeichen durch. Das will ich nicht.
                        # Ich will alle Wörter durchgehen. Wie kriege ich eine Sammlung aller Wörter?
                        # Wie teile ich also einen String in einzelne Wörter auf? split! Das muss ich aber dann
                        # schon vor der for-Schleife machen. 
    if zeichen.isalpha():  # Ich will einfach alle Wörter, also kann ich auf das if verzichten.
                            # Aber evtl. muss ich das Wort noch von einem Komma oder so befreien...
        d[zeichen.lower()] = d.get(zeichen.lower(), 0) + 1  # auch bei den Wörtern ist .lower() ganz sinnvoll...
    
print(d)
```
```python
text = """Findet heraus, was die Dictionary-Methode .values() macht. Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

my_file = open("/home/piko/Downloads/Grundgesetz.txt", encoding="utf-8")  # Mac+Linux
text = my_file.read()

woerterliste = text.split()
# print(woerterliste)

# Ein Dictionary, das die Wörter und nicht die Buchstaben zählt...
d = {}

for wort in woerterliste:  # Aber evtl. muss ich das Wort noch von einem Komma oder so befreien...
    bereinigtes_wort = wort.lower().strip(".,;–()?!")
#     print(bereinigtes_wort)
    d[bereinigtes_wort] = d.get(bereinigtes_wort, 0) + 1  # auch bei den Wörtern ist .lower() ganz sinnvoll...
    
print(d["recht"])

# Wenn ich das alles noch anzeigen will, dann brauche ich [:100] an *genau* der richtigen Stelle:
import matplotlib.pyplot as plt
sortiertes_dictionary = dict(sorted(d.items(), key=lambda x: x[1], reverse=True)[:100])
print(sortiertes_dictionary)
plt.bar(sortiertes_dictionary.keys(), sortiertes_dictionary.values(), 0.5, color='b')
plt.show()
```
```python
my_file = open("/home/piko/Downloads/Grundgesetz.txt", encoding="utf-8")  # Mac+Linux
my_file = open("/home/piko/Downloads/Grundgesetz.txt", encoding="utf-8")  # Für ÜÄÖẞ-Probleme...

# bei Windows:
# my_file = open("C:\\Users\\Alexis\\Downloads\\Grundgesetz.txt")
# Oder mit einzelnen Backslashes, aber mit einem r vor dem String:
# my_file = open(r"C:\Users\Alexis\Downloads\Krasser Pythonkurs\Grundgesetz.txt")

text = my_file.read()

print(text)
print(type(text))  # gibt uns den typ aus => str
```