# Pikos Python Kurs am 18.12.2023
# Protokoll

## Allgemeines
Am 19.12.2023 findet der Auffrischungskurs statt. Wer kommt, kann gerne Fragen oder grundlegende Probleme mitbringen.


## Nachbesprechung der letzten Hausaufgaben
### Gedicht

Ausgangspunkt:
```python
woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]

laenge = randint(6, 12)
gedichtliste = choices(woertervorrat, k=laenge)
```
Es wird eine beliebig lange Gedichtliste zwischen 6 und 12 (`randint()` gibt zufällige Zahl in diesem Bereich aus) mit zufälligen Wörtern aus dem Vorrat erzeugt. Hier wird `choices()` genutzt, weil es Wortwiederholungen zulässt (wer das nicht möchte, muss `sample()` nutzen).

Als nächstes `write()` nutzen, um die Gedichtliste mit der turtle schreiben zu lassen. Statt "normal" kann auch "bold" oder "italic" gewählt werden.
```python
for wort in gedichtliste:
write(wort.upper(), align="center", font=("Persia", 30, "normal"))
```

Aktuell werden noch alle Wörter übereinander geschrieben, da ein Wort immer dort erscheind, wo die turtle sich gerade im Schleifendurchlauf befindet. Die turtle muss sich also nun bewegen:

```python
for wort in gedichtliste:
    #irgendwohingehen
    goto(randint(-300,300), randint(-300,300))
    #Wort schreiben lassen
    write(wort.upper(), align="center", font=("Arial", 30, "bold"))
```
Wenn man bei randint z.B. nur (0,100) nutzt, dann wird das "virtuelle" Quadrat, in dem die turtle malt, nur 100 x 100 pixel groß. Daher mit dem Wert etwas rumspielen.

Das Gedicht kann gerne mit anderen Texten ausprobiert werden. Inspiration gibt es hier: 
https://md.ha.si/Pv4gYTqsTf6DKYywKm-HIw#
https://www.ccc.de/hackerethik
https://uzhupisembassy.eu/uzhupis-constitution/
https://www.haecksen.org/wer-sind-die-haecksen/

Der gesamte Code nach der ersten Teilaufgabe:
```python
woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]

laenge = randint(6, 12)
gedichtliste = choices(woertervorrat, k=laenge)

for wort in gedichtliste:
    goto(randint(-300,300), randint(-300,300))
    write(wort.upper(), align="center", font=("Arial", 30, "normal"))
```

#### Teilaufgabe zum Gedicht: fonts aus einer fontliste zufällig auswählen

Liste von fonts, die jedes auf eigenem Computer hat, kann über folgenden Code ausgegeben werden (Ausgabe = tuple):
```python
from tkinter import Tk, font
root = Tk()
print(font.families())
```
Es besteht nun die Möglichkeit eine font von der `font.families()` mit `choice()` oder `choices()` random auszuwählen. Im weiteren Verlauf wird `choice()` genutzt:
```python
schriftart = choice(font.families())
    write(wort.upper(), align="center", font=(schriftart, 30, "normal"))
    print(schriftart)
```
Es kann sein, dass alle Schriftarten erstmal sehr gleich aussehen, da es viele Schriften gibt, die sich sehr ähnlich sehen. Über `print(schriftart)` kann aber überprüft werden, dass unterschiedliche Schriftarten ausgewählt werden.
#### Zusatz: ähnliche fonts aus der Auswahl auschließen
z.B.: alle fonts der Kategorie "Noto" aus dem Tupel ausschließen:
```python
from tkinter import Tk, font
root = Tk()
schriften = []
for schriftart in font.families():
    if schriftart[0:4] == "Noto":
        print("Uninteressante Schrift")
    else:
        schriften.append(schriftart) #interessante Schriften an neue Liste anhängen
```
Der komplette Code (mit Ausschluss der uninteressanten Schriften):
```python
from turtle import *
from random import choice, choices, randint
from tkinter import Tk, font
root = Tk()

schriften = []
for schriftart in font.families():
    if schriftart[0:4] == "Noto":
        print("Uninteressante Schrift")
    else:
        schriften.append(schriftart)

woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]
laenge = randint(6, 12)
gedichtliste = choices(woertervorrat, k=laenge)

for wort in gedichtliste:
    goto(randint(-300,300), randint(-300,300))
    schriftart = choice(schriften)
    write(wort.upper(), align="center", font=(schriftart, 30, "normal"))
    print(schriftart)
```
## Weihnachtliches Adventslicht

Als Inspiration aus dem generativen Adventskalender soll folgende Kerze nachgestellt werden:
![](https://md.ha.si/uploads/6198bb5f-24f4-495c-8d0c-8785e17c3f32.png)

Zuerst kann man mit den Kreisen für den Hintergrund beginnen. Für die Kerzenstreifen mit Farbverlauf eine eigene Funktion definieren. Die Methode `setheading()` wird erstmalig genutzt, um die Blickrichtung der turtle festzulegen: 
```python
from turtle import *

hintergrundfarbe_aussen = 0.3,0,0
hintergrundfarbe_mitte = 0.4,0.1,0.1
hintergrundfarbe_innen = 0.5,0.2,0.2
bgcolor(hintergrundfarbe_aussen)

#Farbübergang an der Kerze
def kerzenstreifen():
    schritte = 40
    gesamtlaenge = 300
    for i in range(20):
        color(i/20, 0, 1-(1/20))
        fd(gesamtlaenge/20)

#Hintergrund
pu()
fd(350)
color(hintergrundfarbe_mitte)
lt(90)
pd()
begin_fill()
circle(350)
end_fill()

lt(90)
fd(150)
rt(90)
begin_fill()
color(hintergrundfarbe_innen)
circle(200)
end_fill()

#mehrere Streifen für die Kerze
pu()
pensize(10) #für eine dickere Kerze
for i in range(10):
    goto((i-5)*10, 0) #Position der Kerzenstreifen auf Leinwand bestimmen
    setheading(-90) # setzt die Richtung, in die die turtle schauen soll
    pd()
    kerzenstreifen()
    pu()
```
Für die noch fehlende Teile des codes wird Piko uns in einer ausführlichen Hausaufgabe noch Schritt für Schritt anleiten.
## Nächster regulärer Termin am 08.01.2024: 
**Bis dahin wird Piko für uns ein neues Video erstellen, welches einen wichtgen neuen Sachverhalt zeigen wird, den man anschauen sollte. Allen eine schöne Weihnachtszeit :)**
## Pikos Datei
```python
from random import choices, randint, choice
from turtle import *

from tkinter import Tk, font
root = Tk()
##########################################
schriften = []                           #
for schriftart in font.families():       #
    if schriftart[0:4] == "Noto":        #    <= Dashier ist nur um die langweiligen Schriftarten
        print("Uninteressante Schrift")  #       auf Pikos Computer rauszufischen, die zufällig alle
    else:                                #       "Noto ..." heißen, 
        # Interessante Schrift           #
        schriften.append(schriftart)     #
##########################################

woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]
bgcolor(0.4, 0.1, 0.1)
color(0.6, 0.6, 0.6)
laenge = randint(6, 12)
gedichtliste = choices(woertervorrat, k=laenge)
# samples wäre ohne Wiederholung

# print(gedichtliste)

for wort in gedichtliste:
    # irgendwohingehen
    goto(randint(-300, 300), randint(-300,300))
    # Schriftart auswürfeln
    schriftart = choice(font.families())
    print(schriftart)
    # Wort schreiben lassen
    write(wort.upper(), align="center", font=(schriftart, 30, "bold"))
```
Wie funktioniert write?
```python
from turtle import *

wort = "Hallo"

fd(100)
write(wort.upper(), align="center", font=("Arial", 30, "bold"))
```
Welche Schriftarten kann ich verwenden?
```python
from tkinter import Tk, font
root = Tk()
print(type(font.families()))
```
Die Kerze findet Ihr im Projekteordner.