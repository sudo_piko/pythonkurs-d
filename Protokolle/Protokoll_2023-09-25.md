# Pikos Python Kurs am 25.09.2023


# RGB
https://de.wikipedia.org/wiki/Farbmischung#Additive_Farbmischung  
https://de.wikipedia.org/wiki/Additive_Farbmischung  

Vortragsvideo von der Gulasch-Programmiernacht 2022:  
https://media.ccc.de/v/gpn20-92-was-ist-eigentlich-farbe-


# Videos


### Videos zur Hausaufgabenbesprechung
- Variablen verändern: https://diode.zone/w/rZG4wywspRMMZMb4LzcrbU
- Regenbogenflagge: https://diode.zone/w/3UqKSC7DDjjR9fjBb9D6SM
- Regenbogen: https://diode.zone/w/4JGC4wTQYozdEyyErjdCXa

### Videos zu den Inhalten
Im Protokoll habe ich Euch Links und Hinweise gesammelt.
- Objekttypen in Python: https://diode.zone/w/3crbHWRnqrTFJkzMedLmVU
- RGB und for-Schleifen: https://diode.zone/w/97QKB7QQA7ev39R5hVmo39
