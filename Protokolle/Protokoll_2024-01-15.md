# Pikos Python Kurs am 15.01.2024

## Pads
Neue Wörter:  
https://md.ha.si/03OdwDaaQdC3kC7qZ7P7eg?both

## Angesprochene Wörter
* return-Wert = return value = Rückgabewert
* Default-Wert = Standardwert 
* Argument ~ Parameter
* Methode
* keyword
* statement
* Funktionsdefinition = Funktionskopf + Funktionskörper

## Ein Quiz
Das was in die Klammern bei einer Funktionsdefinition kommt, heißt "Argument" oder "Parameter"
<details> 
  <summary>Antwort </summary>
  Richtig
</details>

Den Doppelpunkt am Ende der Kopfzeile der Funktionsdefinition kann ich auch weglassen.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Ich kann in meinen selbstdefinierten Funktionen keine anderen selbstdefinierten Funktionen verwenden. 
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Die Argumente einer Funktion müssen immer durch Kommata getrennt werden. 
<details> 
  <summary>Antwort </summary>
  Richtig
</details>

Im Namen einer Funktion kann kein Leerzeichen vorkommen.
<details> 
  <summary>Antwort </summary>
  Richtig
</details>

Im Namen einer Funktion kann ein Bindestrich vorkommen.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Im Namen einer Funktion kann eine Zahl vorkommen.
<details> 
  <summary>Antwort </summary>
  Richtig, aber nicht ganz am Anfang
</details>

Im Namen einer Funktion kann ein Unterstrich vorkommen.
<details> 
  <summary>Antwort </summary>
  Richtig
</details>


Die Fehlermeldung "TypeError: dreieck() takes 0 positional arguments but 1 was given" bedeutet, dass wir im *Funktionsaufruf* vergessen haben, das Argument anzugeben, das wir in der *Funktiondefinition* mit eingeplant hatten.
<details> 
  <summary>Antwort </summary>
  Falsch, genau andersrum
</details>

Ich kann einer Funktion nur Zahlen als Argument übergeben.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

```def dreieck(laenge=200):```
200 ist der Return-Wert der Funktion
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Alle Argumente einer Funktion können Default-Werte haben. 
<details> 
  <summary>Antwort </summary>
  Richtig
</details>

Alle Argumente einer Funktion müssen in der Funktion verwendet werden.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

```python
liste = [1, 2, 3]
liste.append("vier")  # ist ein Methodenaufruf
```

<details> 
  <summary>Antwort </summary>
  Richtig
</details>

#### Bei den folgenden Codeschnipseln: Funktioniert das?

`def funktion argument1:`
<details> 
  <summary>Antwort </summary>
  Nein
</details>

`def funktion():`
<details> 
  <summary>Antwort </summary>
  Ja
</details>

`def funk(argument1 argument2):`
<details> 
  <summary>Antwort </summary>
  Nein
</details>

`def f(argument1):`
<details> 
  <summary>Antwort </summary>
  Ja
</details>

`def funktion1(argument1=20):`
<details> 
  <summary>Antwort </summary>
  Ja
</details>

`def funktion2(argument1=20, argument2):`
<details> 
  <summary>Antwort </summary>
  Nein
</details>

```python
def begruesse(hallo, person):
    return hallo * 2
```

<details> 
  <summary>Antwort </summary>
  Ja, ist aber unschön
</details>

#### Ab hier ist es wieder Richtig/Falsch
return ist eine Funktion.
<details> 
  <summary>Antwort </summary>
  Falsch, ein "statement"
</details>

Nach return kann ich nur *ein* Objekt angeben und dieses Objekt muss eine Zahl sein. 
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Der return-Wert einer Funktion kann nicht in einer Variablen gespeichert werden.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Eine Funktion kann zwei verschiedene Objekte zurückgeben.
<details> 
  <summary>Antwort </summary>
  Nicht wirklich, sie packt die beiden dann zusammen in ein Tupel.
</details>

Eine Funktion muss immer mindestens drei Zeilen haben.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Wenn ich das return vergesse/weglasse, gibt meine Funktion None zurück.
<details> 
  <summary>Antwort </summary>
  Richtig
</details>

Eine Funktion, die dreieck heißt, kann keinen return Wert haben.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Eine Funktion mit Return-Wert ist wie eine Orangensaftpresse: Da kommt etwas raus, was wir in einer Variable/einer kleinen Plastikflasche auffangen und dann weiterverwenden können.
<details> 
  <summary>Antwort </summary>
  Richtig
</details>

Es ist nie sinnvoll, eine Funktion mit nur einer Zeile Code im Funktionskörper zu schreiben, wenn das Programm ohne die Extra-Funktionsdefinition kürzer wäre und kürzer ist immer besser.
<details> 
  <summary>Antwort </summary>
  Falsch, manchmal ist länger verständlicher.
</details>

In print kann ich keine Funktionsaufrufe schreiben.
<details> 
  <summary>Antwort </summary>
  Falsch
</details>

Nach der Zeile mit return sollte ich in selbstdefinierten Funktionen immer auch noch ein print() aufrufen.
<details> 
  <summary>Antwort </summary>
  Falsch, nach der return-Zeile beendet sich die Funktion
</details>


## Pikos Dateien

```python
def drei_leckere_dinge():
    print("Lebkuchen, Mozartkugeln, Blutorangen")
    
#Im Namen einer Funktion kann ein Unterstrich vorkommen.

drei_leckere_dinge()
```

```python
# Fehlermeldung: dreieck() takes 0 positional arguments but 1 was given

# Versionen, die zu einer Fehlermeldung führen würden:
def dreieck(laenge):
    ...   
dreieck()

def dreieck():
    ...
dreieck(200)

#######

# Richtige Versionen:
def dreieck(laenge):
    ...
dreieck(200)

# ODER
def dreieck():
    ...
dreieck()
```

```python
def addiere(a, b):
    ergebnis = a + b
    return ergebnis

dings = addiere([1, 2, 3], [5, 6, 7])  # Unsere addiere-Funktion ist unerwartet vielseitig!
print(dings)


def dreieck(laenge=200, a="Marzipan"):
    print(laenge*a)
    print("Huiuiuiui")
    
dreieck()  # das ist eine sehr bösartige Benennung.    
```

```python
from turtle import *


def dreieck(laenge,
            nondefault_arg1,
            defaultarg1=3,
            nondef_arg2,  # HIER GIBT ES DANN EIN PROBLEM MIT DER REIHENFOLGE DER NON-DEFAULT- UND DEFAULT-ARGUMENTE
            strichdicke=5,
            defaultarg2="hallo"):
    pensize(strichdicke)
    for i in range(3):
        fd(laenge)
        lt(120)
        
dreieck(200)
fd(300)
dreieck(100, 20)
```

```python
liste = [1, 2, 3]
liste.append("vier")  # ist ein Methodenaufruf – append verändert die Liste, hat aber keinen return-Wert

z = 3

s = "hallo"
gross = s.upper()  # auch eine Methode, aber die hier mit return-Wert
```

```python
import random


def zufallskoordinaten():
    x = random.randint(-400, 400)
    y = random.randint(-400, 400)
    return x, y
    print(x, y)  # Wird nie ausgeführt
    
if 1 == 2:
    print("Uiuiuiui")  # Wird auch nie ausgeführt

dings = zufallskoordinaten()
print(dings)

print(print(zufallskoordinaten()))

def zufallsweg():
    entfernung = random.randint(0, 200)
    richtung = random.choice([">", "<", "^", "v"])
    return entfernung, richtung  # Gibt zwei *unterschiedliche* Dinge zurück: eine Zahl und einen String

print(zufallsweg())

```
