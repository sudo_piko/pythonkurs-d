# Pads

Hier sollen alle Pads gesammelt werden, die im Kurs entstehen.



Allgemeine schöne Dinge, Diverses:  
https://md.ha.si/yrJKysDRRa-ObovJSJdsEg#

## Hilfsmittel

Sammlung aller Wörter, ohne Erklärung:  
https://md.ha.si/03OdwDaaQdC3kC7qZ7P7eg#

## Turtle

Kreise, ab 2023-09-11:  
https://md.ha.si/6hVuSirgRrKxgsdwg9NF3w#

Farbübergangsmandalas, ab 2023-10-10:  
https://md.ha.si/cuv6MATUTCqjEiL_HyUKsw?both

Kachel-Austausch-Pad, ab 2023-10-16:  
https://md.ha.si/Qk9tc_6jSCWmNMlqvoTflQ#

Kachelflächen, ab 2023-10-30:
https://md.ha.si/tKMLUz2TQNGXt3yxPmb6Sw#