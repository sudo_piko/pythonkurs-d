# Generative Art mit Python

Es gibt einen neuen Kurs:  
https://gitlab.com/sudo_piko/pythonkurs-e

Mehr Infos unter https://www.haecksen.org/uncategorized/programmieren-lernen-fur-absolute-anfangerinnen-kurs-e-2024/

# Dingsdongs
Der Kurs ist am 25.03.2024 zu Ende gegangen. Die Materialien bleiben hier online. 

Einige der Aufgaben und Inhalte habe ich per Video besprochen:  
https://diode.zone/c/pykurs_d/videos

In den Protokollen findet Ihr dann noch die direkten Links zu den Videos.

# Für Gäste
Willkommen! Wenn Ihr mit diesen Materialien lernt, ohne im Kurs dabei zu sein, dann sendet mir doch mal einen lieben Gruß, 
zB auf Mastodon: piko@chaos.social

Ich freu mich immer sehr, wenn ich mitbekomme, dass der Kurs mehr Leuten hilft!

![](https://wauland.de/images/logo-blue.svg)

# Infos
Python für absolute Anfänger*innen

- Start: 04.09.2023
- Montags, 19:30 - 21:00
- bis April 2024
- Thema: schöne Dinge bauen; generative Art
- Unser Wappentier: Das Dingsdongs




Der Pythonkurs für Absolute Anfänger\*innen geht ab September 2023 in die nächste Runde. Eingeladen sind alle FINTA 
Personen, also Frauen, agender, inter, nonbinary, trans und agender Personen, die gerne programmieren lernen möchten;
ganz besonders die, die sich für einen hoffnungslosen Fall halten. Wir fangen bei **null** Programmiererfahrung an. 
Der Kurs wird sehr übungslastig sein; der Fokus des Kurses liegt darauf, schöne Dinge automatisch erstellen zu lassen,
also Generative Kunst. Ich habe der Wau-Holland-Stiftung[1] zu danken, deren Förderung mir ermöglicht, den Kurs kostenlos anzubieten!

Wir treffen uns ab dem 04.09. bis ins Frühjahr wöchentlich **montagabends 19:30** in einem BigBlueButton-Raum. Zusätzlich wird es Übungen geben und Ihr werdet Euch regelmäßig in Kleingruppen treffen.

Schreibt bei Interesse eine Mail an piko@riseup.net mit einer kurzen Vorstellung und Eurer Motivation, warum Ihr Programmieren lernen möchtet – von dort gibt es dann zeitnah eine Mail mit den Informationen für das erste Treffen.

[1] https://wauland.de/de/

