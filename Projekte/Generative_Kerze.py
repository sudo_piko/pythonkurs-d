from turtle import *
from random import randint

# Turtle setup
setup(800, 800, 2580, 10)  # nur für Piko
speed(0)

# Farben bestimmen und auswürfeln
hintergrundfarbe_aussen = 0, 0, randint(0,8)/10  # zB 0, 0, 0.3  # dunkles Blau
hintergrundfarbe_mitte = (
    hintergrundfarbe_aussen[0] + 0.1,
    hintergrundfarbe_aussen[1] + 0.1,
    hintergrundfarbe_aussen[2] + 0.1,
    )  # 0.1, 0.1, 0.4   # etwas helleres Blau
hintergrundfarbe_innen = (
    hintergrundfarbe_aussen[0] + 0.2,
    hintergrundfarbe_aussen[1] + 0.2,
    hintergrundfarbe_aussen[2] + 0.2,
    )  # 0.2, 0.2, 0.5   # noch helleres Blau


kerzenfarbe_unten_rot = randint(0,8)/10
kerzenfarbe_oben_gruen = randint(0, 5)/10
kerzenfarbe_oberseite = (0.3, kerzenfarbe_oben_gruen/2+0.3, 0.2)


# Weitere Zahlen bestimmen und auswürfeln
kerzenlaenge = randint(100, 350)
kerzenbreite = 100
streifen = 20



def kerzenstreifen():
    schritte = 40
    gesamtlaenge = kerzenlaenge
    for i in range(schritte):
        color(
            kerzenfarbe_unten_rot * (i/schritte),  # der rot-Wert * <0.0 bis ca. 0.99>
            kerzenfarbe_oben_gruen * (1-i/schritte),  # der blau-Wert * <1.0 bis ca. 0.01
            0  # der grün-Wert ist immer 0
            )
        fd(gesamtlaenge/schritte)

# ellipsen/ovale sind schwierig.
# richtige Ellipse: https://trinket.io/python/8875abe323
# Wir machen das ein bisschen gehackt, es ist keine *echte* Ellipse:
# kleiner Kreisausschnitt+Großer Kreisausschnitt:

def ellipse():  # Das ist unsere Vorlage, die beiden anderen Funktionen nehmen Teile daraus.
    radius = 100
    begin_fill()
    circle(radius, 90)
    circle(radius/2, 90)
    circle(radius, 90)
    circle(radius/2, 90)
    end_fill()
    
def halbe_ellipse_vom_rechten_rand():
    radius = -66 # Durch Ausprobieren gefunden
    pd()
    begin_fill()
    circle(radius/4, 45)
    circle(radius, 90)
    circle(radius/4, 45)
    end_fill()
    
def ellipse_vom_rechten_rand():
    radius = -66 # Durch Ausprobieren gefunden
    pd()
    begin_fill()
    circle(radius/4, 45)
    circle(radius, 90)
    circle(radius/4, 90)
    circle(radius, 90)
    circle(radius/4, 45)
    end_fill()
    
def flammenseite(radius):  # Hier passiert etwas Neues! Wir "übergeben" ein Argument... Das gibt es dann im Video genauer!
    begin_fill()
    for i in range(10):
        circle(radius*(i+1), 90/7)
    fd(abs(radius)*1.6)  # abs macht aus `-10` `10`; 1.6 durch Probieren rausgefunden
    end_fill()

def flamme():
    color(0.8, 0.6, 0)
    pd()
    setheading(0)
    groesse = 20
    
    # äußere Flamme
    flammenseite(groesse)
    pu()
    goto(0,0)
    setheading(180)
    flammenseite(-groesse)
    
    # innere Flamme
    pu()
    goto(0,0)
    setheading(0)
    color(0.9, 0.7, 0.6)
    flammenseite(groesse*0.6)
    pu()
    goto(0,0)
    setheading(180)
    flammenseite(-groesse*0.6)
    
    

bgcolor(hintergrundfarbe_aussen)

# äußerer Kreis
pu()
fd(350)
color(hintergrundfarbe_mitte)
lt(90)
pd()
begin_fill()
circle(350)
end_fill()

# innerer Kreis
lt(90)
fd(200)
rt(90)
color(hintergrundfarbe_innen) 
begin_fill()
circle(150)
end_fill()

# Kerzenkörper
pu()
breite_kerzenstreifen = kerzenbreite/streifen
pensize(breite_kerzenstreifen)
tracer(0)  # Turtlemalen wird nicht angezeigt
for i in range(streifen+1):  # +1, sonst wird es schief
    goto((i-(streifen/2)) * breite_kerzenstreifen, 0)
    pd()
    setheading(-90)
    kerzenstreifen()
    pu()
tracer(1)  # alles, was die Turtle seit "tracer(0)" gemalt hätte, wird auf einen Schlag angezeigt

# untere Ellipse
lt(90)
fd(kerzenbreite/streifen/2-1)  # -1 wegen Rundungsfehler
rt(90)
color(kerzenfarbe_unten_rot,0,0)
pensize(1)
halbe_ellipse_vom_rechten_rand()

# Obere Ellipse = Oberseite der Kerze
pu()
fd(kerzenlaenge)
color(kerzenfarbe_oberseite)
ellipse_vom_rechten_rand()

# Flamme
pu()
goto(0,0)
flamme()
goto(0,0)

exitonclick()