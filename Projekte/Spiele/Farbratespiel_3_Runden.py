from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")
ANZAHL_RUNDEN = 3  # eine Konstante


# Nützliche Funktionen
def farbe_verzehnfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


def bewerten(fehlergroesse):
    # printet eine Bewerung anhand der fehlergroesse
    if fehlergroesse > 0.5:
        print("Das ist aber nicht besonders gut!")
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        print("Du hast ok geschätzt.")
    elif fehlergroesse > 0.1:
        print("Fast!")
    else:
        print("Wow, übermenschlich!")

def punkte_ausrechnen(fehlergroesse):
    # Rechnet von der fehlergroesse aus einen Punktzahl aus.
    # Hier kommt noch Mathematik
    if fehlergroesse > 0.5:
        return 0
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        return 20
    elif fehlergroesse > 0.1:
        return 40
    else:
        return 70
    
punktestand = 0
for i in range(ANZAHL_RUNDEN):
    # Eine Zufallsfarbe generieren und anzeigen
    clear()
    farbe_richtig = (random(), random(), random())
    # zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93 
    bgcolor(farbe_richtig)


    # Raten lassen
    rate_string = input(
        "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
    print("Dein Rateversuch: " + rate_string)


    # Den Rateversuch verarbeiten
    farbe_geraten = []

    ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
    liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
    # farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


    # Den Rateversuch aufmalen lassen:
    color(farbe_geraten)
    pensize(100)
    dot()

    # Ergebniswerte ausrechnen
    differenz = farben_differenz(farbe_richtig, farbe_geraten)
    punkte = punkte_ausrechnen(differenz)
    punktestand = punktestand + punkte
    
    # Auf Ergebnis reagieren
    print("Richtiges Ergebnis", farbe_verzehnfachen(farbe_richtig))  # 80 94 57
    print("Du lagst um", differenz, "daneben.")
    bewerten(differenz)
    print("Du bekommst", punkte, "Punkte!")
    print("Du hast jetzt", punktestand, "Punkte.")
    input("Weiter mit Enter ... ")

exitonclick()



