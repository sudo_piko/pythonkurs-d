# Dieses Programm ist dafür da, die Funktion im Multiplayer-Spiel besser zu verstehen.
# Es gibt ein Video dazu: https://diode.zone/w/sinNkZXP3yB485hEyCueYb

from turtle import *

d = {'Alex': (0.8, 0.5, 0.1), 'Kim': (0.1, 0.6, 0.8), 'Robin': (0.5, 0.5, 0.5), "Piko": (0.1, 0.05, 0.2)}


BREITE = 800
BREITE_ANZEIGEFELD = BREITE - 200
setup(BREITE, 800, 3580, 20)

def rateversuche_aufmalen(rateversuche_dict):
    anzahl_versuche = len(rateversuche_dict)
    if anzahl_versuche == 0:
        print("Fehler! Keine Einträge im Dictionary rateversuche_dict")
        return
    pu()
    pensize(50)
    counter = 0
    for person in rateversuche_dict:
        richtige_farbe = rateversuche_dict[person]
        print(richtige_farbe)
        color(richtige_farbe)
        
        if anzahl_versuche == 1:
            goto(0, 0)
        else:
            x_erster_dot = BREITE_ANZEIGEFELD/2 * -1
            schrittweite = BREITE_ANZEIGEFELD / (anzahl_versuche - 1)
            x_aktuell =    x_erster_dot + counter * schrittweite
            #               -300          0, 1, 2...  150
            goto(x_aktuell, 0)
            
#             goto((BREITE/2 * -1) + counter * (BREITE/ (anzahl_versuche -1)), 0)
        dot()
        
        counter += 1

rateversuche_aufmalen(d)
        
        
        
# fahrradkeller = {"Rennrad":10, "Faltrad":5}
# #                  key    :value
# print(fahrradkeller["Rennrad"])