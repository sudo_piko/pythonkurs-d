from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")


# Nützliche Funktionen
def farbe_verzehnfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


# Eine Zufallsfarbe generieren und anzeigen
farbe_richtig = (random(), random(), random())
zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93
bgcolor(zufallsfarbe)


# Raten lassen
rate_string = input(
    "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
print("Dein Rateversuch: " + rate_string)


# Den Rateversuch verarbeiten
farbe_geraten = []

ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
# farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

for ziffernschnipsel in liste_ratestrings:  # ["30", "89", "10"]
    kommazahl = int(ziffernschnipsel) / 100  # zB 30 => 0.3
    farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


# Den Rateversuch aufmalen lassen:
color(farbe_geraten)
pensize(100)
dot()

print(farben_differenz(farbe_richtig, farbe_geraten))

print("Richtiges Ergebnis", farbe_verzehnfachen(farbe_richtig))  # 80 94 57

exitonclick()
