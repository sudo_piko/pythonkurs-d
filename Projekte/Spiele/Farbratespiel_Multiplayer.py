from turtle import *
from random import random

# Setup
ANZAHL_RUNDEN = 3  # eine Konstante
BREITE = 400
BREITE_ANZEIGEFELD = BREITE - 100

setup(BREITE, 400, 3580, 500)
title("Pikos Farbenratspiel")


#  Funktionen
def farbe_verhundertfachen(rgb_tupel):
    # rein: (0.5, 0.7, 0.8)
    # raus: [50, 70, 80]
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


def bewerten(fehlergroesse):
    # printet eine Bewerung anhand der fehlergroesse
    if fehlergroesse > 0.5:
        print("Das ist aber nicht besonders gut!")
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        print("Du hast ok geschätzt.")
    elif fehlergroesse > 0.1:  # 0.1 bis 0.2
        print("Fast!")
    else:
        print("Wow, übermenschlich!")


def punkte_ausrechnen(fehlergroesse):
    # Rechnet von der fehlergroesse aus einen Punktzahl aus.
    # Hier könnte noch Mathematik
    if fehlergroesse > 0.5:
        return 0
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        return 20
    elif fehlergroesse > 0.1:
        return 40
    else:  # "andernfalls"
        return 70
    
    
def versuch_verarbeiten(rate_string):
    # zB "30 89 10 "  =>  (0.3, 0.89, 0.1)
    
    # Den Rateversuch verarbeiten
    farbe_geraten = []

    ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
    liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]

    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]
    ergebnis = tuple(farbe_geraten)
    return ergebnis


def reagieren_und_punkte_vergeben(rateversuche_dict, spielende_dict, farbe_richtig):
    for person in rateversuche_dict:
        farbe_geraten = rateversuche_dict[person]
    
        # Ergebniswerte ausrechnen
        differenz = farben_differenz(farbe_richtig, farbe_geraten)
        punkte = punkte_ausrechnen(differenz)
        
        # Den Punktestand aktualisieren
        spielende_dict[person] = spielende_dict[person] + punkte  
        print(spielende_dict)
        
        # Auf Ergebnis reagieren
        print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57
        print("Du lagst um", differenz, "daneben.")
        bewerten(differenz)
        print("Du bekommst", punkte, "Punkte!")
        print(person +" hat jetzt", spielende_dict[person], "Punkte.")
        print()


def rateversuche_aufmalen(rateversuche_dict):
    anzahl_versuche = len(rateversuche_dict)
    if anzahl_versuche == 0:
        print("Fehler! Keine Einträge im Dictionary rateversuche_dict")
        return
    pu()
    pensize(50)
    counter = 0
    for person in rateversuche_dict:
        richtige_farbe = rateversuche_dict[person]
        print(richtige_farbe)
        color(richtige_farbe)
        
        if anzahl_versuche == 1:
            goto(0, 0)
        else:
            x_erster_dot = BREITE_ANZEIGEFELD/2 * -1
            schrittweite = BREITE_ANZEIGEFELD / (anzahl_versuche - 1)
            x_aktuell =    x_erster_dot + counter * schrittweite
            #               -300          0, 1, 2...   150
            goto(x_aktuell, 0)
            
            # goto((BREITE/2 * -1) + counter * (BREITE/ (anzahl_versuche -1)), 0)
        dot()
        counter += 1
    
    
# Anfang: Eingeben lassen, wer alles spielt
spielende = {}
while True:
    person = input("Wie heißt Du? (Weiter mit 'Weiter')")
    if person.lower() == "weiter" or person == "":
        break
    spielende[person] = 0
    
print(spielende)


for i in range(ANZAHL_RUNDEN):
    # Eine Zufallsfarbe generieren und anzeigen
    clear()
    farbe_richtig = (random(), random(), random())
    # zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93 
    bgcolor(farbe_richtig)

    # Raten lassen
    rateversuche = {}
    for person in spielende:  # zB Alex, Kim, Robin
        versuch = input("Rateversuch von " + person + ": ")  # "30 89, 12 "
        print(person + "s Versuch war " + versuch)
        versuch = versuch_verarbeiten(versuch)  # (0.3, 0.89, 0.12)
        rateversuche[person] = versuch
    print("Alle Rateversuche:", rateversuche)
    
    # Den Rateversuch aufmalen lassen:
    rateversuche_aufmalen(rateversuche)
    reagieren_und_punkte_vergeben(rateversuche, spielende, farbe_richtig)
        
    for person in spielende:
        print("Punktestand für " + person +":")
        print(spielende[person])
  
    input("Weiter mit Enter ... ")

exitonclick()
