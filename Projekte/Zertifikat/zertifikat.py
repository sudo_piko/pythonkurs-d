# https://www.appsloveworld.com/coding/python3x/146/how-can-i-save-a-turtle-canvas-as-an-image-png-or-jpg
from turtle import *


# Setup
LEINWANDBREITE = 1500  # Dashier könnte noch A4 werden
LEINWANDHOEHE = 1000
setup(LEINWANDBREITE, LEINWANDHOEHE, 0,0)
speed(0)

NAME = "Ithea Piko Nwawa"


# Ein Turtle-Bild malen
pu()
goto(-500, 200)
pd()
for i in range(20):
    fd(10)
    rt(10)
    circle(100, 350)
pu()


# Alle bekannten Schriftarten anzeigen:
# from tkinter import Tk, font
# root = Tk()
# print(font.families())

# Zertifikatstext
goto(0, 200)

write("ZERTIFIKAT", align="center", font=("Times New Roman", 50))

goto(0, 0)
write(NAME, align="center", font=("Times New Roman", 30))
    
goto(0, -200)
write("hat am Pythonkurs für Absolute Anfänger*innen teilgenommen.", align="center", font=("Times New Roman", 30))


# Rahmen
anzahl_punkte_horizontal = 40
anzahl_punkte_vertikal = 15
bemalte_breite = LEINWANDBREITE - 100
bemalte_hoehe = LEINWANDHOEHE - 100
pensize(10)

# oben
for i in range(anzahl_punkte_horizontal):
    goto(-bemalte_breite/2 + i * bemalte_breite / anzahl_punkte_horizontal, bemalte_hoehe/2)
    dot()

# rechts
for i in range(anzahl_punkte_vertikal):
    goto(bemalte_breite/2, bemalte_hoehe/2 - i * bemalte_hoehe / anzahl_punkte_vertikal)
    dot()

# unten
for i in range(anzahl_punkte_horizontal):
    goto(bemalte_breite/2 - i * bemalte_breite / anzahl_punkte_horizontal, -bemalte_hoehe/2)
    dot()

# links
for i in range(anzahl_punkte_vertikal):
    goto(-bemalte_breite/2, -bemalte_hoehe/2 + i * bemalte_hoehe / anzahl_punkte_vertikal)
    dot()

# Turtle zeigen
shape("turtle")
color("green")
shapesize(10)
goto(400, 200)


# Exportieren
canvas = getscreen().getcanvas()

canvas.postscript(file="turtle_img.ps")  # Saves as a .ps file

from PIL import Image
turtle_img = Image.open("turtle_img.ps")
turtle_img.save("turtle_img.png", "png")

exitonclick()