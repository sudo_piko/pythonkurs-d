ausgang = "überzüchten"
loesung = "brühe"

for i in range(len(ausgang)):
    # alle buchstaben durchgehen
    if loesung[0] == ausgang[i]:  # ist es ein "b", als der erste Buchstabe von l?
        anfang = i
        

for i in range(anfang, len(ausgang)):
    # gehe alle Buchstaben vom "b" aus durch
    if loesung[1] == ausgang[i]:
        zweiter = i  # das "r" ist an der i-ten Stelle.
        
schrittweite = zweiter - anfang

ende = anfang + len(loesung)*schrittweite

# print(ausgang[ende])
# Ergibt Fehlermeldung, weil zu hoch, aber es ist
# ja auch die obere Grenze, und deswegen funktioniert
# folgende Zeile trotzdem:

print(ausgang[anfang:ende:schrittweite])

