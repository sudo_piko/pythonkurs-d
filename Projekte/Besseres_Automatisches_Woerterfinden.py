ausgang = "backbacksteinern"
# ausgang2 = "aaaaaaabbbrrrüüühhheee"
loesung = "aktien"

# suche loesung[0] im ausgangswort => anfangs_index, zB 1
for i in range(len(ausgang)):
    if ausgang[i] == loesung[0]:
        anfangs_index = i
        
    
        # suche loesung[1] im ausgangswort (aber nach anfangs_index) => index_zweiter_buchstabe zB 3
        for i in range(anfangs_index, len(ausgang)):
            if ausgang[i] == loesung[1]:
                index_zweiter_buchstabe = i
            
                # schrittweite: 3-1  index_z_b - anfangs_i
                schrittweite = index_zweiter_buchstabe - anfangs_index

                # länge des abschnittes im ausgangswort: schrittweite * len(loesung), zB 2 * 5 = 10
                laenge_abschnitt = schrittweite * len(loesung)

                # end_index: anfangs_index + laenge des wortes, zB 11
                end_index = anfangs_index + laenge_abschnitt

                # print(ausgang[1:11:2])
                vermutete_loesung = ausgang[anfangs_index:end_index:schrittweite]
                if vermutete_loesung == loesung:
                    break

print(vermutete_loesung)
print(anfangs_index, end_index, schrittweite)