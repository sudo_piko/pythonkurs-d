from turtle import *

import time

setup(800, 800, 3580, 20)
speed(0)


strichdicke = 5
pensize(strichdicke)

for i in range(40):
    color(0, 0, 1-i/40)
    radius = i*strichdicke
    # pu()
    # rt(90)
    # fd(radius/2)
    # lt(90)
    # pd()
    circle(radius)

time.sleep(10)