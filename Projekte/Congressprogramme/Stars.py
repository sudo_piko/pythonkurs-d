from turtle import *
from random import randint

setup(800, 800, 2580, 20)
speed(0)


strichdicke = 50
pensize(strichdicke+2)

anzahl_kreise = 6

def gehe_zu_zufallsort():
    hoehe = 200
    breite = hoehe
    zufalls_x = randint(-breite, breite)
    zufalls_y = randint(-hoehe, hoehe)
    goto(zufalls_x, zufalls_y)

def stern(sternenradius):
#     sternenradius = randint(1, 100)
    pd()
    begin_fill()
    circle(sternenradius, 90)
    fd(sternenradius/3)
    lt(180)
    fd(sternenradius/3)
    
    circle(sternenradius, 90)
    lt(180)
    
    circle(sternenradius, 90)
    fd(sternenradius/3)
    lt(180)
    fd(sternenradius/3)
    
    circle(sternenradius, 90)
    lt(180)
    end_fill()
    pu()
    

# 
for i in range(anzahl_kreise):
    color(0, 0, 1-i/anzahl_kreise)
    radius = i*strichdicke
    pu()
    rt(90)
    fd(radius)
    lt(90)
    pd()
    circle(radius)
    pu()
    lt(90)
    fd(radius)
    rt(90)
    


color(1,1,1)
pensize(1)
for i in range(100):
    gehe_zu_zufallsort()
    stern(10)
    