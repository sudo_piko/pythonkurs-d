from random import choices, randint
from turtle import *
from time import sleep

bgcolor("#46090e")
color("#868686")

ethik = """
    Der Zugang zu Computern und allem, was einem zeigen kann, wie diese Welt funktioniert, sollte unbegrenzt und vollständig sein.
    Alle Informationen müssen frei sein.
    Mißtraue Autoritäten — fördere Dezentralisierung.
    Beurteile einen Hacker nach dem, was er tut, und nicht nach üblichen Kriterien wie Aussehen, Alter, Herkunft, Spezies, Geschlecht oder gesellschaftliche Stellung.
    Man kann mit einem Computer Kunst und Schönheit schaffen.
    Computer können dein Leben zum Besseren verändern.
    Mülle nicht in den Daten anderer Leute.
    Öffentliche Daten nützen, private Daten schützen.
"""

woertervorrat = []

for wort in ethik.split():
    woertervorrat.append(wort.strip(",.").upper())
    
# print(woertervorrat)



laenge = randint(6, 25)
gedichtliste = choices(woertervorrat, k=laenge)

# print(gedichtliste)

pu()
hideturtle()
for wort in gedichtliste:
    sleep(randint(5, 40)/10)

    goto(randint(-400, 400), randint(-200, 200))
    write(wort, align="center", font=("FreeMono", 10, "bold"))