from turtle import *

setup(800, 800, 3580, 20)
speed(0)


strichdicke = 10
pensize(strichdicke)

for i in range(20):
    color(i/20, 0, 0)
    radius = i*strichdicke
    pu()
    rt(90)
    fd(radius/2)
    lt(90)
    pd()
    circle(radius)
    pu()
    lt(90)
    fd(radius/2)
    rt(90)