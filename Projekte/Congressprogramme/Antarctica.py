from turtle import *
from random import randint

pensize(10)

speed(0)
n = 240

color("black", "blue")
begin_fill()
for i in range(n):
    t = randint(0,3)
    lt(randint(-10, 10))
    if t > 2:
        fd(randint(0,30))
    elif t == 2:
        r = randint(-50, 50)
        circle(r, randint(30, 120))
        if randint(0,1) > 0:
            circle(-r, randint(30, 120))
    elif t == 3:
        ...
end_fill()