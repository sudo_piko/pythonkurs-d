from turtle import *
from random import randint

setup(800, 800, 20, 20)

tracer(0) 
seitenlaenge = 80
dicker_stift = 12

speed(10)
bgcolor((0.6, 0, 0.2))
pencolor("white")
pu()

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()

        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    
    ttile_1()
    
    rt(90)
    back(seitenlaenge)
    
    
def kreuzung_oben():
    # der strich von links nach rechts liegt oben.
    pensize(dicker_stift)
    pu()
    # Strich von unten nach oben
    fd(seitenlaenge/2)
    pd()
    lt(90)
    fd(seitenlaenge/2 - dicker_stift*1.5)
    pu()
    fd(dicker_stift * 3)
    pd()
    fd(seitenlaenge/2 - dicker_stift*1.5)
    pu()
    # Strich von rechts nach links
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    pd()
    fd(seitenlaenge)
    # Zurück zum Ausgangspunkt
    lt(90)
    pu()
    fd(seitenlaenge/2)
    lt(90)

def kreuzung_unten():
    # Wie kreuzung_oben(), nur um 90 Grad gedreht
    pu()
    fd(seitenlaenge)
    lt(90)
    kreuzung_oben()
    rt(90)
    bk(seitenlaenge)
    
for j in range(10):
    for i in range(10):
        goto(i*seitenlaenge-400, j*seitenlaenge-400)
        zahl = randint(1, 10)
        if zahl == 1:
            kreuzung_unten()
        elif zahl == 2:
            kreuzung_oben()
        elif zahl < 7:
            ttile_1()
        else:
            ttile_2()


tracer(1)