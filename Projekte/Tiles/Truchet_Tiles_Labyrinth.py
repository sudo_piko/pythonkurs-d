from random import randint
from turtle import *

bgcolor(0,0,1)

setup(800, 800, 2560, 10)
pensize(2)
seitenlaenge = 40
speed(10)
pu()

dicker_stift = 4
duenner_stift = 1

pensize(duenner_stift)
# pu()

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        
        
def strich_rechtsoben():
#     quadrat()
    pd()
    pensize(dicker_stift)
    lt(45)
    fd(2**0.5 * seitenlaenge)
    back(2**0.5 * seitenlaenge)
    rt(45)
    pu()


def strich_linksoben():
    fd(seitenlaenge)
    lt(90)
    
    strich_rechtsoben()
    
    rt(90)
    back(seitenlaenge)
    
    
    
for j in range(16):
    for i in range(16):
        goto(i * seitenlaenge-300, j * seitenlaenge-200)
        zufallszahl = randint(1, 6)
        print(zufallszahl)
        if zufallszahl == 1:
            strich_linksoben()
        else:
            strich_rechtsoben()
            

