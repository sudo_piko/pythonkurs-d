from turtle import *
from random import randint

# setup(800, 800, 2560, 10)
# tracer(0)
seitenlaenge = 50
dicker_stift = 19
duenner_stift = 0

pensize(duenner_stift)
# pu()

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        

def ttile_1():
    pu()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()
        pensize(duenner_stift)
        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    
    ttile_1()
    
    rt(90)
    back(seitenlaenge)
    
    

for j in range(20):
    for i in range(20):
        pu()
        goto(i*seitenlaenge-400, j*seitenlaenge-400)
        a = randint(0, 1)
        if a == 0:
            ttile_1()
        else:
            ttile_2()

# tracer(1)