from turtle import *


setup(800, 800, 2560, 10)

seitenlaenge = 200
dicker_stift = 40
duenner_stift = 0

pensize(duenner_stift)
# pu()

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        
        
def strich_rechtsoben():
    quadrat()
    pd()
    pensize(dicker_stift)
    lt(45)
    fd(2**0.5 * seitenlaenge)
    back(2**0.5 * seitenlaenge)
    rt(45)
    pu()


def strich_linksoben():
    fd(seitenlaenge)
    lt(90)
    
    strich_rechtsoben()
    
    rt(90)
    back(seitenlaenge)
    
    
strich_linksoben()