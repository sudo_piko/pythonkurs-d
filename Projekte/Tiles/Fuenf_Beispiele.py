from turtle import *
tracer(False)
pu()
bk(1200)
l = 100
pensize(4)


# Das hier ist ein gutes Beispiel für schlechten Code. Ich hab das letzte Woche geschrieben
# und kann mich schon gar nicht mehr orientieren.....

def q():
    pu()
    fd(l+l/10)
    pd()
    for i in range(4):
        fd(l)
        lt(90)
   
# Quadrat mit Streifen
q()
for i in range(6):
    lt(45)
    fd(0.65*l)
    bk(0.65*l)
    rt(45)
    fd(l/10)

bk(l/2 + l/10)
lt(90)
fd(l)
rt(90)
fd(l)
rt(180)
for i in range(6):
    lt(45)
    fd(0.65*l)
    back(0.65*l)
    rt(45)
    fd(l/10)

fd(4*l/10)
lt(90)
fd(l)
lt(90)

# Quadrat mit Kreisen
q()

for i in range(2):
    fd(l/4)
    circle(l/4)
    fd(l/4)
bk(l)
lt(90)
fd(l)
rt(90)
for i in range(2):
    fd(l/4)
    circle(-1*l/4)
    fd(l/4)
bk(l)
lt(-90)
fd(l)
rt(-90)


q()
for j in range(3):
    for i in range(3):
        pu()
        fd(l/4)
        pd()
        circle(l/4)
    pu()
    bk(l*0.75)
    lt(90)
    fd(l/4)
    rt(90)

rt(90)
fd(l*0.75)
lt(90)


q()

for i in range(4):
    fd(l/2)
    lt(90)
    circle(l/2, 90)
    rt(90)
    fd(l/2)
    rt(180)

q()
for i in range(3):
    pu()
    fd(l/4)
    pd()
    lt(90)
    fd(l)
    bk(l)
    rt(90)
pu()
bk(l*0.75)
lt(90)
fd(l)
rt(180)
for i in range(3):
    pu()
    fd(l/4)
    pd()
    lt(90)
    fd(l)
    bk(l)
    rt(90)

fd(l/4)
lt(90)
tracer(1)