from turtle import *
from random import randint

seitenlaenge = 50
dicker_stift = 10

pensize(dicker_stift)

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        
def viertelquadrat():
    pensize(1)
    
    dicke = dicker_stift/2
    entfernung_von_ecke = dicke/4
    # Diese Werte kommen durch Raten und Rumprobieren.
    
    fd(entfernung_von_ecke)
    begin_fill()
    # äußere Kontur:
    fd(dicke)
    lt(90)
    fd(entfernung_von_ecke + dicke)
    lt(90)
    fd(entfernung_von_ecke + dicke)
    lt(90)
    fd(dicke)
    lt(90)
    # innere Kontur
    fd(entfernung_von_ecke)
    rt(90)
    fd(entfernung_von_ecke)
    
    end_fill()
    lt(90)  # jetzt ist die Blickrichtung wie am Anfang
    fd(-1 * entfernung_von_ecke)  # zurück zum Ausgangspunkt
    
    
def vier_eckchen_in_die_ecken():
    for i in range(4):
        viertelquadrat()
        fd(seitenlaenge)
        lt(90)
    pensize(dicker_stift)
    
    
def ttile_1():
    pu()
    vier_eckchen_in_die_ecken()
    for i in range(2):
        fd(seitenlaenge/2)
        lt(90)
        pensize(dicker_stift)
        pd()
        circle(-seitenlaenge/2, 90)
        pu()

        lt(90)
        fd(seitenlaenge/2)
        lt(90)
       
       
def ttile_2():
    pu()
    fd(seitenlaenge)
    lt(90)
    
    ttile_1()
    
    rt(90)
    back(seitenlaenge)
    
    
def ttile_kreuzung():
    vier_eckchen_in_die_ecken()
    pu()
    fd(seitenlaenge/2)
    pd()
    lt(90)
    fd(seitenlaenge)
    pu()
    rt(90)
    fd(seitenlaenge/2)
    rt(90)
    fd(seitenlaenge/2)
    pd()
    rt(90)
    fd(seitenlaenge)
    pu()
    lt(90)
    fd(seitenlaenge/2)
    lt(90)
    
    
for j in range(20):
    for i in range(20):
        pu()
        goto(i*seitenlaenge-200, j*seitenlaenge-200)
        a = randint(0, 2)
        if a == 0:
            ttile_1()
        elif a == 1:
            ttile_2()
        else:
            ttile_kreuzung()

