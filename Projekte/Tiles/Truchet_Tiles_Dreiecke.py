from turtle import *


setup(800, 800, 2560, 10)

pensize(3)
seitenlaenge = 40
speed(0)

tracer(0)

def rechts_unten():
    begin_fill()
    for i in range(2):
        fd(seitenlaenge)
        lt(90)
    end_fill()
    
    for i in range(2):
        fd(seitenlaenge)
        lt(90)

def rechts_oben():
    fd(seitenlaenge)
    lt(90)
    rechts_unten()
    rt(90)
    back(seitenlaenge)
    
def links_oben():
    for i in range(2):
        fd(seitenlaenge)
        lt(90)
    rechts_unten()
    for i in range(2):
        rt(90)
        back(seitenlaenge)
    
def links_unten():
    for i in range(3):
        fd(seitenlaenge)
        lt(90)
    rechts_unten()
    for i in range(3):
        rt(90)
        back(seitenlaenge)
    
pu()
     
for j in range(16):  # acht nach oben
    for i in range(16):  # acht nach rechts
        goto(i*seitenlaenge-300, j*seitenlaenge-300)
        if j % 4 == 0:  # wenn der Index der Zeile genau durch vier teilbar ist
            if i % 4 == 0:  
                links_unten()  # 0, 4, 8, 12
            elif i % 4 == 1:
                links_oben()  # 1, 5, 9, 13, 17, 21, 25
            elif i % 4 == 2:
                rechts_oben()  # 2, 6, 10, 14
            else:  # i % 4 == 3:
                rechts_unten()  # 3, 7, 11, 15
        

        elif j % 4 == 1:
            if i % 4 == 0:
                rechts_unten()
            elif i % 4 == 1:
                rechts_oben()
            elif i % 4 == 2:
                links_oben()
            else:
                links_unten()
        

        elif j % 4 == 2:  
            if i % 4 == 0: 
                rechts_oben()   # 0, 4, 8, 12
            elif i % 4 == 1:
                rechts_unten() # 1, 5, 9, 13, 17, 21, 25
            elif i % 4 == 2:
                links_unten()  # 2, 6, 10, 14
            else:  # i % 4 == 3:  # 3, 7, 11, 15
                links_oben() 
        

        else:  # wenn der Index der Zeile gerade ist
            if i % 4 == 0: 
                links_oben()  # 0, 4, 8, 12
            elif i % 4 == 1:
                links_unten()   # 1, 5, 9, 13, 17, 21, 25
            elif i % 4 == 2:  # 2, 6, 10, 14
                rechts_unten()
            else:  # i % 4 == 3: # 3, 7, 11, 15
                rechts_oben() 
        

tracer(1)
        
