from random import choices

quelle = """   immer alles richtig machen
                die Zähne putzen
das Fusselsieb reinigen
das Eisfach abtauen
die Dichtungen mit Silikonöl behandeln
zur Vorsorge-Untersuchung gehen
das Verfallsdatum kontrollieren
ein Backup machen
Lüften
die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
deine Eltern anrufen
 Bei Auszug renovieren
die Kassette zurück spulen
es 15 Minuten einwirken lassen
2m Rangierabstand lassen
     es Fest verschließen
 es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
Öl nicht in den Abfluss gießen
es Nur mit wasserlöslichen Schmierstoffen verwenden
      Sowieso nur zum äußerlichen Gebrauch"""

quellenliste = quelle.splitlines()

# strip schneidet die Leerzeichen nur ganz am Anfang des Strings ab.
# Ich brauche also einzelne Strings, die ich *alle* *einzeln*
# strippen muss... Ich muss also durch die gesamte Liste gehen,
# jedes einzelne Element der Liste in die Hand nehmen, und strip
# drauf ausführen...

neue_liste = []

for zeile in quellenliste:
    # die jeweilige Zeile strippen:
    neu = zeile.strip()
    # die gestrippte Zeile an die neue Liste anhängen:
    neue_liste.append(neu)

# print(neue_liste[0])

auswahl = choices(neue_liste, k=4)

for task in auswahl:
    print("Du sollst " + task + "!")


