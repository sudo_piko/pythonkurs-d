quelltext = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   width="210mm"
   height="210mm"
   viewBox="0 0 210 297"
   version="1.1"
   id="svg1"
   inkscape:export-filename="Weisheit.svg"
   inkscape:export-xdpi="25.656567"
   inkscape:export-ydpi="25.656567"
   sodipodi:docname="Weisheit.svg"
   inkscape:version="1.3.2 (091e20ef0f, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.54063989"
     inkscape:cx="640.9072"
     inkscape:cy="528.07794"
     inkscape:window-width="2560"
     inkscape:window-height="1025"
     inkscape:window-x="0"
     inkscape:window-y="31"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1">
    <rect
       x="-294.27432"
       y="-174.05082"
       width="1149.8619"
       height="1338.6577"
       id="rect3" />
    <rect
       x="93.739349"
       y="204.62741"
       width="637.15387"
       height="463.43527"
       id="rect1" />
    <rect
       x="93.739349"
       y="204.62741"
       width="637.15387"
       height="463.43527"
       id="rect1-7" />
    <rect
       x="93.739349"
       y="204.62741"
       width="637.15387"
       height="463.43527"
       id="rect1-7-2" />
    <rect
       x="93.739349"
       y="204.62741"
       width="637.15387"
       height="463.43527"
       id="rect1-7-3" />
    <rect
       x="93.739349"
       y="204.62741"
       width="637.15387"
       height="463.43527"
       id="rect1-7-8" />
  </defs>
  <g
     inkscape:label="Ebene 1"
     inkscape:groupmode="layer"
     id="layer1"
     style="display:inline">
    <rect
       style="display:inline;fill:#ffffff;stroke-width:8.82685"
       id="rect2"
       width="297"
       height="297"
       x="-43.5"
       y="0" />
    <text
       xml:space="preserve"
       transform="matrix(0.43131934,0,0,0.43131934,-141.26376,-50.713516)"
       id="text1-3"
       style="font-weight:bold;font-size:64px;font-family:sans-serif;-inkscape-font-specification:'sans-serif Bold';text-align:center;white-space:pre;shape-inside:url(#rect1-7);display:inline;fill:#000000;stroke-width:15.1181"
       x="-2.9296875e-06"
       y="0"><tspan
         x="344.47644"
         y="262.85896"
         id="tspan11"><tspan
           style="font-weight:normal;font-family:Z003;-inkscape-font-specification:Z003"
           id="tspan10">Fiskalwürger</tspan></tspan></text>
    <text
       xml:space="preserve"
       transform="matrix(0.43131934,0,0,0.43131934,-25.977455,-0.26211045)"
       id="text1-3-1"
       style="font-weight:bold;font-size:64px;font-family:sans-serif;-inkscape-font-specification:'sans-serif Bold';text-align:center;white-space:pre;shape-inside:url(#rect1-7-2);display:inline;fill:#000000;stroke-width:15.1181"
       x="-2.9296875e-06"
       y="0"><tspan
         x="317.88446"
         y="262.85896"
         id="tspan13"><tspan
           style="font-weight:normal;font-family:Z003;-inkscape-font-specification:Z003"
           id="tspan12">sind wie</tspan></tspan></text>
    <text
       xml:space="preserve"
       transform="matrix(0.43131934,0,0,0.43131934,-105.8959,68.773705)"
       id="text1-3-0"
       style="font-weight:bold;font-size:64px;font-family:sans-serif;-inkscape-font-specification:'sans-serif Bold';text-align:center;white-space:pre;shape-inside:url(#rect1-7-3);display:inline;fill:#000000;stroke-width:15.1181"
       x="-2.9296875e-06"
       y="0"><tspan
         x="254.23645"
         y="262.85896"
         id="tspan15"><tspan
           style="font-weight:normal;font-family:Z003;-inkscape-font-specification:Z003"
           id="tspan14">andere Dinge:</tspan></tspan></text>
    <text
       xml:space="preserve"
       transform="matrix(0.43131934,0,0,0.43131934,-90.215567,144.21375)"
       id="text1-3-9"
       style="font-weight:bold;font-size:64px;font-family:sans-serif;-inkscape-font-specification:'sans-serif Bold';text-align:center;white-space:pre;shape-inside:url(#rect1-7-8);display:inline;fill:#000000;stroke-width:15.1181"
       x="-2.9296875e-06"
       y="0"><tspan
         x="159.93243"
         y="262.85896"
         id="tspan17"><tspan
           style="font-weight:normal;font-family:Z003;-inkscape-font-specification:Z003"
           id="tspan16">sie machen Sachen aaa</tspan></tspan></text>
    <text
       xml:space="preserve"
       transform="matrix(0.37419642,0,0,0.37419642,-43.5,0)"
       id="text1"
       style="fill:#000000;-inkscape-font-specification:Z003;font-family:Z003;stroke-width:15.11811024;font-size:133.75421905px;text-align:end;white-space:pre;shape-inside:url(#rect3)" />
  </g>
</svg>
"""


import random

nomen = """Wünsche
Träume
Ängste
Ziele
Freunde
Hoffnungen
Fragen
Antworten
Träume, 
Erfahrungen, 
Geschichten, 
Missgeschicke, 
Zähne, 
Freunde, 
Verwandte, 
 Kinder, 
 Geister, 
Bücher, 
Koffer, 
Betten, 
Essen, 
Käse, 
Gesetze, 
Fässer, 
Weine, 
Haare, 
Beine, 
Klischees, 
"""

praedikate = """gehen auf
frieren im Winter
krümeln
brauchen Schokolade
haben keine Ahnung
fallen nicht weit vom Stamm
machen allein auch nicht glücklich
ersetzen einen Zimmermann
wurden von der Werbeindustrie frei erfunden
heilen alle Wunden
teilt man gern unter Freunden
machen noch keinen Sommer
werden auch von Vögeln gemieden
fügen sich ganz von allein
ergeben einen schmackhaften Auflauf
haben nur andere Vorzeichen
"""

praedikatliste = praedikate.splitlines()
nomenliste_unbereinigt = nomen.splitlines()

nomenliste_bereinigt = []

for nomen in nomenliste_unbereinigt:
#     print(nomen)
    nomen = nomen.strip()
#     print(nomen)
    # entweder: nomen = nomen.rstrip(","))
#     nomen = nomen.strip().rstrip(", ")  # Don't try this at home
    if nomen[-1] == ",":
        nomen = nomen[:-1]
    nomenliste_bereinigt.append(nomen)
    
    
    
nomen1 = random.choices(nomenliste_bereinigt, k=1)[0]

nomen2 = random.choice(nomenliste_bereinigt)
praedikat = random.choice(praedikatliste)


print(nomen1 + " sind wie " + nomen2 + ": sie " + praedikat + ".")


neuer_quellcode = quelltext.replace("Fiskalwürger", nomen1)
neuer_quellcode = neuer_quellcode.replace("andere Dinge", nomen2)
neuer_quellcode = neuer_quellcode.replace("machen Sachen aaa", praedikat)



# neuer_quellcode = quelltext.replace("sie machen Sachen aaa", "sie sind auch da.")
# neuer_quellcode = neuer_quellcode.replace("Fiskalwürger", "Ühüs")


f = open("Neue_Weisheit.svg", "w",  encoding="utf-8")
f.write(neuer_quellcode)
f.close()


