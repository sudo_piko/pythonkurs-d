from random import choices, randint


woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]

laenge = randint(6, 25)

gedichtliste = choices(woertervorrat, k=laenge)

# print(gedichtliste)

s = ""

for wort in gedichtliste:
    s = s + " " + wort
    
    kommawuerfeln = randint(1, 7)
    if kommawuerfeln == 7:
        s = s + ","
    zeilenumbruchwuerfeln = randint(1,5)
    if zeilenumbruchwuerfeln == 5:
        s = s + "\n"

print(s)


"\n"
