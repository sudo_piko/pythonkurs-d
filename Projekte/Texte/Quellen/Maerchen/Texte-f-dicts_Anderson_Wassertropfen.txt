

Hans Christian Andersen
Sämmtliche Märchen, 1862
Der Wassertropfen

Du wirst doch wohl wahrscheinlich ein Vergrößerungsglas kennen, so ein rundes Brillenglas, das Alles hundert Mal größer macht, als es ist? Wenn man dies nimmt und es vor's Auge hält und auf einen Wassertropfen aus dem Teiche draußen sieht: da erblickt man über tausend wunderbare Thiere, die man sonst niemals im Wasser wahrnimmt. Aber sie sind da, und es ist keine Täuschung. Es sieht beinahe aus, wie ein ganzer Teller voll Meerspinnen, die durcheinander herumspringen. Und sie sind so gierig: sie reißen einander Arme und Beine, Hinter - und Vordertheile aus, und sind doch auf ihre Art lustig und vergnügt.
Nun war einmal ein alter Mann, den alle Leute Kribbel-Krabbel nannten; denn so hieß er. Er wollte stets von einer jeden Sache das Beste haben, und wenn das durchaus nicht anging, dann nahm er es durch Zauberei.
Da sitzt er nun eines Tages und hält sein Vergrößerungsglas vor die Augen und schaut in einen Wassertropfen, der aus einer Wasserpfütze im Graben genommen war. Aber wie kribbelte und krabbelte es da! Alle die Tausende von kleinen Thieren hüpften und sprangen, zerrten aneinander und verschlangen einander.
"Das ist aber doch abscheulich!" sagte der alte Kribbel-Krabbel; "kann man sie denn nicht dazu bringen, in Ruhe und Frieden zu leben, so daß sich Jeder nur um sich selbst bekümmert!" Und er sann und sann, aber es wollte nicht gehen, und er mußte also zaubern. "Ich muß ihnen Farbe geben, sodaß sie deutlicher zu sehen sind!" sagte er; und dann goß er etwas, wie ein kleines Tröpfchen rothen Wein, in den Wassertropfen; aber das war Hexenblut aus dem Ohrläppchen, die allerfeinste Sorte zu neun Pfennigen. Und nun wurden alle die wunderbaren Thierchen rosenroth über und über; es sah aus, wie eine ganze Stadt voll nackter, wilder Männer.
"Was hast Du da?" fragte ein anderer alter Zauberer, der keinen Namen hatte, und das war das Feine an ihm.
"Ja, wenn Du rathen kannst, was das ist," sagte Kribbel-Krabbel, "dann will ich Dir es schenken. Aber es ist nicht leicht ausfindig zu machen, wenn man es nicht weiß!"
Und der Zauberer, der keinen Namen hatte, sah durch das Vergrößerungsglas. Es sah wirklich aus darin, wie eine ganze Stadt, in der alle Menschen ohne Kleider herumliefen! Es war schauderhaft! Aber noch schauderhafter war es, zu sehen, wie der Eine den Andern puffte und stieß, hackte und schnappte, biß und zerrte. Was unten war, sollte nach oben, und was oben war, sollte nach unten! - Sieh, sieh! Sein Bein ist länger, als meins! Bah! Weg damit! Da ist Einer, der hat ein kleines Beulchen hinter dem Ohr, ein kleines, unschuldiges Beulchen. Aber das thut ihm weh, und daher soll es noch mehr weh thun! Und sie hackten darauf los, und sie zerrten an ihm herum, und sie verschlangen ihn wegen des kleinen Beulchens. Da saß Eine so still, wie eine kleine Jungfrau, und wünschte blos Friede und Ruhe. Aber nun sollte sie hervor! Und sie zerrten an ihr und rissen sie herum und verschlangen sie!"
"Das ist spaßhaft!" sagte der Zauberer.
"Ja, aber was meinst Du denn, daß das ist?" fragte Kribbel-Krabbel. "Kannst Du das ausfindig machen?"
"Nun, das kann man doch wohl sehen!" sagte der Andere. "Das ist ja Paris oder eine andere große Stadt; - sie gleichen ja alle einander. Eine große Stadt ist es!"
"Das ist Grabenwasser!" sagte Kribbel-Krabbel.

Hans Christian Andersen (1805-1875)
