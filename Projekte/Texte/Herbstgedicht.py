from random import choices, randint

woerter = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun", "ist"]

l = choices(woerter, k=10)

ergebnissatz = ""

for wort in l:
    ergebnissatz += wort
    # Würfeln mit einem zehnseitigen Würfel:
    interpunktion = randint(1, 10)
    if interpunktion == 10:  # wenn 10 gewürfelt:
        ergebnissatz += ","
    elif interpunktion == 9:  # wenn 9 gewürfelt:
        ergebnissatz += "."
    ergebnissatz += " "
    
    # Und noch mit Zeilenumbruch:
    # Würfeln mit einem vierseitigen Würfel:
    zeilenumbruch = randint(1, 4)
    if zeilenumbruch == 4:  # wenn 4 gewürfelt:
        ergebnissatz += "\n"
    
print(ergebnissatz)
    
