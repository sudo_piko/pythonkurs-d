# Aufgaben vom 11.12.2023

## Wichtige Hinweise

### Weihnachtspause
Am 25.12 und am 01.01.2024 findet kein Kurs statt!

### Auffrischungsstunde
Am 19.12. um 19:00 gibt es eine Auffrischung der bisherigen Inhalte, für alle Neudazugekommenen und Fadenverlorenen. Da geht Piko das bisherige Material nochmal durch.

### Ausstellung
Deadline für die Ausstellungsstücke ist der 15.12.2023, also kommender Freitag. 

Bitte schreibt Ceryo (ceryo@form-f.art) eine Mail mit dem Programm oder dem Bild; gern auch mehr als eins. Wenn Ihr befürchtet, dass Ihr ein Bild oder Programm einreicht, das jemand anderes ganz ähnlich auch einschickt, dann lasst Ceryo da drüber entscheiden. Selbst, wenn Ihr mehrere sehr ähnliche Programme einschickt, ist das nicht schlimm, sondern eher "Eine Reihe" oder ["Eine Serie"](https://glitchgallery.org/alteration/) oder so :D 


## 0 - Lesestoff
Python-Trivia: Der TIOBE-Index misst die Popularität von Programmiersprachen. Python ist da im Moment ganz oben:  
https://www.tiobe.com/tiobe-index/


## 1 – Einmal alles bitte!
### 1.1 – Kreise

* :zap: Schreibt ein Programm, in dem die Turtle in einer for-Schleife Kreise (oder Teile davon) malt. Wie viele Kreise, welche Farbe, welche Strichdicke, wie groß, wie weit auseinander, das könnt Ihr Euch alles selbst aussuchen. 

> Speichert das Programm. Und macht nicht den Fehler, den Piko in der Stunde gemacht hat, ohne es zu bemerken: wenn Ihr jetzt am Programm etwas ändert und es ausführt, speichert Thonny das Programm nochmal – und überschreibt damit das alte Programm... Deshalb ist es sinnvoll, vor dem Ändern eine Kopie zu machen und mit der weiter zu arbeiten. Das macht Ihr mit "Speichern unter ..." – oder mit `Strg`+`Umschalt`+`S`.
 
* :zap: Importiert das random-Modul und macht die Farben oder andere Dinge zufällig. Was oft gut klappt, ist sozusagen "schwacher" Zufall, also nicht zwischen 0 und 100, sondern zwischen 90 und 110 auswürfeln. So werden die Ergebnisse nur ein bisschen unregelmäßig, fast schon organisch:
![Unebener_Kreisring.png](..%2FAbbildungen%2FUnebener_Kreisring.png)
* Wenn Sachen krakelig oder langweilig aussehen, hilft es oft, einfach die Strichdicke hochzustellen. Das geht mit `pensize()`.

### 1.2 – Farbfüllung
:zap:
Öffnet ein neues Programm.
* Lasst die Turtle einen zufälligen Weg laufen. Da können auch Kreisteile dabei sein. Oder der Weg ist nicht *ganz* zufällig, zB wenn eben ein Kreisabschnitt war, könnte danach sehr wahrscheinlich ein weiterer Kreisabschnitt kommen, nur in die andere Richtung gedreht.
* Die Turtle darf auch gerne Schleifen laufen.
* Wenn sie mit dem Weg fertig ist, soll sie die Fläche, die sie eingeschlossen hat, mit einer Farbe füllen. Das geht mit `begin_fill()` (das muss *vor* dem Weg aufgerufen werden) und `end_fill()` (am Ende des Wegs).
* Spielt mit den Farben und der Strichdicke rum! Auch der Radius der Kreise kann zufällig sein, oder die Länge der Striche und die jeweilige Richtung. Fangt aber mit wenig an und fügt erst allmählich Sachen hinzu!


<details> 
  <summary>Tipps: Wie mache ich, dass nach dem einen Kreisabschnitt noch einer kommt? </summary>
Da können wir ein bisschen schummeln: Bevor wir den Kreisabschnitt anfangen, würfeln wir nochmal, zB "0 oder 1", also randint(0, 1). Wenn wir eine 0 würfeln, dann machen nur einen Kreisabschnitt. Bei einer 1 machen wir zwei Kreisabschnitte: wir würfeln den Radius aus (zB von -100 bis 100; Minuszahlen zeichnen den Bogen in die andere Richtung) und speichern ihn in einer Variable. Dann malen wir die beiden Kreisabschnitte, gern auch mit zufälligen Gradzahlen: `circle(radius, randint(0, 100))`. 100 ist hier mein Beispiel für die Gradzahl, also wie viel vom Kreis ich zeichnen lasse.
</details>


### 1.3 Truchet Tiles
:zap:
* Schaut Euch nochmal das Truchet-Tile-Programm an (`Projekte/Tiles/Truchet_Tiles_Kreisbögen.py`) oder baut es neu.  
![TT_Ausschnitt.png](..%2FAbbildungen%2FTiles%2FTT_Ausschnitt.png)
* Macht die Strichdicke dünner.
* Betrachtet, wie von jedem Quadrat alle vier Seiten in der Mitte eine Verbindung zum nächsten Quadrat haben. Was in dem Quadrat passiert, ist eigentlich egal, solange diese Verbindungen existieren:  
![TT_Enden.png](..%2FAbbildungen%2FTiles%2FTT_Enden.png)
* Solange es diese vier Verbindungen gibt, "funktioniert" das Truchet-Tiling. Deshalb haben die Kreuzungen da auch gut reingepasst: Die hatten ja auch vier Verbindungen.   
![TT_Kreuzung.png](..%2FAbbildungen%2FTiles%2FTT_Kreuzung.png)
* :zap: Macht ein eigenes Tile (oder mehrere), das vier Verbindungen hat. Was die Linien da drin tun, könnt Ihr Euch aussuchen: eine Schleife vielleicht, oder Verzweigungen, oder einfach irgendwo aufhören...

## 2 – Inspiration
Holt Euch hier Inspiration für Eure Ausstellungsstücke, programmiert fremde Programme nach und verändert sie!  
[Schöne Kreise](https://md.ha.si/6hVuSirgRrKxgsdwg9NF3w#)  
[Farbübergänge](https://md.ha.si/cuv6MATUTCqjEiL_HyUKsw#)  

Oder schaut Euch Pikos Programme im Ordner `Projekte/Congressprogramme` an, da dürft Ihr nach Herzenslust räubern und neu kombinieren!  
Der Sternenhimmel war übrigens eine Hausaufgabe des letzten Kurses, die Aufgaben findet Ihr hier:
https://gitlab.com/sudo_piko/pythonkurs-c/-/blob/main/Aufgaben/Aufgaben_2023-01-05.md?ref_type=heads  
und hier:  
https://gitlab.com/sudo_piko/pythonkurs-c/-/blob/main/Aufgaben/Aufgaben_2023-01-19.md?ref_type=heads

## 3 – Extra: Turtle und Text

### 3.1 – Herbstgedicht in der Turtle
Hier ist der erste Teil des Herbstgedichts von vor ein paar Wochen:
```python
from random import choices, randint

woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]

laenge = randint(6, 25)
gedichtliste = choices(woertervorrat, k=laenge)
# print(gedichtliste)

for wort in gedichtliste:
   ...
 ```
Statt dieses Gedicht einfach nur zu printen, soll es jetzt zufällig auf der Turtle-Leinwand verteilt werden, etwa so:  
![Turtle_Herbstgedicht.png](..%2FAbbildungen%2FTurtle_Herbstgedicht.png)

Dafür braucht Ihr die Turtle-Funktion `write()`, zum Beispiel so:
```python
write(wort.upper(), align="center", font=("Arial", 30, "normal"))
```
* `align="center"` sorgt dafür, dass das Wort "in die Mitte" von der derzeitigen Position der Turtle geschrieben wird, nicht nur nach rechts.
* `font` stellt die Schriftart ein. 
  * Arial ist eine ganz häufige, schmucklose Schriftart. Da könnt Ihr auch andere nehmen, zB "Times New Roman" oder "Courier", falls die auf Eurem Computer installiert sind.
  * Die 30 ist die Schriftgröße, die könnt Ihr natürlich verändern. 
  * Und "normal" ist die Schriftstärke, also nicht fett (`bold`) oder kursiv (`italic`).
* `.upper()` ist die String-Methode, die Ihr schon kennt – die macht einfach Großbuchstaben, also aus "Herbst" "HERBST".

:zap: Schreibt das Gedicht mit `write()` auf die Turtle-Leinwand. Die Wörter sollen zufällig verteilt sein; das macht Ihr mit `goto()` und `randint()`. Dann noch die Hintergrundfarbe mit `bgcolor()` und die Stiftfarbe mit `color()` ändern, und fertig!

### Wenn Ihr mehr Schriftarten haben wollt
Welche Schriftarten Ihr verwenden könnt, hängt davon ab, welche Schriftarten auf Eurem Computer installiert sind. Das könnt Ihr hiermit rausfinden:
```python
from tkinter import Tk, font
root = Tk()
print(font.families())
```
Das gibt Euch ein langes Tupel mit Strings – den Namen der Schriftarten, die Ihr verwenden könnt. (Da könntet Ihr natürlich auch `random.choice` drauf werfen, um eine zufällige Schriftart zu bekommen...)


### 3.2 – Eigenes Gedicht

Findet ein eigenes Gedicht oder einen Text, den Ihr mögt. Das kann ein Liedtext sein, ein Gedicht, ein Zitat, ein Auszug aus einem Buch, ein Spruch, was auch immer. Schreibt es in ein neues Programm, und lasst es mit `write()` auf der Turtle-Leinwand erscheinen. Auch das wäre ein mögliches Ausstellungsstück!

Textvorlagen-Ideen sind zB [die Hacking-Ethik](https://www.ccc.de/hackerethik), [das Selbstverständnis der Haecksen](https://www.haecksen.org/wer-sind-die-haecksen/) oder  [die Verfassung der Republik Užupis](https://uzhupisembassy.eu/uzhupis-constitution/).