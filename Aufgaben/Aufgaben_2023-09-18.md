# Aufgaben vom 18.09.2023

## Am kommenden Montag, den 25.09. ist kein Kurs! Stattdessen werde ich Euch hier auf Gitlab Videolinks zur Hausaufgabenbesprechung und zu den Inhalten hochladen. Ihr könnt Euch aber natürlich trotzdem treffen und die Hausaufgaben besprechen!

## 0 - Unterhaltung
For-Schleifen in the wild:  
https://www.reddit.com/media?url=https%3A%2F%2Fi.redd.it%2Fvzs32ng8lpi21.gif

## 1 – Variablen verändern

### 1.1 Nur einmal verändern
Vergleicht die folgenden Codebeispiele und überlegt, was passiert:

```python
a = 5
print(a)
a = 10
print(a)
```

```python
a = 5
print(a)
a = a + 5
print(a)
```

Was passiert in der dritten Zeile? Warum wäre das in der Mathematik verboten?

### 1.2 In einer for-Schleife verändern

**Bevor** Ihr Euch den folgenden Codeschnipsel anschaut, überlegt, wie Ihr dieses `a = a + 5` von oben in einer for-Schleife verwenden könntet. Was würde passieren, wenn Ihr das in eine for-Schleife schreibt? :zap: Probiert es aus und lasst Euch die Zahl mit `print` ausgeben!

<details> 
  <summary>Codeschnipsel – :zap: erst öffnen, wenn Ihr es selbst ausprobiert habt!</summary>

![Codeschnipsel](../Abbildungen/Codeschnipsel_Inkrementierung_in_for.png)

(Hier in den Tipps kann ich leider keinen formatierten Code hinschreiben, deshalb bekommt Ihr ein Bild davon. Wenn das für eins von Euch ein Problem zu lesen ist, gebt mir unbedingt bescheid, dann finde ich eine andere Lösung!)
</details>


<details> 
  <summary>Tipps: Ich versteh den Codeschnipsel nicht! </summary>
   Verändert die verschiedenen Zahlen und schaut, was passiert. Für einen besseren Überblick könnt Ihr mit `print()` (ohne irgendwas zwischen den Klammern) eine leere Zeile einfügen, sodass Ihr schneller seht, was jeweils in den selben Schleifendurchläufen passiert.
</details>

<details>
  <summary>Tipps: Ich hab da jetzt eine Weile rumprobiert und bin immer noch ahnungslos...</summary>
Die Zahl in i ist bei jedem Schleifendurchlauf immer um 1 höher. Also zuerst 0, dann 1, dann 2, etc. 
Die Zahl in a ist ganz am Anfang 5. Wenn die Schleife das erste Mal in Zeile 5 ankommt, wird sie um 5 erhöht, ist dann also 10. Wenn die Schleife das zweite Mal in Zeile 5 ankommt, wird sie wieder um 5 erhöht, ist dann also 15. Und so weiter.
</details>

## 2 – Regenbogenflagge mit Fehlern
:zap: Findet die Fehler im folgenden Code und korrigiert sie!
- Schaut erstmal, ob Ihr nur so vom Drüberschauen schon Fehler findet. Thonny wird da auch einiges markieren.
- Wenn Ihr keine mehr findet, dann führt das Programm aus. Wahrscheinlich wird es noch Fehlermeldungen geben. Die können Euch helfen – worauf wird hingewiesen? Welche Zeile? (Vielleicht ist der Fehler schon davor...)
- Wenn das Problem ohne Fehlermeldungen läuft: Malt es wirklich eine Regenbogenflagge? Korrigiert das Programm so, dass es eine Regenbogenflagge malt!

```python
from * import turtle

regenbogenfarben = ["purple", "blue", "green", "orange", "red"

pensize(20)

for farbe in regenbogenfarben
forward(200)
    
penup()
back(100)
left(90)
forward(20)
right(90)
pendown()
```


## 3 – Regenbogen 

### 3.1 Vorher noch selbst probieren
Hier kommt gleich die Lösung für den Regenbogen. Wenn Ihr nochmal selbst rumprobieren wollt, braucht Ihr noch zwei wichtige Turtle-Funktionen, nämlich `penup()` und `pendown()`. `penup()` hebt sozusagen den Stift der Turtle, sodass sie über die Leinwand laufen kann, ohne zu malen. `pendown()` setzt den Stift wieder auf die Leinwand und die Turtle malt wieder. 

:zap: Zusammen mit dem, was in Aufgabe 1 passiert ist – und noch einer Menge Knobeln – habt Ihr die Zutaten für einen Regenbogen. Selbst wenn Ihr beim Rumprobieren kein befriedigendes Ergebnis bekommt, werdet Ihr dabei etwas lernen!


### 3.2 Pikos Lösung 
Wenn Ihr schon genug rumprobiert habt, dann schaut Euch den Code an. Welche Teile kennt Ihr schon?
```python
from turtle import *

regenbogenfarben = ["purple", "blue", "green", "yellow", "orange", "red"]
radius = 100

pensize(22)
penup()

for farbe in regenbogenfarben:
    forward(radius)
    left(90)
    pendown()
    
    color(farbe)
    circle(radius, 180)
    
    penup()
    left(90)
    forward(radius)
    
    radius = radius + 20
```

Hier ist eine Version mit einigen Kommentaren. :zap: Ergänzt die Kommentare an den Stellen, wo ich sie schon mit `#`  oder mit einigen Worten angefangen habe!

```python
from turtle import *

# Sechs Regenbogenfarben
regenbogenfarben = ["purple", "blue", "green", "yellow", "orange", "red"]
# Radius von ...
radius = 100

# pensize setzt ...
pensize(22)
# Den Stift heben, damit die Turtle erstmal nicht malt
penup()

for farbe in regenbogenfarben:
    #
    forward(radius)
    left(90)
    pendown()
    
    # 
    color(farbe)
    circle(radius, 180)
    
    #
    penup()
    left(90)
    forward(radius)
    
    #
    radius = radius + 20
```

Wenn Euch das schwerfällt, dann nehmt Euch sechs Stifte und ein Blatt Papier und macht genau das, was da steht – als wärt Ihr die Turtle: 
- Erstmal die Stifte in die richtige Reihenfolge bringen.
- einen Radius von 100 (vielleicht 100 Millimeter, also 10cm) abschätzen
- (Stiftdicke verändert wird schwierig... Das lassen wir mal aus :D)
- sich darauf einstellen, erstmal nicht zu malen, wegen `penup()`
- Den ersten Stift nehmen (= die Kopfzeile der for-Schleife)
- vom Mittelpunkt diese geschätzten 10cm nach vorne gehen – "vorne" ist für die Turtle erstmal Euer rechts, denn sie guckt anfangs in Schreibrichtung
- die Richtung um 90° nach links drehen
- und so weiter...


## 4 – Lizenzen
Schreibt, wenn Ihr möchtet, Eure Lizenzen unter die Bilder im Pad von letzter Woche:  
https://md.ha.si/6hVuSirgRrKxgsdwg9NF3w?both

Dann kann ich zB der Welt darüber berichten: https://chaos.social/@piko/111091158300483270

Falls Ihr noch neue Bilder habt, könnt Ihr die auch gerne noch hochladen!



