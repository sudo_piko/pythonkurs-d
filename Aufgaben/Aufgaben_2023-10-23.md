# Aufgaben vom 23.10.2023


## Ankündigungen
### Donnerstag, 26.10. 17:00 Fadenwiederfinden

Am Donnerstag, den 26.10.2023 um 17:00 gibt es ein spontanes Treffen, in dem Piko nochmal ganz von Anfang an erklärt – für alle, die den Faden verloren haben... Ihr dürft alle Fragen mitbringen, von "Was ist eine Enter-Taste?" über "Was ist denn eigentlich *wirklich* die richtige Anzahl von Keksen?" bis hin zu "Mein Thonny geht nicht...".

### Videos
Die Aufgabe 2 der letzten Woche bespreche ich hier:  
https://diode.zone/w/q83b17oFnq7VHzVkrkFuh8


## 0 - Lesestoff
Truchet tiles:  
https://de.wikipedia.org/wiki/Truchet-Fliesen


Schaut Euch nochmal das Cheatsheet an:    
![elif-Cheatsheet](../Cheatsheets/elif_Cheatsheet.png)


Unterhaltung: Einwand zur Benennung des Keks-Anzahl-Beurteilungs-Programms aus der Stunde:
https://chaos.social/deck/@guenther/111290368980170986

## 1 – Truchet Tiling
Für die letzte Woche haben wir ja Truchet Tiles gebaut. Kopiert die Funktionen für diese Tiles hier in eine neue Datei und macht verschiedene Grids!
![Truchet Tile from Wikipedia](../Abbildungen/Tiles/Truchet_Tiles.png)  
(Abb. [Wikipedia](https://commons.wikimedia.org/w/index.php?curid=11253366))
(Ihr findet eine Musterlösung im Ordner `Projekte/Tiles/` hier im Gitlab. Diese Musterlösung bespreche ich auch im Video, das ich oben verlinkt habe.)

- :zap: Malt ein Grid, in dem nur eine der Kacheln vorkommt, also alles gleich aussieht. Wie verändert sich das Bild, wenn Ihr stattdessen die andere Kachel verwendet?
- :zap: Malt ein Grid, in dem die Kacheln zufällig kommen, so wie in der Labyrinth-Aufgabe letzte Woche.
- :zap: Malt ein Grid, bei dem sich die beiden Kacheln immer abwechseln.
<details> 
  <summary>Tipps: Wie kriege ich die Kacheln abwechselnd? (1)</summary>
   Wenn wir hochzählen, also `0, 1, 2, 3, 4, ...`, dann sind die Zahlen ja immer abwechselnd gerade und ungerade...
</details>
<details> 
  <summary>Tipps: Wie kriege ich die Kacheln abwechselnd? (2)</summary>
   "Gerade" sein bedeutet ja, dass die Zahl ohne Rest durch zwei teilbar ist. Wir können also Code aus dem fizz-Programm verwenden, das wir in der Stunde entwickelt haben – Ihr findet es im Protokoll.
</details>
<details> 
  <summary>Tipps: Wie kriege ich die Kacheln abwechselnd? (3)</summary>
   Überlegt (oder probiert aus), was `i%2` jeweils ist, wenn `i` hochgezählt wird, also in einem `for i in range(...` ist.
</details>
- :zap: Malt ein Grid, bei dem die erste Funktion jede dritte Kachel malt, und die zweite Funktion immer die beiden anderen; so, wie wir das in der Stunde mit dem fizz gemacht haben... Vorsicht, die Variable `i` ist schon besetzt, `j` auch, also kommen wir auf `k`...
Also diese Abfolge:

![Visualisierung](../Abbildungen/Tiles/Jede_dritte_Kachel.png)

- :zap: Malt ein Grid, das zufällig die beiden Funktionen verwendet, aber so, dass die erste Funktion viel wahrscheinlicher ist als die zweite. Vergleicht bei größeren Grid-Größen, wie das dann aussieht. (Für Hilfe hierfür könnt Ihr Euch auch nochmal das Ende des [Hausaufgabenbesprechungs-Videos](https://diode.zone/w/q83b17oFnq7VHzVkrkFuh8) anschauen.)


<details> 
  <summary>Tipps: NameError: name 'randint' is not defined</summary>
   Genauso wie die Turtle, sind auch die Zufallszahlen nicht Teil der eingebauten Funktionen von Python, sondern müssen am Anfang des Programms importiert werden. Bei der Turtle wollen wir einfach direkt alles verwenden, deswegen schreiben wir da `from turtle import *`, importieren also alles. Für die Zufallszahlen brauchen wir nur die Funktion `randint()` und deswegen importieren wir auch nur diese Funktion, schreiben also am Anfang des Programms, zusätzlich zum Turtle-Import: `from random import randint`.
</details>


## 2 – Altersverifikation
Baut eine Altersverifikation, so wie wir in der Stunde eine Passwort-Abfrage gebaut haben. Die Altersverifikation soll so laufen:
```
Bitte Alter eingeben:
> 18

Der Inhalt ist nur für Personen unter 18 Jahren freigegeben. Ältere Personen können sich auch anders unterhalten. 
```
oder:
```
Bitte Alter eingeben:
> 17

Willkommen! Hier hast du lustige Inhalte: ...
```


## 3 – Genart Bots
Hier findet Ihr ein paar Bots, die Algorithmen verwenden, die wir teils schon selbst programmieren könnten. Schaut Euch ein paar von den Posts an und überlegt, was die Regeln sind, nach denen die Bots die Bilder generieren. 

### Random Tiling
Bei diesem Bot hat jedes Bild ein eigenes Regelwerk:  
https://botsin.space/@RandomTiling

### Flags and Faces
Diese zwei Bots haben jeweils ein Regelwerk, nach dem sie immer wieder neue Bilder generieren.  
https://botsin.space/@new_flags  
https://botsin.space/@sillyfaces  
