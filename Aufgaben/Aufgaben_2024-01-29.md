# Aufgaben vom 30.01.2024


## 1 – Farbratespiel

Unser letzter Stand des Farbratespiels ist der hier:

```python
from turtle import *
from random import random


# Fenster einrichten
setup(400, 400, 580, 500)
title("Pikos Farbenratspiel")
ANZAHL_RUNDEN = 3  # eine Konstante
spielende = {"Alex":0, "Kim":0, "Robin":0}


# Nützliche Funktionen
def farbe_verhundertfachen(rgb_tupel):
    neue_liste = []
    for zahl in rgb_tupel:
        zahl = int(round(zahl, 2) * 100)
        neue_liste.append(zahl)
    return neue_liste


def farben_differenz(farbe1, farbe2):
    #  (0.7, 0.1, 0.5), (0.6, 0.2, 0.3) => returnt dann 0.4
    summe = 0
    for i in range(3):
        # nur den Unterschied der ersten Stelle
        unterschied = abs(farbe1[i] - farbe2[i])
        summe = summe + unterschied
    return summe


def bewerten(fehlergroesse):
    # printet eine Bewerung anhand der fehlergroesse
    if fehlergroesse > 0.5:
        print("Das ist aber nicht besonders gut!")
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        print("Du hast ok geschätzt.")
    elif fehlergroesse > 0.1:
        print("Fast!")
    else:
        print("Wow, übermenschlich!")


def punkte_ausrechnen(fehlergroesse):
    # Rechnet von der fehlergroesse aus einen Punktzahl aus.
    # Hier kommt noch Mathematik
    if fehlergroesse > 0.5:
        return 0
    elif fehlergroesse > 0.2:  # 0.2 bis 0.5
        return 20
    elif fehlergroesse > 0.1:
        return 40
    else:
        return 70
    
    
    
punktestand = 0
for i in range(ANZAHL_RUNDEN):
    # Eine Zufallsfarbe generieren und anzeigen
    clear()
    farbe_richtig = (random(), random(), random())
    # zufallsfarbe = (farbe_richtig)  # 0.54, 0.11, 0.93 
    bgcolor(farbe_richtig)


    # Raten lassen
    rate_string = input(
        "Was glaubst du, ist das für eine Farbe? \n Gib drei Zahlen von 0 bis 100 ein: ")  # zB "30 89 10 "
    print("Dein Rateversuch: " + rate_string)


    # Den Rateversuch verarbeiten
    farbe_geraten = []

    ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
    liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
    # farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]


    # Den Rateversuch aufmalen lassen:
    color(farbe_geraten)
    pensize(100)
    dot()

    # Ergebniswerte ausrechnen
    differenz = farben_differenz(farbe_richtig, farbe_geraten)
    punkte = punkte_ausrechnen(differenz)
    punktestand = punktestand + punkte
    
    # Auf Ergebnis reagieren
    print("Richtiges Ergebnis", farbe_verhundertfachen(farbe_richtig))  # 80 94 57
    print("Du lagst um", differenz, "daneben.")
    bewerten(differenz)
    print("Du bekommst", punkte, "Punkte!")
    print("Du hast jetzt", punktestand, "Punkte.")
    
    
    for person in spielende:
        print("Punktestand für " + person +":")
        print(spielende[person])
 
 
    input("Weiter mit Enter ... ")

exitonclick()
```

:zap: Kopiert diesen Code in eine neue Datei und führt ihn erstmal aus – es ist ein funktionierendes Spiel, aber manche Sachen passen nicht zusammen: Nur eine Person spielt, aber danach kommt dann doch noch der Punktestand für die drei Personen – der immer 0 bliebt. Wir haben also nur das `spielende`-Dictionary und die for-Schleife am Ende drangeflanscht, ohne sie so richtig einzubinden. 

:zap: Ergänzt Euren Code, dass er auch das Dictionary und die for-Schleife hat.


## 2 – Spielmechanik

Jetzt ist wieder ein Papier-und-Stift-Moment: Wie soll das Spiel für mehrere Personen ablaufen? Schreibt Euch einen Ablauf auf!

<details> 
  <summary>Möglicher Ablauf als Vorschlag </summary>
   Farbe wird angezeigt  //
Rateversuche werden abgefragt ("Alex, was glaubst Du...", Alex gibt einen Rateversuch ein, "Kim, was glaubst Du...", Kim gibt einen ein, etc.) //
Die drei Farben werden als drei dots angezeigt  //
Die richtigen Zahlenwerte werden geprintet  //
Die drei Rateversuche werden bewertet ("Alex' Versuch (80 90 30) lag um 0.2 daneben. Das ist ok. Alex bekommt 20 Punkte." nächste Zeile: "Kims Versuch (70 80 20) lag um 0.1 daneben. Fast! Kim bekommt 40 Punkte." etc.)  //
Die Punkte werden aufsummiert und der Punktestand wird geprintet  //
"Weiter mit Enter ... "
</details>

Wenn Ihr es Euch zutraut, versucht diese Mechanik umzusetzen! Geht zunächst davon aus, dass das Dictionary auf magische Weise immer die richtigen Zahlen enthält – darum könnt Ihr Euch später kümmern.  
Im Folgenden ist das Ganze schrittweise in Aufgaben.

## 2.1 – Drei Rateversuche
:zap: Schreibt eine for-Schleife, die drei Rateversuche abfragt. Die Rateversuche sollen erstmal gar nicht gespeichert werden, nur abgefragt, und dann vielleicht direkt geprintet. Erinnert Euch an die for-Schleifen mit dem Dictionary – den Code findet Ihr auch nochmal im Protokoll.

Genauso wie wir in for-Schleifen oft Listen befüllen, können wir auch Dictionaries befüllen:
```python
rateversuche = {}
for person in spielende:
    versuch = input(...)  # Hier muss noch was rein
    print(person + "s Versuch war " + versuch)
    rateversuche[person] = versuch
```
Erinnert Euch an den Fahrradkeller: `fahrradkeller["Faltrad"] = 3` hat die Anzahl der Falträder im fahrradkeller auf 3 gesetzt. Ob da schon ein Wert drin war oder nicht, ist egal. Wenn wir also `rateversuche["Piko"] = versuch` machen, dann wird der Rateversuch in das Dictionary `rateversuche` gespeichert, und zwar mit dem "key" `"Piko"`. Im Codeschnipsel steht da aber nicht "Piko" sondern `person` –" also was auch immer in der Variable `person` drin ist. Das ist aber ja die Schleifenvariable, also ist da in jedem Schleifendurchlauf was neues drin – nämlich "Alex", "Kim" und "Robin". 

Ok, `person` hat also immer den jeweiligen Namen. `versuch` hat den jeweiligen Rateversuch, also zB "80 90 30". Wenn wir also `rateversuche[person] = versuch` machen, dann wird der Rateversuch von Alex unter dem key "Alex" gespeichert, der von Kim unter dem key "Kim" und der von Robin unter dem key "Robin".
`fahrradkeller[dings] = dongs` macht, dass der fahrradkeller so etwas dazubekommt: `{dings: dongs, ...`
`rateversuche["Alex"] = "80 90 30"` macht, dass `rateversuche` so etwas dazubekommt: `{"Alex": "80 90 30", ...`

:zap: Ergänzt also die for-Schleife, sodass sie die Rateversuche in ein Dictionary speichert. Gebt dann das Dictionary aus, sodass Ihr seht, ob es funktioniert hat. Es soll so aussehen (die einzelnen Ziffern sind natürlich von der Eingabe abhängig): `{'Alex': '20 30 40', 'Kim': '40 30 20', 'Robin': '80 80 80'}`

:zap: Im `rateversuche`-Dictionary sind also jetzt die Zahlen – aber noch als String. Es wäre viel besser, wenn sie als Zahlen, vielleicht sogar schon als Kommazahlen, drin wären. Also das Tupel `(0.2, 0.3, 0.4)` statt dem String `"20 30 40"`. Dafür müssen wir den Rateversuch umwandeln *bevor* wir es in das Dictionary speichern (also vor der Zeile `rateversuche[person] = versuch`). Die ganze Verarbeitung ist das hier: 

```python
    # Den Rateversuch verarbeiten
    farbe_geraten = []

    ratestring_stripped = rate_string.strip()  # "30 89 10"  (Leerzeichen am Anfang und Ende entfernen)
    liste_ratestrings = ratestring_stripped.split(" ")  # ["30", "89", "10"]
    # farbe_geraten = farbe_geraten.strip().split(" ")  # Das wäre beides auf einmal, ist aber unübersichtlich

    for ziffernschnipsel in liste_ratestrings:  # ["30,", "89", "10"]
        ziffernschnipsel = ziffernschnipsel.strip(",")
        kommazahl = int(ziffernschnipsel) / 100  # zB "30" => 0.3
        farbe_geraten.append(kommazahl)  # [0.3, 0.89, 0.1]
```

Da machen wir aus dem `rate_string`, also z.B. `"30 89 10"` oder wie auch immer die Ziffern sonst sind, eine Liste mit Kommazahlen, `[0.3, 0.89, 0.1]`. Diese Umwandlung muss jetzt mit dem Versuch der jeweiligen `person` innerhalb der for-Schleife passieren. Ihr müsst den Variablennamen `rate_string` durch einen anderen Variablennamen ersetzen, in dem der Rate-String drin ist. Welcher ist das? Und was müsst Ihr dann in das Dictionary speichern (=in welcher Variable ist das Ergebnis, das wir haben wollen)?

<details> 
  <summary>Tipps: womit `rate_string` ersetzen? </summary>
  In `rate_string` war ja in einer früheren Version der Rateversuch als String drin, also "30 89 10". Jetzt haben wir ja die for-Schleife, und darin wird ein Rateversuch abgefragt. Wie heißt die Variable da? Printet sie, um sicher zu gehen.
</details>
<details> 
  <summary>Auflösung: womit `rate_string` ersetzen? </summary>
    `versuch` ist der Rateversuch, also z.B. "30 89 10". Also müssen wir `rate_string` durch `versuch` ersetzen.
</details>
<details>
<summary>Tipps: Was im Dictionary speichern?</summary>
Das Dictionary mit den Rateversuchen soll am Ende so aussehen: `{'Alex': [0.2, 0.3, 0.4], 'Kim': [0.4, 0.3, 0.2], 'Robin': [0.8, 0.8, 0.8]}`. Also müssen wir die Liste mit den Kommazahlen speichern: `[0.2, 0.3, 0.4]`. Wo ist die drin? Printet sie, um sicher zu gehen, ob Ihr die richtige habt! 
</details>

## Extra

Wenn Ihr möchtet, könnt Ihr weiterarbeiten; wir werden in der kommenden Stunde an dem Spiel gemeinsam weiterarbeiten. 
Die nächsten Schritte sind:
- Die `dot()`s nebeneinander setzen. 
- Die Fehler ausrechnen, die Spieler*innen loben/bewerten und Punkte vergeben.
- Ganz am Ende die Punkte anzeigen und rausfinden, wer gewonnen hat.