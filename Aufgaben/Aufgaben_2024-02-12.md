# Aufgaben vom 12.02.2024

## 0 - Lesestoff
Wikipedia zu Unicode:  
https://de.wikipedia.org/wiki/Unicode

## 1 – Buchstaben zählen

Wir hatten in der Stunde ja schon bestimmte Buchstaben gezählt:
```python
text = """Findet heraus, was die Dictionary-Methode .values() macht. ⚡ Probiert es aus oder schlagt es im Internet nach!
Was ist in der Variable dings im folgenden Codeschnipsel? Stellt erst eine Vermutung an und probiert es dann aus!"""

d = {"e": 0, "a": 0, "o": 0}
for zeichen in text:
#     print(dings, dings.isalpha())  # Erstmal rausfinden, was das eigentlich macht...
#     if zeichen == "e" or zeichen == "a" or zeichen == "o":  # Das funktioniert genauso wie die Zeile darunter
    if zeichen in "eao":
        # d[zeichen] += 1  # Das funktioniert genauso wie die Zeile darunter
        d[zeichen] = d[zeichen] + 1
    
print(d)
```
Ich habe hier absichtlich die letzte Zeile der for-Schleife genommen (mit `d[zeichen] = d[zeichen] + 1`) und nicht die vorletzte (mit `d[zeichen] += 1`), die eigentlich kürzer und eleganter ist, weil das, was wir gleich machen wollen, mit der letzten Zeile besser funktioniert.


**Im folgenden Absatz war ein Fehler: Piko hat links und rechts verwechselt.**  
:zap: Verändert die *rechte* Hälfte dieser Zeile so, dass nicht mehr `d[zeichen]` verwendet wird, sondern d.get(). Wenn Ihr Euch mit .get() noch unsicher seid, dann schaut im Protokoll oder in der letzten Hausaufgabe nach, und spielt damit ein bisschen herum. Wie viele Argumente braucht .get() mindestens? Was tut das erste Argument? Was tut das zweite? 

Denkt bitte vorher einige Minuten nach und probiert Sachen aus, bevor Ihr jeweils zum nächsten Tipp geht – Ihr lernt dadurch viel mehr, auch wenn Ihr nicht auf die Lösung kommt.
<details> 
  <summary>Antwort zu den Fragen zu .get() </summary>
   .get() braucht mindestens ein Argument, das erste: Dieses Argument wird im Dictionary nachgeschlagen. Es ist also wie das, was zwischen die eckigen Klammern kommt: punktezahlen["Alex"] ist das Gleiche wie punktezahlen.get("Alex") – solange wir wissen, dass Alex im Dictionary `punktezahlen` vorkommt. 
<br>Wir hatten aber bisher immer zwei Argumente bei .get(), weil das zweite Argument das eigentlich interessante ist: <br>Was gibt .get() zurück, wenn das, wonach wir suchen, nicht im Dictionary ist? Wenn Alex nicht im Dictionary `punktezahlen` vorkommt, dann gibt es bei punktezahlen["Alex"] einen Fehler, aber bei punktezahlen.get("Alex") gibt es keinen Fehler, sondern es wird das zurückgegeben, was wir als zweites Argument angeben. Das ist also, was .get() zurückgibt, wenn das, wonach wir suchen, nicht im Dictionary ist. Wenn Alex da nicht drin ist, soll .get() 0 zurückgeben, dann schreiben wir also punktezahlen.get("Alex", 0). 
<br><br>
Uuuuund wenn ein Buchstabe in unserem Dictionary `d` nicht vorkommt, und wir schlagen ihn nach, um rauszufinden, wie viele von diesem Buchstaben bisher gezählt wurden, was soll .get() dann zurück geben? => nächster Tipp
</details>


<details> 
  <summary>Antwort zur Frage im vorherigen Tipp </summary>
   Auch dann möchten wir 0 zurückbekommen, weil er ja bisher 0 mal vorgekommen ist. Wie also sieht die linke Hälfte der Zeile aus, wenn wir .get() verwenden? => übernächster Tipp
</details>

<details> 
  <summary>Einschub: Möp, ich verwende get richtig, aber es zählt nicht mehr!</summary>
   Ihr dürft das `+1` nicht vergessen. Been there, done that. :D
</details>

<details> 
  <summary>Antwort zur Frage im vor-vorherigen Tipp </summary>
   d[zeichen] = d.get(zeichen, 0) + 1
</details>


Wenn Ihr die As, Es und Os jetzt mit `.get()` zählen lasst, dann müssen wir das aber nicht mehr nur auf die beschränken – und wir können sogar ein leeres Dictionary am Anfang nehmen, nicht mehr `{"e": 0, "a": 0, "o": 0}` – denn dass da 0 'e's drin sind, dafür sorgt ja jetzt `.get()`. 


:zap: Verändert also auch die Zeile, in der `d` definiert wird, so, dass es ein leeres Dictionary ist, und macht, dass es mit get() mit allen Buchstaben klarkommen würde, die im Text vorkommen. Vielleicht findet Ihr auch einen besseren Namen für das Dictionary als `d` – passt aber auf, dass Ihr den Namen überall ändert, wo Ihr ihn verwendet...

Jetzt sollte das Dictionary immer noch nur "a", "e" und "o" (mit den jeweiligen Zahlen) enthalten. Warum ist das so?

<details> 
  <summary>Antwort </summary>
Weil wir get() nur ausführen, wenn in Zeichen ein e, a oder o steht. Da ist die if-Bedingung schuld: Wenn das Zeichen nicht in "eao" steht, dann wird die Zeile mit .get() nicht ausgeführt, und das Dictionary wird nicht verändert... Kann aber natürlich sein, dass Ihr da schon dran gedacht habt, und schon die nächste Aufgabe gemacht habt. :D
</details>

:zap: Lasst jetzt alle Buchstaben zählen, die im Text vorkommen. Dafür müsst Ihr bei dem if noch ein bisschen was ändern.

<details> 
  <summary>Was muss das if machen? </summary>
Es soll das get() nur dann ausführen, wenn das Zeichen ein Buchstabe ist. Wie prüft Ihr das nochmal? Wir hatten das in der Stunde...
</details>

<details> 
  <summary>Wie prüft eins das? </summary>
Das war diese String-Methode .isalpha(). Schaut im Protokoll nochmal nach, wie die eingesetzt wird!
</details>


## 2 – Mehr Fahrradkeller

```python
fahrradkeller = {"Rennrad": 3, "Mountainbike": 5, "Trekkingrad": 2, "E-Bike": 1}
```

:zap: Baut ein Programm, das Fahrräder hinzufügt. Es soll immer wieder (mit `input()`) fragen, und dann das dictionary anpassen, so wie in der Abbildung:  

![Fahrradkeller.png](..%2FAbbildungen%2FFahrradkeller.png)

<details> 
  <summary>Immer wieder? </summary>
Bei sowas ist eine while-Schleife gut. Schlagt gerne einfache Beispiele im Internet nach, wenn Ihr noch unsicher seid – oder seht Euch die while-Schleife in Projekte/Spiele/Farbratespiel_Multiplayer.py an.
</details>

:zap: Baut das Programm so, dass es auch mit Fahrradtypen umgehen kann, die noch nicht im Dictionary vorkommen.

<details> 
  <summary>KeyError </summary>
Ihr braucht wieder `get()`. Wie habt Ihr das in Aufgabe 1 gemacht?
</details>

## Extra-Aufgabe: Histogramm

`matplotlib` ist eine Bibliothek (auch "Paket" oder "Library", so etwas ähnliches wie turtle, nur, dass wir es noch aus dem Internet runterladen müssen) für Python, die es uns erlaubt, Daten zu visualisieren. Wir können damit also z.B. Diagramme erstellen.

### 1 - Installation

`matplotlib` ist nicht automatisch bei Python dabei; Profis sagen "es ist nicht in der Standardbibliothek". Das heißt, wir müssen es erst installieren. Das geht so: Klickt in Thonny oben in der Menüleiste auf "Werkzeuge", dann auf "Verwalte Pakete ...". Da öffnet sich ein Fenster, in dem Ihr, Überraschung, Pakete verwalten könnt. Gebt in die Suchleiste oben "matplotlib" ein und drückt Enter oder klickt auf "Suche im PyPI". Ganz oben in den Suchergebnissen sollte "matplotlib" stehen (siehe Screenshot). 

![Pakete_verwalten.png](..%2FAbbildungen%2FPakete_verwalten.png)

Klickt auf das Wort, dann sollte es sich eine Detail-Ansicht zeigen. Da könnt Ihr auf "Installieren" klicken, und dann könnt Ihr `matplotlib` benutzen.

### Kleiner Warnhinweis

`matplotlib` hat wahnsinnig viele Funktionen und Möglichkeiten, und es ist meistens ziemlich schwierig, sich zurecht zu finden. Falls Ihr jemals mehr damit machen wollt/müsst, dann schaut Euch unbedingt ein kurzes Tutorial an und macht es dann wie die meisten Profis: Sucht Euch Beispiele im Internet und passt sie an Eure Bedürfnisse an.

Beweis zur Unterhaltung: https://chaos.social/deck/@piko/111924508052513254

### 2 - Ein Histogramm erstellen

Wenn ihr `matplotlib` installiert habt, dann hängt folgenden Code an die Datei aus Aufgabe 1 an:
```python
import matplotlib.pyplot as plt

sortiertes_dictionary = dict(sorted(d.items(), key=lambda x: x[1], reverse=True))
print(sortiertes_dictionary)
plt.bar(sortiertes_dictionary.keys(), sortiertes_dictionary.values(), 1.0, color='g')
plt.show()
```

Troubleshooting:
- ModuleNotFoundError: No module named 'matplotlib' => da ist wohl was beim Installieren schief gegangen. Versucht es nochmal oder schreibt mir eine Mail.
- NameError: name 'd' is not defined => `d` ist ja das Dictionary. Aber Ihr habt das eventuell umbenannt, deshalb muss da der Name des Dictionaries hin, das Ihr in Aufgabe 1 benutzt habt.

Immer noch nicht genug? :zap: Spielt mit den beiden Werten `1, color='g'` in der vorletzten Zeile rum!