# Aufgaben vom 04.12.2023

### Es ist ein Fehler aufgefallen: In Aufgabe 1.2 hat Piko vergessen, zu erwähnen, dass Ihr den neuen String per Hand in eine neue svg-Datei kopieren sollt. Siehe unten.

## Ankündigungen
Es gibt eine Auffrischungsstunde für Fadenverlorene und Neudazugekommene am 19.12. um 19:00.

Die Musterlösung für die Aufgabe 2 von letzter Woche findet sich in [Aufgaben_2023-11-27.md](..%2FAufgaben%2FAufgaben_2023-11-27.md)

## 1 – Weisheitengenerator für Bilder

### 1.1 Per Hand
Im Ordner [Projekte/Texte/Weisheitsgenerator](..%2FProjekte%2FTexte%2FWeisheitsgenerator) findet Ihr eine SVG-Datei namens `Weisheit.svg`. Wenn Ihr die Datei auf der Webseite öffnet, könnt Ihr das Bild anschauen – eine Zufallsweisheit in einer kitschigen Schrift. Direkt darüber sind ein paar Buttons auf der rechten Seite, unter anderem die beiden hier:  
![Gitlab_Ansichtsumschalter.png](..%2FAbbildungen%2FGitlab_Ansichtsumschalter.png)  
Wenn Ihr auf den linken Button (`</>`) klickt, dann zeigt Euch Gitlab den Quellcode des Bildes an. Wenn Ihr auf den rechten Button (das Blatt Papier mit dem Eselsohr) klickt, seht Ihr wieder das Ergebnis-Bild. 

* :zap: Sucht in dem Quellcode die Wörter, die im Bild angezeigt werden, also "Eulen", "sind wie" und so weiter.
* :zap: Es gibt einen Download-Button ganz rechts auf der gleichen Höhe der beiden Buttons. Ladet das Bild herunter und öffnet es. Wahrscheinlich wird sich das Bild und nicht der Quelltext öffnen; vielleicht in einem Browser, oder in einem Bildbearbeitungsprogramm, vielleicht mit einer anderen Schriftart. Schließt es wieder und rechtsklickt auf die Datei. Schaut Euch an, welche Programme Euch bei `Öffnen mit >` angeboten werden:  
![Oeffnen_mit_Rechtsklick.png](..%2FAbbildungen%2FOeffnen_mit_Rechtsklick.png)
* :zap: öffnet das Bild mit einem Text-Editor. Jetzt seht Ihr wieder den Quellcode.
  * Unter Ubuntu/Linux könnte das sein: Gedit, Leafpad, Mousepad, Pluma
  * Unter Windows sollte es sein: Notepad
  * Unter Mac sollte es sein: TextEdit
  * Falls das nicht funktioniert, könnt Ihr auch Thonny verwenden! Das wird zwar nicht unter den normalen Optionen angezeigt, aber unter `Andere ...` oder `Mit anderer Anwendung öffnen ...` oder ähnlichem solltet Ihr Thonny auswählen können. 
* :zap: Findet wieder die Wörter (irgendwo kurz vor dem Ende) und verändert den Text, ersetzt z.B. `Eulen` mit `Uhus`, speichert und schließt die Datei. Öffnet sie wieder als Bild – also wieder Rechtsklick, `Öffnen mit >` und dann vielleicht das Programm, das vorhin das SVG als Bild angezeigt hat – oder ein anderes Programm: mehr als schiefgehen kann es nicht :D

### 1.2 Automatisch

:zap: Öffnet nochmal den Quellcode (siehe oben). Kopiert den ganzen Quellcode in eine neue Datei in Thonny. Macht vorne und hinten je drei Anführungszeichen dran und schreibt einen Variablennamen davor, zB `quellcode = `  
Ungefähr wie in dieser Abbildung, aber natürlich ist Eure Datei länger, weil ich hier viele Zeilen weggelassen habe:  
![SVG_in_Thonny.png](..%2FAbbildungen%2FSVG_in_Thonny.png)
:zap: Speichert die Datei als Python-Datei ab, zB `Weisheits_SVG_Generator.py`.

#### Intermezzo: Texte ersetzen
:zap: Schaut Euch nochmal die String-Methode `replace` an. Entweder, Ihr kramt den w3schools-Link aus den (vor-?)letzten Hausaufgaben raus, oder Ihr schaut einfach selbst im Internet nach (z.B. `python string method replace`) ooooder Ihr probiert selbst einfach ein bisschen herum. 

:zap: Ersetzt in den folgenden Texten die Textteile so, dass es wieder stimmt:
```python
text = "Montag Dienstag Dingsdongstag Donnerstag Freitag Samstag Sonntag"
```
```python
text = "Die Würde des Tasters ist unanmenschbar."
```
```python
text =  "Pythonkurs Dingsbums"
```

:zap: Verwendet `replace`, um string1 in string2 zu verwandeln:
```python
string1 = "Ich bin ein S.t?r'i_n:g"
string2 = "Ich bin ein lalalala"
```
```python
string1 = "')[1].split(' | ')] for line in the_puz.splitlines()]]]))"
string2 = "')[1].split(' | ')] for line in my_new_puzzle.splitlines()]]]))"
```
```python
string1 = """from turtle import *

for i in range(20):  # hier! Die Anzahl.
    circle(100)
    right(10)"""
    
string2 = """from turtle import *

for i in range(40):  # hier! Die Anzahl.
    circle(100)
    right(10)"""
```

:zap: Verwendet `replace`, um den Sperling mit dem ausgewürfelten Wort zu ersetzen!
```python
from random import choice

woerterliste = ["Stephenschlüpfer", "Meisendickkopf", "Fiskalwürger", "Rotkopfwürger", "Torobülbül", "Strichelkehlsopranist", "Ultramarinbischof", "Maskensaltator", "Cinderellaastrild"]

ausgewuerfelt = choice(woerterliste)

satz = "Der Vogel des Tages ist der Sperling."

print(satz.replace(???)
```


#### Zurück zum SVG
:zap: Nehmt wieder die Python-Datei (mit dem `quellcode = ...`). Kopiert aus der [Lösung](..%2FProtokolle%2FProtokoll_2023-12-04.md) zur letzten Hausaufgabe, die uns `nomen1`, `nomen2` und `praedikat` (oder wie auch immer Ihr das genannt habt) generiert hat, den gesamten Code in die neue Datei.   

:zap: Führt das einmal aus, um sicherzugehen, dass noch alles funktioniert. Weil wir mit `quellcode` noch nichts machen, sollte es nur eine Zeile mit der Weisheit ausgaben, genauso wie bei der Hausaufgabe. 

:zap: Wendet `replace()` auf den langen `quellcode`-String an! Ersetzt die Eulen mit `nomen1`, und so weiter. Orientiert Euch am Beispiel mit dem Sperling oben.

HIER HAT PIKO VERGESSEN, ZU ERWÄHNEN, DASS IHR DEN NEUEN QUELLCODE-STRING IN EIN SVG-FILE KOPIEREN SOLLT! (thx @ C for pointing that out!)
* entweder Ihr legt eine neue Textdatei (mit dem Texteditor) an und speichert sie mit dem Datei-Ende `.svg`, oder Ihr überschreibt den



:zap: Öffnet die Datei als SVG-Bild. Wenn alles geklappt hat, sollte es jetzt eine Weisheit mit den von Euch ausgewürfelten Wörtern anzeigen.

## 2 – Extra

Gefällt Euch die Schriftart nicht? Dann könnt Ihr sie ändern! Der Name der Schriftart ist "Z003". 
* Findet das erstmal im Quellcode (mit Strg+F sollte das ganz einfach gehen). 
* Dann sucht Euch eine Schriftart aus, die auf Eurem Computer existiert und die Ihr schöner findet. Das könnt Ihr in einem Textverarbeitungsprogramm machen, zB LibreOffice Writer oder Microsoft Word. 
* Wenn Ihr die Schriftart gefunden habt, dann ersetzt den Namen "Z003" im Quellcode der SVG-Datei durch den Namen Eurer Schriftart.
* Schaut, ob Euch das gefällt. Wenn ja, dann ersetzt es auch im Python-Programm!

Probiert, ob Ihr auch den Ort der Wörter verändern könnt. Das sind wieder Koordinaten, mit x und y. Findet die richtigen und schiebt die Wörter ein bisschen durch die Gegend, bis Ihr ein Gefühl dafür habt. Dann könnt Ihr die Wörter zufällig auf dem Bild platzieren lassen!

## 3 – Turtle write
Da die Aufgabe 1 recht lang geworden ist und Ihr sicher auch ein bisschen jahresendgestresst seid, gibt es diese Woche nur eine Aufgabe. Wenn Ihr trotzdem sehr gespannt seid, könnt Ihr selbstständig im Internet rausfinden, was denn `write()` aus dem turtle-Module so macht.


## 4 – Extra 2: In Datei speichern 
Diese Aufgabe wurde am 9.12.2023 ergänzt.  
:zap: Hängt die folgenden Zeilen an Euer Programm an:
```python
f = open("Neue_Weisheit.svg", "w")
f.write(neuer_quellcode)
f.close()
```
`neuer_quellcode` ist eine Variable, die den neuen Quellcode (also mit den Ersetzungen) enthalten sollte. Den Namen könnt Ihr natürlich verändern. Diese drei Zeilen machen, dass der Text der Variable automatisch in eine Datei geschrieben wird.