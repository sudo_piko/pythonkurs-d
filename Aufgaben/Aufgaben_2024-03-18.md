# Aufgaben vom 18.03.2024

## 0 - Lesestoff

Mehr Infos zu Klassen:
https://programmieren-starten.de/blog/python-klassen/

Die Aufgabe 2 bespreche ich in einem Video:
https://diode.zone/c/pykurs_d/videos

## 1 – Choral

Schaut Euch nochmal den Code für den Choral an:  
https://github.com/Quefumas/gensound/wiki/Melodic-Shorthand-Notation

Was ist in `sig` drin? Warum ist das für uns etwas Ungewöhnliches? Schaut den Code genau an!

<details> 
  <summary>Ja, warum? </summary>
   `sig` ist eine Variable, aber wird dann später wie eine Funktion verwendet – weil da auch eine Funktion drin gespeichert ist! Nämlich zum Beispiel `Triangle()`. Bei der Definition der Variable ist da keine Klammer, deswegen wird die Funktion nicht ausgeführt oder ausgewertet. Es kommt also in `sig` nicht das Ergebnis der Ausführung (das wäre zB ein return-Wert), sondern die Funktion selbst. Ausgeführt wird sie erst, wenn die Klammern kommen, und das passiert ab `S = sig(...`. Da sind dann ja Klammern; und `sig` wird durch `Triangle` ersetzt; dann steht da `S = Triangle(...`; und das wird dann ausgewertet.
</details>

Warum ist das ganz praktisch?

<details> 
  <summary>Ja, warum? </summary>
   Wie viel müsst Ihr verändern, um bei `S = ...` und `A = ...` und `T = ...` und `B = ...` statt `Triangle` `Sine` zu verwenden? Hätten wir die Variable `sig` nicht, müssten wir es an diesen vier Stellen austauschen. Mit `sig` müssen wir nur `sig = ...` verändern, und die anderen Stellen werden dann automatisch angepasst, weil sie ja `sig` verwenden.
</details>

Experimentiert mit Triangle, Sine und Square. Hört Ihr den Unterschied?

<details> 
  <summary>Tipps </summary>
   Ihr müsst die Zeile `sig = Triangle` verändern, zum Beispiel in `sig = Sine`. Passt auf, dass Ihr da nicht versehentlich Klammern dazuschreibt!
</details>


## 2 – Generative Melodien

Diese Aufgabe ist sehr reichhaltig geworden – verteilt die Arbeit daran gern auf mehrere Wochen.

Das folgende ist nochmal der Ausgangscode für die Melodieklasse – aber Ihr könnt auch jede Weiterentwicklung nehmen, die wir in den letzten Wochen gebaut haben; in dieser Aufgabe geht es vor allem um die Liste, die wir in `Melodie()` reintun.
```python
from gensound import Sine

class Melodie:
    def __init__(self, toene):
        self.toene = toene
    
    def erhoehen(self, intervall=1):
        neue_toene = []
        for ton in self.toene:
            neue_toene.append(ton+intervall)
        self.toene = neue_toene
        
    def print_toene(self):
        print("Hier die Töne der Melodie:", self.toene)
        
    def ausgeben_fuer_sine(self):
        tonnamen = "BCDEFGABCDEFGABCDEFGABCDEFGAB"
        
        ergebnis = ""
        for ton in self.toene:
            ergebnis += tonnamen[ton] + " "
        return ergebnis


kleine_melodie = Melodie([5, 3, 1, 4, 5, 3, 1, 2, 3, 1, 2, 0, 1])
kleine_melodie.print_toene()

melodiestring = kleine_melodie.ausgeben_fuer_sine()
print("So sieht die Ausgabe von 'ausgeben_fuer_sine' aus:", melodiestring)

s = Sine(melodiestring, duration=500.0)*0.2
s.play()
```

Ignoriert also erstmal vollständig die Klasse; die ist nur der Rahmen, damit wir von einer Liste mit Zahlen zu einem String für gensound kommen, und dann mit gensound zu einer Sound-Ausgabe. 

Was wir da in `Melodie()` reintun, ist wirklich nur eine Liste mit Zahlen. Um das noch ein bisschen deutlicher zu machen, können wir die eine Zeile
```python
kleine_melodie = Melodie([5, 3, 1, 4, 5, 3, 1, 2, 3, 1, 2, 0, 1])
```
in zwei Zeilen aufteilen:
```python
zahlenliste = [5, 3, 1, 4, 5, 3, 1, 2, 3, 1, 2, 0, 1]
kleine_melodie = Melodie(zahlenliste)
```
:zap: Schaut Euch diese beiden Codeschnipsel genau an; was habe ich da gemacht? (Wirklich nicht viel :D)  
Wenn Ihr jetzt nur `zahlenliste` anschaut: da ist auch wirklich wenig Magie drin. Zeit für random!

### 2.1 Reiner Zufall

:zap: Macht eine neue Datei auf und schreibt ein Programm, das eine Liste mit 10 Zufallszahlen zwischen 0 und 9 erstellt und sie dann printet.

<details> 
  <summary>Welches random brauche ich? </summary>
   random.randint(1,6) gibt uns Zufallszahlen zwischen 1 und 6, wie auf einem Würfel.
</details>

<details> 
  <summary>Wie kriege ich mehrere zufällige Zahlen in eine Liste? </summary>
   Dafür braucht Ihr wieder eine for-Schleife.
</details>

<details> 
  <summary>In meiner Liste ist zehnmal die selbe Zahl! </summary>
   Dann wurde nur einmal gewürfelt und diese Zahl zehnmal genommen. Das Würfeln (also der randint-Aufruf) muss innerhalb der for-Schleife passieren, sonst passiert er eben nur einmal.
</details>

:zap: Steckt den Code in eine Funktion, sodass das Programm ungefähr so aussieht:
```python
import random

def zufallszahlenliste(hiervielleichteinargument):
    ...
    return ergebnis

neue_liste = zufallszahlenliste()  # Oder halt mit einem Argument zwischen den Klammern...
print(neue_liste)
```

:zap: Kopiert den Code in Euer Melodie-Programm und ersetzt die Liste in `Melodie()` durch die Liste, die durch die neue Funktion generiert wird. Aber Vorsicht: Das ist eine selbstständige Funktion, keine Methode, also kein Teil der Klassendefiniton. Deswegen muss ihre Definition "außerhalb" der Klasse sein; also davor oder danach, jedenfalls nicht so eingerückt wie die Methoden (also zB `def __init__(...)` und `def erhoehen(...)` und so weiter) 

:zap: Spielt das Programm ab und hört, was passiert. Spielt es mehrfach ab und vergleicht! Bekommt Ihr zufällige, unterschiedliche Melodien?

<details> 
  <summary>NameError </summary>
   Wenn der NameError sich auf random oder randint bezieht, dann habt Ihr wahrscheinlich das random-Modul nicht importiert. Oder Ihr habt Euch vielleicht irgendwo verschrieben. NameErrors beziehen sich darauf, dass Ihr einen Namen verwendet, den Python nicht kennt. Das kann eine Variable sein, die Ihr noch nicht definiert habt, oder eine Funktion, oder auch ein Modul. Sehr oft liegt es an Tippfehlern...
</details>


### 2.2 Verschiedene Arten von Zufall

Jetzt können wir anfangen, die Erstellung der Zufallszahlenliste zu verändern. Kopiert für jede der folgenden Aufgaben die Funktion `zufallszahlenliste()`, benennt sie um und verändert sie, sodass Ihr am Ende eine ganze Reiche an Zufallszahlenfunktionen habt. Dann könnt Ihr gut hin- und herwechseln und die verschiedenen Funktionen vergleichen. 
1. Wie klingt es, wenn Ihr nur Zahlen zwischen 0 und 3 nehmt?
2. Baut die for-Schleife so um, dass sie mit `choice()` immer einen Ton aus folgender Liste nimmt: `[1, 2, 3, 5, 6]` (Musik-Trivia: Erstaunlich viele Melodien kommen mit diesen fünf Tönen aus, und zufällige Melodien mit diesen fünf Tönen klingen oft irgendwie ok. Deshalb haben sie einen Namen: Pentatonik. Die Power der Pentatonik könnt Ihr auch an einem Klavier ausprobieren, falls gerade eins zur Hand ist: Spielt nur die schwarzen Tasten, das ist auch eine Pentatonik.)
3. Das Ergebnis von 2. kriegen wir auch ohne for-Schleife hin, mit der Funktion `choices` statt `choice`. Erinnert Ihr Euch noch daran, wie wir das mit `k=3` gemacht haben? Probiert es aus! Wenn Ihr nicht weiterkommt, dann schlagt `python random choices example` im Internet nach. Wenn Ihr verwirrt seid, dann nehmt den Code nochmal in eine eigene Datei, die nur die Zahlen printet.
4. Was war nochmal der Unterschied zwischen `choices()` und `sample()`? 
    * Wenn Ihr es nicht mehr wisst, dann schlagt es im Internet nach!
    * Wie wird sich dann eine Melodie mit `choices()` von einer mit `sample()` unterscheiden? Überlegt erst und probiert es dann aus!
5. Was passiert, wenn Ihr in die Variante von 3. statt `[1, 2, 3, 5, 6]` folgende Liste reintut: `[1, 1, 1, 2, 3, 3, 4, 5, 5, 5, 6, 7, 8, 8, 8]` Vorsicht: es geht hier nicht um den Unterschied von `choices()` und `sample()`, wir "manipulieren" hier eher nur die Wahl von `choices()` ein bisschen...
6. Schaut Euch die Funktion `shuffle()` an: https://www.w3schools.com/python/ref_random_shuffle.asp 
    * Gibt sie eine neue Liste als Return-Wert zurück oder verändert sie die Liste, die Ihr übergeben wird? Wie müsst Ihr diese Funktion deswegen einsetzen? Findet das durch Ausprobieren raus!
    * Was könnt Ihr über eine Melodie sagen, deren Töne durch `random.shuffle([1, 2, 3, 4, 5, 6, 7, 8])` generiert werden? Wie unterscheidet sie sich zu der Melodie, die mit `sample()` generiert wurde?
    * Schreibt eine Funktion, die die Liste [1, 2, 3, 4, 5, 6, 7, 8] shufflet und zurückgibt. Wie hört sich die Melodie zu dieser Liste dann an?



### 2.3 Geregelter Zufall

### 2.3.1 Sprungbegrenzung
Wenn Ihr die Melodien, die in 2.2 generiert werden, so anhört, fällt Euch vielleicht auf, dass sie manchmal arg viel "rumspringen", also viele weit voneinander entfernte Töne nacheinander kommen. Die allermeiste Musik, die wir hören, hat das eher nicht, deshalb klingt es für Euch wahrscheinlich fremd und etwas komisch. 

Wie wäre es also, wenn wir nach einem Ton nur solche Töne erlauben, die höchstens drei Töne entfernt sind?

Das hieße, dass in der Töneliste nach einer `2` nur irgendwas zwischen `-1` und `5` kommen darf, aber zum Beispiel keine `7`. 

:zap: Kopiert die Funktion, die Ihr in 2.1 geschrieben habt und verändert sie. In der for-Schleife muss sichergestellt werden, dass der Ton, der angehängt wird, höchstens drei Töne entfernt ist von dem Ton, der das letzte Mal angehängt wurde.

<details> 
  <summary>Tipps: Wie komme ich an den Ton, der das letzte Mal angehängt wurde?</summary>
   Holt Euch am Anfang der for-Schleife die Zahl, die Ihr als letztes angehängt habt, mit `zahlenliste[-1]`. Das geht natürlich nicht, wenn die Liste leer ist, also ganz am Anfang. Am einfachsten könnt Ihr das lösen, indem Ihr *vor* der for-Schleife schon eine Zahl in die Liste reintut. Die könnt Ihr zufällig bestimmen mit einem Extra-Aufruf von randint, oder einfach festlegen (eine 1 klingt oft gut).
</details>

<details> 
  <summary>Tipps: Ok, ich hab den letzten Ton. Aber wie kann ich den nächsten Ton ausrechnen/auswürfeln? </summary>
   Sagen wir, den letzte Ton war eine 4. Demnach dürfen folgende Töne danach kommen: 1, 2, 3, 4, 5, 6, 7 . Das ist ja `4 + (-3)`, `4 + (-2)`, `4 + (-1)`, `4 + 0`, `4 + 1`, `4 + 2`, `4 + 3`. Wenn wir also zu der 4 ein zufälliges Element der folgenden Liste addieren: [-3, -2, -1, 0, 1, 2, 3], dann bekommen wir ein Element der Liste [1, 2, 3, 4, 5, 6, 7]! Wie kommen wir an eine zufällige Zahl zwischen -3 und 3? randint! (Alternativ könnt Ihr auch choice() auf die Liste [-3, -2..., 2, 3] ausführen, aber das ist mehr Tipparbeit und etwas weniger elegant.)   
</details>


### 2.3.2 Wahrscheinlichkeiten
Vielleicht springt es Euch immer noch zu viel herum. Wir könnten die Töne direkt nebenan noch wahrscheinlicher machen. Also bei der `2` soll am wahrscheinlichsten eine `1` oder eine `3` kommen, dann vielleicht eine `2` (Tonwiederholung), und erst dann `-1`, `0`, `4` oder `5`. 

:zap: Überlegt (und probiert dann aus), was hier rauskommt: 

```python
import random
ausgangsliste = [1, 1, 1, 1, 3, 3, 3, 3, 2, 2, -1, 0, 4, 5]
zahl = random.choice(ausgangsliste)
print(zahl)
```

Im letzten Tipp hab ich geschrieben, choice() auf die Liste [-3, -2, ..., 2, 3] auszuführen, wäre weniger elegant. Hier brauchen wir das jetzt aber:
- Falls Ihr es noch nicht habt, baut eine Variante, in der die Zahl, die Ihr auf den letzten Ton addiert, mit choice() aus der Liste mit Zahlen von -3 bis 3 ausgewählt wird.
- Verändert die Liste so, dass sie `-1` und `1` mehrfach enthält. 
- Vergleicht die Varianten, indem Ihr beide mehrfach anhört.

### 2.3.3 Befriedigendes Ende
Ein weiteres Problem ist, dass die Melodie oft sehr unvermittelt abbricht. Das kriegen wir eingedämmt, indem wir am Ende immer ein C anhängen – also 1 oder 8 oder 15 etc. 

Noch ausführlichere End-Stücke, die Ihr alle ausprobieren (oder zufällig anhängen) könnt:
- `5, 8` 
- `7, 8`  oder `0, 1`
- `2, 1` oder `9, 8`
- `4, 5, 8` 
- `6, 5, 8`
- `2, 5, 1`
- `3, 1, 2, 0, 1` oder `10, 8, 9, 7, 8`
- `4, 5, 6, 5, 8` 


<details> 
  <summary>Tipps </summary>
   Dieses Anhängen muss außerhalb der for-Schleife passieren, sonst wird es ja in jedem Schleifendurchlauf angehängt...
</details>

### 2.4 Längen
Was müsst Ihr verändern, um die Länge der Melodie zu verändern? :zap: Verwendet `random.randint()`, sodass die Melodie mal länger und mal kürzer ist!

<details> 
  <summary>Tipps: Melodielänge</summary>
   Die Melodie ist durch die Länge der Zahlenliste festgelegt. Die befüllen wir mit einer for-Schleife; wir müssen also dafür sorgen, dass die for-Schleife eine zufällige Anzahl mal ausgeführt wird. Wenn Ihr zB `for i in range(10)` habt, kriegt Ihr ja 10 Töne in die Liste (plus vielleicht den Anfangston und den Endton, die ja außerhalb der for-Schleife dazukommen...). Diese `10` könnt Ihr mit randint ersetzen.
</details>

Welche Stelle müsst Ihr verändern, um die alle **Tonlängen** zufällig werden zu lassen? Wo könnten wir die Tonlängen festlegen?

Schaut dafür auch nochmal hier rein: https://github.com/Quefumas/gensound/wiki/Melodic-Shorthand-Notation#adding-rhythm  
Wie wird dort die Tonlänge (nicht die Pausen!) festgelegt?

:zap: Verwendet auch dafür randint, aber teilt die Zufallszahl durch 10, sodass die Töne nicht so ewig lang werden...

<details> 
  <summary>Tipps: Tonlängen 1</summary>
   Für diese Aufgabe müsst Ihr in die Klassendefinition reingucken, denn die Tonlängen werden nicht in der zahlenliste festgelegt.
</details>
<details> 
  <summary>Tipps: Tonlängen 2</summary>
   Die Tonlängen werden in `ausgeben_fuer_sine()` festgelegt – oder, in unserem Fall, eben nicht, sodass gensound einfach alle Töne gleich lang macht...
</details>
<details> 
  <summary>Tipps: Tonlängen 3</summary>
   Im unteren Beispiel von dem Link haben die Töne oft ein "=0.5" oder "=1" hinten dran hängen: "C5=0.5 B C=1 G Ab C=0.5". Das muss also auch bei uns in `ausgeben_fuer_sine()` an die Tonnamen drangehängt werden. Wenn es komische Fehlermeldungen gibt, dann lasst Euch den `melodiestring` printen; sieht der so aus, wie Ihr es erwarten würdet? 
</details>