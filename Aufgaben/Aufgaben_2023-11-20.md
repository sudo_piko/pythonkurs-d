# Aufgaben vom 20.11.2023

## 0 - Lesestoff
https://6-2-1.alpaka.space/ (Bitte mehrfach neu laden und die Ergebnisse vergleichen!)


## 1 – Gebotegenerator fertig bauen
### 1.1 – Kleinbuchstaben
```python
from random import sample

quelle = """   immer alles richtig machen
                die Zähne putzen
das Fusselsieb reinigen
das Eisfach abtauen
die Dichtungen mit Silikonöl behandeln
zur Vorsorge-Untersuchung gehen
das Verfallsdatum kontrollieren
ein Backup machen
Lüften
die Verpackung auf Beschädigungen kontrollieren
Hier unterschreiben
Knicken und abreißen
Fest zubeißen
 die Ausfahrt freihalten
Die Akkus aufladen
Am Stecker und NICHT am Kabel ziehen
 es nach Gebrauch zurück bringen
2 Liter Wasser am Tag trinken
deine Eltern anrufen
 Bei Auszug renovieren
die Kassette zurück spulen
es 15 Minuten einwirken lassen
2m Rangierabstand lassen
     es Fest verschließen
 es Außerhalb der Reichweite von Kindern aufbewahren
 Für den Urlaub sparen
 Für die Altersvorsorge sparen
bei Bildern Bildbschreibungen hinzufügen
Den Tag nicht vor dem Abend loben
Kein Öl in den Abfluss gießen
es Nur mit wasserlöslichen Schmierstoffen verwenden
      Sowieso nur zum äußerlichen Gebrauch"""


# print(quelle)
quellenliste = quelle.splitlines()

alle_gebote = []

for zeile in quellenliste:
    # die jeweilige Zeile strippen:
    gestrippte_zeile = zeile.strip()
    # Hier muss der erste Buchstabe der Zeile kleingeschrieben werden.
    
    
    
    # die fertig bearbeitete Zeile an die neue Liste anhängen:
    alle_gebote.append(kleingeschriebene_zeile)


gebote_auswahl = sample(alle_gebote, k=10)

print(gebote_auswahl)

# for schleifenvariable in wertevorrat:
for gebot in gebote_auswahl:
    print("Du sollst " + gebot + "!")
```
:zap: Ergänzt den Code, sodass der erste Buchstabe jeder Zeile kleingeschrieben wird, so wie wir das in der Stunde besprochen haben.

<details> 
  <summary>Tipps </summary>
    Um den ersten Buchstaben der Zeile klein zu schreiben müsst Ihr die Zeile in zwei Teile zerlegen: Den ersten Buchstaben und den ganzen Rest.
    Den ersten Buchstaben könnt Ihr dann mit .lower() kleinschreiben.
</details>

### 1.2 – Letzte Zeile
Die letzte Zeile im `quelle`-String ("Sowieso nur zum äußerlichen Gebrauch") ist offensichtlich Unsinn. Wir könnten sie einfach löschen. Stellen wir uns aber mal vor, wir holen uns diesen String von einem Server, wo wir die Zeile nicht einfach löschen können.  
Wo können wir sie dann aussortieren? Es gibt mehrere Möglichkeiten. :zap: Entscheidet Euch für eine und setzt sie um.

## 2 – Kleine Aufgaben zum Üben
### 2.1 – Sample und choices
* Piko packt seinen Koffer und will drei Hosen mitnehmen. :zap: Schreibt ein Programm, das zufällig drei Hosen aus dem Kleiderschrank auswählt! Bedient Euch dabei am Code, den wir für den Putzplan geschrieben haben (den findet Ihr auch im Protokoll).
```python
pikos_kleiderschrank = ["Jeans", "Cordhose", "Schlabberhose", "Jogginghose", "Feine Hose", "Engelbert-Strauß-Hose"]
```
* :zap: Ihr habt drei Sorten Kekse und möchtet einen zufälligen Keksteller zusammenstellen. Schreibt ein Programm, das den Keksteller zufällig mit 20 Keksen befüllt.
```python
keks_sorten = ["Spekulatius", "Ingwerkeks", "Haselnuss-Ausstecherle"]
```
* :zap: Aus dem folgenden Vorrat aus Wörtern sollen Zufallsgedichte entstehen!
```python
woertervorrat = ["Herbst", "Nebel", "Ruhe", "Dunst", "leise", "weich", "fallen", "blau", "grau", "braun"]
```
So sollen die Zufallsgedichte aussehen:   
`Dunst, weich leise weich Herbst Nebel weich Herbst Dunst fallen, grau `


Wie immer: Versucht erstmal, es ohne Tipps zu lösen. Wenn das nicht hinhaut, dann schaut Euch den ersten Tipp an und versucht es nochmal!
<details><summary>Tipps: Vorgehen</summary>
Ignoriert erstmal die Kommata. Macht das Gedicht zuerst als Liste: ["Dunst", "weich", "leise", "weich",...] Fügt die Liste dann in einer for-Schleife zu einem String zusammen.
</details>

<details><summary>Wie kann ich die Wörterliste zu einem String zusammenfügen?</summary>
Das funktioniert ganz ähnlich wie die amsel-drossel-fink-star-Hausaufgabe – nur dass das, was Ihr befüllt, diesmal keine Liste, sondern ein String ist...
</details>

<details><summary>Huch? Wie befülle ich nochmal einen String?</summary>
befuellter_string = befuellter_string + neuer_teilstring
</details>

<details><summary>Wie komme ich jetzt an die Kommata?</summary>
In der for-Schleife für der String könnt Ihr mit randint würfeln, so wie bei den zufälligen Truchet-Tiles. Wenn Ihr hoch genug würfelt, könnt Ihr ein Komma an den String anhängen.
</details>


### 2.2 – Listenmethoden
Hier findet Ihr eine Liste von Listenmethoden: https://www.w3schools.com/python/python_lists_methods.asp
- :zap: Überfliegt die Liste. Welche Methode kennt Ihr schon? Bei welchen könnt Ihr Euch vorstellen, wofür sie gut sind? Welche verwirren Euch noch?
- :zap: probiert Folgendes aus:
```python
l = [5, 4, 9, 2, 1, 5, 7, 5, 8]
print(l)
l.sort()
print(l)
```
Wie verändert sich l? Was macht die Methode sort()? Was passiert, wenn Ihr l.sort() durch l.sort(reverse=True) ersetzt?
- :zap: probiert auf die gleiche Weise folgenden Methoden aus: `clear()`, `reverse()`, `pop()`
  (reverse=True geht nur bei sort)
- :zap: `pop()` kann aber eigentlich noch mehr! Probiert es hier aus und vergleicht es mit dem Code oben: 
```python
l = [5, 4, 9, 2, 1, 5, 7, 5, 8]
print("l ist vorher:        ", l)
dings = l.pop()
print("l ist jetzt:         ", l)
print("in dings ist jetzt:  ", dings)
```
- :zap: Probiert das auch mit `index(9)`, `count(5)`
- :zap: Sucht im Internet nach dem Unterschied zwischen `append` und `extend`!